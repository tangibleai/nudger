# start-django production ASGI server (daphne)

if [ -f ".env" ] ; then
    source .env
fi

if [ -f ".venv/bin/activate" ] ; then
    source .venv/bin/activate
fi

if [[ $INCLUDE_CELERY = "True" ]]; then
  echo "Starting celery"
  celery -A convohub.celery worker --beat --scheduler django --loglevel=info --concurrency 4 --detach
else
  echo "Skipping celery startup"
fi

echo "Starting daphne"
daphne -b 0.0.0.0 -p 10000 convohub.asgi:application
