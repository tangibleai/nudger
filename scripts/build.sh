#!/usr/bin/env bash

source ./scripts/build_environment.sh

python manage.py migrate

echo "Collecting static files!"
python manage.py collectstatic --noinput -v 2

# Create default Project instance
echo "Default Project instance creation"
python -m scripts.create_default_project

# Creating the default Convo instance for Project if not exits
echo "Default Convo creation for Project"
python -m scripts.create_default_convo_on_db

# Create default user
echo "Default user creation"
python -m scripts.create_predefined_django_users


