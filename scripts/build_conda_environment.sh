
#!/usr/bin/env bash

# Check if Conda environment exists
ENV_NAME="convohub"
if conda env list | grep -q $ENV_NAME
then
    # If it exists, activate the Conda environment
    echo "Conda environment $ENV_NAME already exists. Activating it..."
    source activate $ENV_NAME
else
    # Else, print a message saying the environment does not exist
    echo "Creating Conda environment $ENV_NAME..."
    conda create -n $ENV_NAME python=3.8
    conda activate $ENV_NAME
fi

if [ -f ".env" ] ; then
    echo "WARNING: .env already exists! You should overwrite your .env file with the latest .env_example"
else
    echo "INFO: Creating .env from a copy of .env_example."
    cp .env_example .env
fi
source .env

pip install --upgrade pip poetry wheel build twine

pip install --editable .

python -m spacy download en_core_web_sm
# FIXME: Dev dependencies are NOT installed using "extras [dev]" in pyproject.toml
pip install --upgrade black pytest