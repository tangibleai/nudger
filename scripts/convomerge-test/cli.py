import argparse
import convomerge.script


# def cli():
#     parser = argparse.ArgumentParser(
#         prog='ConvoMerge',
#         description='Allows to merge multiptle linear conversation files into single conversation tree'
#     )
#     parser.add_argument('inputs', nargs='+', help='Path(s) to input .txt or .yml linear conversation files')
#     parser.add_argument('-o', '--output', help='Path to output .yml to store the result', required=True)
#     args = parser.parse_args()

#     scripts = [ScriptBuilder.from_file(script_file) for script_file in args.inputs]
#     merger = ScriptMerger()
#     merger.merge_scripts(scripts)


def cli():
    parser = argparse.ArgumentParser(
        prog="ConvoMerge",
        description="Allows to merge multiptle linear conversation files into single conversation tree",
    )
    parser.add_argument(
        "input", help="Path(s) to input .txt or .yml linear conversation files"
    )
    parser.add_argument(
        "-o", "--output", help="Path to output .yml to store the result", required=True
    )
    parser.add_argument("-n", "--convo-name")
    parser.add_argument("-d", "--convo-description")
    parser.add_argument("-a", "--base-author", default="teacher")
    args = parser.parse_args()

    script_file = args.input
    output_file = args.output
    convo_name = args.convo_name
    convo_description = args.convo_description
    base_author = args.base_author
    script = convomerge.script.read_file(
        script_file,
        convo_name=convo_name,
        convo_description=convo_description,
        base_author=base_author,
    )
    script.to_yaml(output_file)
