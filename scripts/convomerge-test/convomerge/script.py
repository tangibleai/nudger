from __future__ import annotations
from typing import Optional, TextIO
from collections.abc import Sequence, Iterator
from convomerge.state import State, Action, Trigger
from convomerge.comparators import (
    StateActionComparator,
    StateActionComparator,
    SimpleStateActionComparator,
)
from convomerge.parsers import ScriptParser, SimpleScriptParser
import os
import yaml


class LinearScriptPath:
    def __init__(
        self,
        states: Optional[dict[str, State]] = None,
        first_state_name: Optional[str] = None,
    ) -> None:
        if states is None or len(states) == 0:
            self._states: dict[str, State] = {}
            self._first_state = None
            self._last_state = None
            self.validate()
        else:
            self._states: dict[str, State] = {
                state_name: state.copy() for state_name, state in states.items()
            }
            first_state_name = (
                first_state_name
                if len(self._states) > 1
                else list(self._states.keys())[0]
            )
            self._first_state = self._states[first_state_name]
            self.validate()
            self._last_state: Optional[State] = self._states[list(self)[-1].name]

    def __repr__(self) -> str:
        return f"LinearScriptPath(states={repr(self._states)}, first_state_name={repr(self._first_state and self._first_state.name)})"

    def __iter__(self) -> LinearPathIterator[State]:
        return LinearPathIterator(
            {state_name: state.copy() for state_name, state in self._states.items()},
            self._first_state and self._first_state.name,
        )

    def __len__(self) -> int:
        return len(self._states)

    def copy(self):
        return LinearScriptPath(
            {state_name: state.copy() for state_name, state in self._states.items()},
            self._first_state and self._first_state.name,
        )

    # Utils section

    def first_state(self) -> Optional[State]:
        return self._first_state and self._first_state.copy()

    def last_state(self) -> Optional[State]:
        return self._last_state and self._last_state.copy()

    def length(self) -> int:
        return len(list(self)) - 1

    # def append_actions(self, actions: Sequence[Action], trigger: Optional[Trigger] = None) -> LinearScriptPath:
    #     if len(self._states) == 0:
    #         new_state = State(name='start')
    #         self._first_state = new_state
    #     else:
    #         new_state = State(name=f'state_{len(self._states)}')

    #     new_state.actions += [action.copy() for action in actions]

    #     if self._last_state is not None:
    #         if trigger is None:
    #             trigger = Trigger('__next__', new_state.name, new_state.actions[0].group_name)
    #         else:
    #             trigger = trigger.copy()
    #             trigger.state_name = new_state.name
    #         self._last_state.triggers += [trigger]

    #     self._last_state = new_state
    #     self._states[new_state.name] = new_state
    #     self.validate()
    #     return self

    # def append_action(self, action: Action, trigger: Optional[Trigger] = None) -> LinearScriptPath:
    #     return self.append_actions([action], trigger)

    def append_state(
        self, state: State, trigger: Optional[Trigger] = None
    ) -> LinearScriptPath:
        self.validate()

        if state.name in self._states:
            if trigger and trigger.state_name == state.name:
                self._last_state.triggers.append(trigger.copy())
                self.validate()
                return self
            else:
                raise RuntimeError("Appending error due to states name duplication")

        new_state = state.copy(triggers=[])

        if len(self._states) == 0:
            self._first_state = new_state

        if self._last_state is not None:
            # `trigger` should be appended to previous last state
            if trigger is None:
                # create __next__ trigger
                trigger = Trigger("__next__", state.name, state.actions[0].group_name)
            else:
                trigger = trigger.copy()

            self._last_state.triggers.append(trigger)

        self._states[new_state.name] = new_state
        self._last_state = new_state

        self.validate()
        return self

    def prepend_state(
        self, state: State, trigger: Optional[Trigger] = None
    ) -> LinearScriptPath:
        self.validate()

        # if state.name in self._states:
        #     if trigger and trigger.state_name == state.name:
        #         self._last_state.triggers.append(trigger.copy())
        #         self.validate()
        #         return self
        #     else:
        #         raise RuntimeError('Appending error due to states name duplication')

        new_state = state.copy(triggers=[])

        if len(self._states) == 0:
            self._last_state = new_state

        if self._first_state is not None:
            # `trigger` should be appended to previous last state
            if trigger is None:
                # create __next__ trigger
                trigger = Trigger(
                    "__next__", self._first_state.name, state.actions[0].group_name
                )
            else:
                trigger = trigger.copy()

            new_state.triggers.append(trigger)

        self._states[new_state.name] = new_state
        self._first_state = new_state

        self.validate()
        return self

    def append_action(
        self, action: Action, trigger: Optional[Trigger] = None
    ) -> LinearScriptPath:
        new_state = State(
            name=f"state_{len(self._states) + 1}", actions=[action.copy()]
        )
        return self.append_state(new_state, trigger)

    # Validation section

    def validate(self) -> None:
        self._validate_state_names()
        self._validate_linear()

    def _validate_state_names(self) -> None:
        for state_name, state in self._states.items():
            if state_name != state.name:
                raise RuntimeError(
                    "State names constraint of LinearScriptPath violated"
                )

            for trigger in state.triggers:
                if trigger.state_name not in self._states:
                    raise RuntimeError(
                        "State names constraint of LinearScriptPath violated"
                    )

    def _validate_linear(self) -> None:
        if len(self._states) == 0:
            return

        current_state = self._first_state
        num_states = 0

        while current_state is not None and num_states <= len(self._states) + 1:
            if len(current_state.triggers) == 0:
                current_state = None
            elif len(current_state.triggers) == 1:
                next_state_name = current_state.triggers[0].state_name

                if next_state_name not in self._states:
                    raise RuntimeError(
                        "Linearity constraint of LinearScriptPath violated"
                    )

                current_state = self._states[next_state_name]

                if current_state == self._first_state:
                    # The path is looped to first state
                    current_state = None
            else:
                print(self)
                raise RuntimeError("Linearity constraint of LinearScriptPath violated")

            num_states += 1

        if num_states != len(self._states):
            print(self)
            raise RuntimeError("Linearity constraint of LinearScriptPath violated")

    # Import section

    @classmethod
    def from_action_sequence(cls, actions: Sequence[Action]) -> LinearScriptPath:
        path = cls()

        for action in actions:
            path.append_action(action)

        return path


class LinearPathIterator(Iterator):
    def __init__(
        self, states: dict[str, State], first_state_name: Optional[str]
    ) -> None:
        self._states = states
        self._first_state = self._states.get(first_state_name, None)
        self._current_state = self._first_state
        self._loop_reached = False

    def __next__(self):
        if self._current_state is not None:
            current_state = self._current_state

            if self._loop_reached:
                self._current_state = None
            else:
                if len(self._current_state.triggers) == 0:
                    self._current_state = None
                else:
                    self._current_state = self._states[
                        self._current_state.triggers[0].state_name
                    ]

                if self._current_state is self._first_state:
                    self._loop_reached = True

            return current_state.copy()

        raise StopIteration()


class Script:
    def __init__(
        self,
        states: Optional[dict[str, State]] = None,
        action_comparator: Optional[StateActionComparator] = None,
        convo_name: Optional[str] = None,
        convo_description: Optional[str] = None,
    ) -> None:
        if states is None:
            self._states: dict[str, State] = {}
            self._start_state: Optional[State] = None
            self._current_state: Optional[State] = None
            # self._max_level = 0
        else:
            self._states: dict[str, State] = {
                state_name: state.copy() for state_name, state in states.items()
            }
            self._start_state: Optional[State] = states.get("start", None)
            self._current_state: Optional[State] = self._start_state
            # self._max_level = max([state.kwargs.get('level', 0) for state in self._states.values()])

        self._action_comparator = action_comparator or SimpleStateActionComparator()

        if convo_name is None and self._start_state is not None:
            self._convo_name = self._start_state.kwargs.get("convo_name", None)
        else:
            self._convo_name = convo_name

        if convo_description is None and self._start_state is not None:
            self._convo_description = self._start_state.kwargs.get(
                "convo_description", None
            )
        else:
            self._convo_description = convo_description

    def _find_state(
        self,
        name: Optional[str] = None,
        actions: Optional[Sequence[Action]] = None,
        many: bool = False,
    ) -> Optional[State] | Sequence[State]:
        states = list(self._states.values())

        if name is not None:
            if name in self._states:
                states = [self._states[name]]
            else:
                states = []

        if actions is not None:
            states = [
                state
                for state in states
                if self._action_comparator.compare_seq(state.actions, actions)
            ]

        if many:
            return states
        else:
            return states[0] if len(states) else None

    # def _find_paths(self, from_state: State, to_state: State) -> list[list[Trigger]]:
    #     lookup_states: list[tuple[State, list[Trigger]]] = [ (from_state, []) ]
    #     processed_states = set()
    #     discovered_paths: list[list[Trigger]] = []

    #     while len(lookup_states) > 0:
    #         current_state, current_path = lookup_states.pop(0)

    #         if current_state is to_state and len(current_path) > 0:
    #             discovered_paths.append(current_path)
    #             continue
    #         if current_state in processed_states:
    #             continue

    #         processed_states.add(current_state)
    #         next_states = [self._find_state(name=trigger.state_name) for trigger in current_state.triggers]
    #         next_paths = [[*current_path, trigger] for trigger in current_state.triggers]
    #         lookup_states += list(zip(next_states, next_paths))

    #     return discovered_paths

    def _find_paths(self, from_state: State, to_state: State) -> list[LinearScriptPath]:
        lookup_states: list[tuple[State, LinearScriptPath]] = [
            (from_state, LinearScriptPath().append_state(from_state))
        ]
        processed_states = set()
        discovered_paths: list[LinearScriptPath] = []

        while len(lookup_states) > 0:
            current_state, current_path = lookup_states.pop(0)

            if current_state is to_state and current_path.length() > 0:
                discovered_paths.append(current_path)
            if current_state in processed_states:
                continue

            processed_states.add(current_state)
            next_states = [
                self._find_state(name=trigger.state_name)
                for trigger in current_state.triggers
            ]
            next_paths = [
                current_path.copy().append_state(next_state, trigger)
                for trigger, next_state in zip(current_state.triggers, next_states)
            ]
            lookup_states += list(zip(next_states, next_paths))

        return discovered_paths
        #     if current_state is to_state:
        #         discovered_paths.append(current_path)
        #     if current_state in processed_states:
        #         continue

        #     processed_states.add(current_state)
        #     next_states = [self._find_state(name=trigger.state_name) for trigger in current_state.triggers]
        #     # next_paths = [[*current_path, trigger] for trigger in current_state.triggers]
        #     next_paths = [current_path.copy().append_state(next_state, trigger) for trigger, next_state in zip(current_state.triggers, next_states)]
        #     lookup_states += list(zip(next_states, next_paths))
        #     print(lookup_states)
        #     print()

        # return discovered_paths

    def _merge_or_append(
        self,
        branch: LinearScriptPath,
        trigger_value: Optional[str],
        trigger_group_name: Optional[str],
    ) -> None:
        if self._start_state is None:
            self._append(branch, trigger_value, trigger_group_name)
            return

        self._loopback_current_state(trigger_group_name)

        # Try merge branch to the same path (if it exists)
        merge_point = self._find_state(actions=branch.last_state().actions)

        if merge_point is None:
            self._append(branch, trigger_value, trigger_group_name)
            return

        paths = self._find_paths(self._current_state, merge_point)

        if len(paths) == 0:
            self._append(branch, trigger_value, trigger_group_name)
            return

        # for path in paths:
        #     print('compare paths')
        #     print(list(path))
        #     print(list(branch.copy().prepend_state(self._current_state, Trigger(trigger_value, branch.first_state().name, trigger_group_name))))
        #     print('')

        ###
        self._append(branch, trigger_value, trigger_group_name)

    def _loopback_current_state(self, trigger_group_name: str):
        if self._current_state is not None:
            # handle "loopback" to existing state if possible
            loopback_states = self._find_state(
                actions=self._current_state.actions, many=True
            )
            loopback_states.remove(self._current_state)

            if len(loopback_states) > 1:
                raise RuntimeError("Loopback impossible due to ambiguous options")
            elif len(loopback_states) == 1:
                # perform "loopback" to the particular state
                loopback_state = loopback_states[0]
                for state in self._states.values():
                    for trigger in state.triggers:
                        if trigger.state_name == self._current_state.name:
                            trigger.state_name = loopback_state.name
                self._states.pop(self._current_state.name)
                self._current_state = loopback_state

            # handle __default__ trigger
            feedback_trigger = Trigger(
                "__default__", self._current_state.name, trigger_group_name
            )
            for trigger in self._current_state.triggers:
                if trigger.value == feedback_trigger.value:
                    # __default__ trigger already exists
                    if trigger.state_name != feedback_trigger.state_name:
                        raise RuntimeError(
                            "Loopback impossible due to inconsistent __default__ trigger"
                        )
                    break
                if trigger.value == "__next__":
                    # __next__ trigger exists, __default__ trigger shouldn't be created
                    break
            else:
                self._current_state.triggers.append(feedback_trigger)

    @staticmethod
    def _can_merge(path1: LinearScriptPath, path2: LinearScriptPath) -> int:
        return 0

    def _append(
        self,
        branch: LinearScriptPath,
        trigger_value: Optional[str],
        trigger_group_name: Optional[str],
    ) -> None:
        prev_trigger = None

        for next_state_src in branch:
            if len(self._states) == 0:
                # create "start" state
                next_state = State(
                    name="start",
                    convo_name=self._convo_name,
                    convo_description=self._convo_description,
                    nlp="case_insensitive",
                    level=0,
                    **next_state_src.kwargs,
                )
                self._start_state = next_state
                self._current_state = None
            else:
                # create new state
                next_state = State(
                    name=f"state_{len(self._states)}", **next_state_src.kwargs
                )

            # handle actions
            next_state.actions += [action.copy() for action in next_state_src.actions]

            # handle triggers for previous state
            if self._current_state is not None:
                if prev_trigger is None:
                    prev_trigger = Trigger(
                        trigger_value, next_state.name, trigger_group_name
                    )

                prev_trigger.state_name = next_state.name
                self._current_state.triggers.append(prev_trigger)

            if len(next_state_src.triggers) == 0:
                prev_trigger = None
            elif len(next_state_src.triggers) == 1:
                prev_trigger = next_state_src.triggers[0].copy()
            else:
                raise RuntimeError("Unexpected number of triggers in state")

            # prepare for moving to next state
            self._states[next_state.name] = next_state
            self._current_state = next_state

    # Export section

    def to_list(self) -> list:
        return [state.to_dict() for state in self._states.values()]

    def to_yaml(self, fp: str | TextIO) -> None:
        if isinstance(fp, str):
            if not fp.endswith(".yml"):
                fp += ".yml"
            dir = os.path.dirname(fp)
            if not os.path.exists(dir):
                os.makedirs(dir)
            fp = open(fp, "w", encoding="utf-8")

        script_list = self.to_list()
        yaml.safe_dump(script_list, fp, sort_keys=False, encoding="utf-8")
        fp.close()

    # Import section

    @classmethod
    def from_list(cls, states_list: list) -> Script:
        states = {
            state_dict["name"]: State.from_dict(state_dict)
            for state_dict in states_list
        }

        script = cls(states=states)
        return script

    @classmethod
    def from_file(
        cls,
        fp: str | TextIO,
        text_script_parser: Optional[ScriptParser] = None,
        action_comparator: Optional[StateActionComparator] = None,
        convo_name: Optional[str] = None,
        convo_description: Optional[str] = None,
        base_author: str = "teacher",
    ) -> Script:
        if isinstance(fp, str) and (fp.endswith(".yml") or fp.endswith(".yaml")):
            return cls.from_yaml_file(fp)
        else:
            # Assume text file by default
            if text_script_parser is None:
                text_script_parser = SimpleScriptParser()
            if action_comparator is None:
                action_comparator = SimpleStateActionComparator()
            if convo_name is None:
                convo_name = "convo_name"
            if convo_description is None:
                convo_description = ""

            return cls.from_text_file(
                fp,
                text_script_parser,
                action_comparator,
                convo_name,
                convo_description,
                base_author,
            )

    @classmethod
    def from_yaml_file(cls, fp: str | TextIO):
        if isinstance(fp, str):
            fp = open(fp, "r", encoding="utf-8")

        script_list = yaml.safe_load(fp)
        fp.close()
        return cls.from_list(script_list)

    @classmethod
    def from_text_file(
        cls,
        fp: str | TextIO,
        text_script_parser: ScriptParser,
        action_comparator: StateActionComparator,
        convo_name: str,
        convo_description: str,
        base_author: str,
    ) -> Script:
        if isinstance(fp, str):
            fp = open(fp, encoding="utf-8")

        raw_lines = fp.readlines()
        fp.close()

        # Build script
        script = cls(
            action_comparator=action_comparator,
            convo_name=convo_name,
            convo_description=convo_description,
        )
        trigger_value, trigger_group_name = None, None
        action_sequence = []

        script_lines = text_script_parser.parse_lines(raw_lines)

        for line in script_lines:
            if line.author == base_author:
                action = Action(line.text, line.lang_group)
                action_sequence.append(action)
            else:
                branch = LinearScriptPath.from_action_sequence(action_sequence)
                script._merge_or_append(branch, trigger_value, trigger_group_name)
                action_sequence = []
                trigger_value, trigger_group_name = line.text, line.lang_group

        branch = LinearScriptPath.from_action_sequence(action_sequence)
        script._merge_or_append(branch, trigger_value, trigger_group_name)

        return script


def read_file(
    fp: str | TextIO,
    text_script_parser: Optional[ScriptParser] = None,
    action_comparator: Optional[StateActionComparator] = None,
    convo_name: Optional[str] = None,
    convo_description: Optional[str] = None,
    base_author: str = "teacher",
) -> Script:
    return Script.from_file(
        fp,
        text_script_parser=text_script_parser,
        action_comparator=action_comparator,
        convo_name=convo_name,
        convo_description=convo_description,
        base_author=base_author,
    )
