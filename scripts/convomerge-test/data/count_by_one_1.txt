teacher: Let's learn how to count by 1s. I will give you three numbers and you tell me what the next number is. If I say '2, 3, 4' then you should say '5'
teacher: 1, 2, 3?
student: 4
teacher: Perfect!
teacher: 11, 12, 13?
student: 14
teacher: Good job!
teacher: 97, 98, 99?
student: 101
teacher: Not quite. Try again.
student: 102
teacher: That's still not right.
teacher: Notice that 98 is one more than 97. And 99 is one more than 98.
teacher: So the answer to 97, 98, 99 is the number that is one more than 99.
teacher: 97, 98, 99?
student: 100
teacher: Excellent work!