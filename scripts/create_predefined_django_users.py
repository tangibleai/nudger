import django
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "convohub.settings")
django.setup()

from django.contrib.auth.models import User
from django.db.utils import IntegrityError

from convohub.constants import DEFAULT_PROJECT
from hub.models import Project, ProjectCollaborator
from users.models import Profile


try:
    default_project = Project.objects.filter(**DEFAULT_PROJECT).first()
except:
    print(f"Create the default Project instance first.")

try:
    super_user = User.objects.create_superuser(
        username=os.environ.get("DJANGO_SUPERUSER_USERNAME"),
        password=os.environ.get("DJANGO_SUPERUSER_PASSWORD"),
    )
    Profile.objects.create(user=super_user)
    print("Super user has been created!")
except IntegrityError as exc:
    print(f"{str(exc)}. Super user already exists!")


try:
    default_user = User.objects.create_user(
        username=os.environ.get("DJANGO_DEFAULT_USERNAME"),
        password=os.environ.get("DJANGO_DEFAULT_PASSWORD"),
    )  # to serve as AnonymousUser to bing it with default conversation
    Profile.objects.create(user=default_user)
    print("Default user has been created!")
except IntegrityError as exc:
    print(f"{str(exc)}. Default user already exists!")

default_user = User.objects.get(
    username=os.environ.get("DJANGO_DEFAULT_USERNAME"),
)

super_user = User.objects.get(
    username=os.environ.get("DJANGO_SUPERUSER_USERNAME"),
)

try:
    ProjectCollaborator.objects.create(user=super_user, project=default_project)
    ProjectCollaborator.objects.create(user=default_user, project=default_project)
except:
    print(f"Problem adding users to default Project instance")
