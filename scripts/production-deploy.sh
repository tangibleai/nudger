#!/bin/sh

echo "start running deploy script"

rsync -e "ssh -o StrictHostKeyChecking=no" -raz --progress ./ "root@${PRODUCTION_IP_ADDRESS}:convohub/"

ssh -o StrictHostKeyChecking=no root@$STAGING_IP_ADDRESS << 'ENDSSH'
  pwd

  cd convohub
  sudo systemctl restart gunicorn.service

ENDSSH


echo "Finished production-deploy.sh"
