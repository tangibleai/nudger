#!/usr/bin/env bash

python manage.py shell <<EOF

from django.contrib.auth.models import User

User.objects.create_superuser(
    username = '$1',
    password = '$2',
    email = 'engineering@tangibleai.com',
)

print("Finished creating superuser '$1' ")

exit()

EOF

echo "Finished $0 for superusername $1"
