import os

import django
from django.db.utils import IntegrityError

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "convohub.settings")
django.setup()

from hub.models import Project
from convohub.constants import DEFAULT_PROJECT


def get_or_create_default_project():
    try:
        project, created = Project.objects.get_or_create(**DEFAULT_PROJECT)
    except IntegrityError as exc:
        print(f"{str(exc)}. Default project already exists!")
    return project


if __name__ == "__main__":
    get_or_create_default_project()
