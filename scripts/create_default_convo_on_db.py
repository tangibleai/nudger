"""
Don't change the sequence of the imports.
`setdefault` and `django.setup` should be on the start!
"""
import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "convohub.settings")  # noqa
django.setup()  # noqa

import io
import logging
from pathlib import Path
import sys
import yaml

from django.core.files.base import ContentFile
from django.core.files.uploadedfile import InMemoryUploadedFile

from convohub.constants import (
    DEFAULT_CONVOGRAPH_YAML_PATH,
    DEFAULT_PROJECT,
)
from hub.convograph_validator import helpers
from hub.convograph_validator.convograph_validator_errors import (
    ConvoGraphValidationError,
)
from hub.utils import get_file_format_version
from hub.yaml_to_db_records.yaml_to_db_records import convoyaml_to_convomodel
from hub.models import Convo, Project
from django.conf import settings

log = logging.getLogger(__name__)


def delete_default_convo(default_project, convo_name):
    try:
        Convo.objects.all().filter(
            project=default_project, name=convo_name
        ).first().delete()
    except AttributeError:
        print("Couldn't delete default convo, it does not exists!")


def convert_to_inmemoryuploadedfile(text_io_wrapper, filename, content_type):
    content = text_io_wrapper.read()
    in_memory_uploaded_file = InMemoryUploadedFile(
        file=ContentFile(content),
        field_name=None,
        name=filename,
        content_type=content_type,
        size=len(content),
        charset=None,
    )
    return in_memory_uploaded_file


def create_convo(
    project,
    file_path,
    convo_name=None,
    convo_description=None,
):
    file_path = Path(file_path)
    conversation = None

    if not file_path.is_file():
        log.error(f"No file found: {file_path}")
        return
    elif not project:
        log.error("Create a default project first!")
        return
    error_msg = None
    try:
        conversation = yaml.safe_load(Path(file_path).read_text())
        format_version = get_file_format_version(conversation)
        convograph_validator = helpers.get_validator_by_version(format_version)
        convograph_validator(conversation).run()
    except ConvoGraphValidationError as exc:
        error_msg = f"{str(exc)} "

    if error_msg:
        error_msg = "Some issue occurred in default conversation! " + error_msg
        error_msg += "Use the format in the https://gitlab.com/tangibleai/convohub/-/blob/main/data/countByOne_0001.yml"
        log.error(error_msg)
        return
    if settings.USE_S3:
        with open(file_path, "rb") as f:
            file = ContentFile(f.read())
        file.name = file_path.name

    else:
        file = convert_to_inmemoryuploadedfile(
            io.TextIOWrapper(open(file_path, "rb")),
            file_path.name,
            "text/yaml",
        )
    convo_name = conversation[0].get("convo_name") if not convo_name else convo_name
    convo_description = (
        conversation[0].get("convo_description")
        if not convo_description
        else convo_description
    )
    convo_queryset = convoyaml_to_convomodel(
        graph=file,
        project=project,
        convo_name=convo_name,
        convo_description=convo_description,
    )
    log.info(
        f"Created convo:\n  file: {file}\n  project: {project}\n"
        + f"  convo_name: {convo_name}\n  description: {convo_description}"
    )
    return convo_queryset


if __name__ == "__main__":
    default_project = Project.objects.filter(**DEFAULT_PROJECT).first()
    path = DEFAULT_CONVOGRAPH_YAML_PATH
    default_convo_name = "demo"
    delete_default_convo(default_project, default_convo_name)
    if len(sys.argv) > 1:
        path = Path(sys.argv[1])
    convo_queryset = create_convo(default_project, path)
