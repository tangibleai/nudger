import requests
import json
import argparse

API_ENDPOINT = "http://localhost:8000/api/convo/post_file"


def send_file(file_path, username, is_private):
    file_content = read_file_content(file_path)
    if not file_content:
        print("File reading error. Check the file path.")
        return

    file_path_parts = file_path.split("/")
    filename = file_path_parts[-1]
    json_data = {
        "file": file_content,
        "username": username,
        "is_private": is_private,
        "filename": filename,
    }
    try:
        response = requests.post(API_ENDPOINT, json=json_data)
        # Print the response from the server
        print(response.text)
    except requests.RequestException as e:
        print(f"Error sending request: {e}")


def read_file_content(file_path):
    try:
        with open(file_path, "r") as file:
            file_content = file.read()
            return file_content
    except FileNotFoundError:
        print(f"File not found: {file_path}")
        return None


def main():
    parser = argparse.ArgumentParser(
        description="Send file content as JSON to an API endpoint"
    )
    parser.add_argument(
        "--file_path",
        type=str,
        help="Relative to this script path to the file to be sent",
        required=True,
    )
    parser.add_argument(
        "--username",
        type=str,
        help="User for whose project the conversation will be stored",
        required=True,
    )
    parser.add_argument(
        "--is_private",
        type=str,
        help="User for whose project the conversation will be stored",
        default=False,
    )
    args = parser.parse_args()

    send_file(args.file_path, args.username, args.is_private)


if __name__ == "__main__":
    main()
