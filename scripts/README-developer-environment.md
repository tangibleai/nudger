## Running Redis in a container
```shell
# Pull Redis from docker-hub
docker pull redis:latest
# Run a container from the Redis image
docker run --detach --publish 6379:6379 --name nudger-redis redis:latest
```

## To check if Redis is running
```shell
nmap localhost -p 6379
```

## Simplified installation steps for Linux
```shell

# clone the repository
git clone git@gitlab.com:tangibleai/community/nudger.git
cd nudger

# create a virtualenv and install packages
pip install --upgrade pip virtualenv
python -m virtualenv --python=3.10.1 .venv  # you can select another suitable version
source .venv/bin/activate
pip install .

# create a .env file

# create the tables on the to database
python manage.py migrate

# create staticfiles directory
echo "Collecting static files!"
python manage.py collectstatic --noinput -v 3

# Create default user
echo "Default user creation"
python -m scripts.create_predefined_django_users

# Creating the default convo if not exits
echo "Default convo creation"
python -m scripts.create_default_convo_on_db

# Start the redis and celery
# docker container start <redis-container-id>
./scripts/start-celery.sh &

# Start django application
python manage.py runserver localhost:8000
```
