# celery -A convohub.celery worker --loglevel=info --concurrency=2 -O fair &
# celery -A convohub.celery beat --loglevel=info --scheduler django_celery_beat.schedulers:DatabaseScheduler &

# echo `pwd`
if [ -f "./.env" ] ; then
    source ./.env
fi

if [ -f ".venv/bin/activate" ] ; then
    source .venv/bin/activate
fi

celery -A convohub.celery worker --beat --scheduler django --loglevel=info --concurrency 4
