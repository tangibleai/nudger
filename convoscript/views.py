import base64
import json
from collections import OrderedDict
from logging import getLogger

import yaml
from django.http import HttpResponse
from django.shortcuts import render

from django.http import JsonResponse

from hub.models import Convo, State, Trigger, Message


log = getLogger(__name__)

current_state_name = "name"
current_user_statement = "#1"
user_statements = ["#1"]
states = []

prev_state_name = ""
prev_states = []

blank_state_dict = {
    "State name": "",
    "Bot statement": "",
    "User statements": {
        1: {"User statement": "", "Is a button?": "No", "Next state": ""}
    },
}

blank_userstatement_dict = {
    "User statement": "",
    "Is a button?": "No",
    "Next state": "",
}

current_context = dict()

dictionary_of_states = dict()


def home(request):
    global current_user_statement
    global user_statements
    global current_context

    context = {
        "data": [1],
        "statename": "",
        "statenamereadonly": "",
        "botstatement": "",
        "userstatements": ["#1"],
        "userstatement": "",
        "isabutton": "",
        "nextstate": "",
        "states": states,
    }
    current_context = context

    return render(request, "convoscript/home.html", context)


def state_form_buttons(request):
    global current_state_name
    global dictionary_of_states
    global current_user_statement
    global user_statements
    global states
    global prev_state_name
    global prev_states
    global current_context

    context = {
        "data": [1],
        "statename": "",
        "statenamereadonly": "",
        "botstatement": "",
        "userstatements": user_statements,
        "userstatement": "",
        "isabutton": "",
        "nextstate": "",
        "states": states,
    }

    try:
        button_value = request.POST["button"]
        log.debug("button_value is " + button_value)
    except Exception as e:
        log.error(e)
        button_value = ""

    if button_value == "Add":
        log.debug("button clicked is Add")
        # state = State()
        save_current_state(request)
        newstatename = request.POST["newstatename"]
        if newstatename != "" and newstatename not in states:
            states.append(newstatename)
        log.debug("newstatename is " + newstatename)
        log.debug("states is " + str(states))
        print_dictionary_of_states()

        statename = request.POST["sname"]
        readonly = "readonly"
        if statename == "":
            readonly = ""
        botstatement = request.POST["botstatement"]
        log.debug("userstatement is " + request.POST["userstatement_dropdown"])
        id = (int)(request.POST["userstatement_dropdown"].replace("#", ""))
        total_statements = len(dictionary_of_states[statename]["User statements"])
        user_statements_array = ["#" + str(i) for i in range(1, total_statements + 1)]
        user_statements_array.remove(request.POST["userstatement_dropdown"])
        user_statements_array.insert(0, request.POST["userstatement_dropdown"])
        isabutton = dictionary_of_states[statename]["User statements"][id][
            "Is a button?"
        ]
        isabutton = "checked" if isabutton == "checked" else ""
        nextstate = dictionary_of_states[statename]["User statements"][id]["Next state"]
        try:
            list.sort(states)
            states.remove(nextstate)
            states.insert(0, nextstate)
        except Exception as e:
            log.error(e)

        context = {
            "data": [1],
            "statename": statename,
            "statenamereadonly": readonly,
            "botstatement": botstatement,
            "userstatements": user_statements_array,
            "userstatement": dictionary_of_states[statename]["User statements"][id][
                "User statement"
            ],
            "isabutton": isabutton,
            "nextstate": dictionary_of_states[statename]["User statements"][id][
                "Next state"
            ],
            "states": states,
        }
        current_user_statement = "#" + str(id)

    elif button_value == "Next state":
        log.debug("button clicked is Next state")
        save_current_state(request)
        nextstate = ""
        currentstate = ""
        try:
            currentstate = request.POST["sname"]
            nextstate = request.POST["nextstate"]
        except Exception as e:
            log.error(e)
        if nextstate != "" and currentstate != "":
            prev_states.append(currentstate)
            current_user_statement = "#1"
            if nextstate not in dictionary_of_states:
                dictionary_of_states[nextstate] = {
                    "State name": "",
                    "Bot statement": "",
                    "User statements": {
                        1: {"User statement": "", "Is a button?": "", "Next state": ""}
                    },
                }
                context = {
                    "data": [1],
                    "statename": nextstate,
                    "statenamereadonly": "readonly",
                    "botstatement": "",
                    "userstatements": ["#1"],
                    "userstatement": "",
                    "isabutton": "",
                    "nextstate": "",
                    "states": states,
                }
            else:
                total_user_statements = len(
                    dictionary_of_states[nextstate]["User statements"]
                )
                user_statements_array = [
                    "#" + str(i) for i in range(1, total_user_statements + 1)
                ]
                context = {
                    "data": [1],
                    "statename": nextstate,
                    "statenamereadonly": "readonly",
                    "botstatement": dictionary_of_states[nextstate]["Bot statement"],
                    "userstatements": user_statements_array,
                    "userstatement": dictionary_of_states[nextstate]["User statements"][
                        1
                    ]["User statement"],
                    "isabutton": dictionary_of_states[nextstate]["User statements"][1][
                        "Is a button?"
                    ],
                    "nextstate": dictionary_of_states[nextstate]["User statements"][1][
                        "Next state"
                    ],
                    "states": states,
                }

    elif button_value == "":
        log.debug("button clicked is 'Currently viewing statement'")
        log.debug("current_user_statement is " + current_user_statement)
        save_previous_state(request)

        currentlyviewingstatement = request.POST["userstatement_dropdown"]
        log.debug("currentlyviewingstatement is " + currentlyviewingstatement)
        if currentlyviewingstatement == "new":
            statename = request.POST["sname"]
            readonly = "readonly"
            if statename == "":
                readonly = ""
            botstatement = request.POST["botstatement"]
            new_id = len(dictionary_of_states[statename]["User statements"]) + 1
            dictionary_of_states[statename]["User statements"][new_id] = {
                "User statement": "",
                "Is a button?": "",
                "Next state": "",
            }

            user_statements_array = ["#" + str(i) for i in range(1, new_id + 1)]
            user_statements_array.remove("#" + str(new_id))
            user_statements_array.insert(0, "#" + str(new_id))

            context = {
                "data": [1],
                "statename": statename,
                "statenamereadonly": readonly,
                "botstatement": botstatement,
                "userstatements": user_statements_array,
                "userstatement": "",
                "isabutton": "",
                "nextstate": "",
                "states": states,
            }
            current_user_statement = "#" + str(new_id)

        else:
            statename = request.POST["sname"]
            readonly = "readonly"
            if statename == "":
                readonly = ""
            botstatement = request.POST["botstatement"]
            log.debug("userstatement is " + request.POST["userstatement_dropdown"])
            id = (int)(request.POST["userstatement_dropdown"].replace("#", ""))
            total_statements = len(dictionary_of_states[statename]["User statements"])
            user_statements_array = [
                "#" + str(i) for i in range(1, total_statements + 1)
            ]
            user_statements_array.remove(request.POST["userstatement_dropdown"])
            user_statements_array.insert(0, request.POST["userstatement_dropdown"])
            isabutton = dictionary_of_states[statename]["User statements"][id][
                "Is a button?"
            ]
            isabutton = "checked" if isabutton == "checked" else ""
            nextstate = dictionary_of_states[statename]["User statements"][id][
                "Next state"
            ]
            try:
                list.sort(states)
                states.remove(nextstate)
                states.insert(0, nextstate)
            except Exception as e:
                log.error(e)
            context = {
                "data": [1],
                "statename": statename,
                "statenamereadonly": readonly,
                "botstatement": botstatement,
                "userstatements": user_statements_array,
                "userstatement": dictionary_of_states[statename]["User statements"][id][
                    "User statement"
                ],
                "isabutton": isabutton,
                "nextstate": dictionary_of_states[statename]["User statements"][id][
                    "Next state"
                ],
                "states": states,
            }
            current_user_statement = "#" + str(id)
        print_dictionary_of_states()
    elif button_value == "Prev state":
        log.debug("Prev state")
        if len(prev_states) > 0:
            save_current_state(request)
            current_user_statement = "#1"
            prev_state = prev_states.pop(len(prev_states) - 1)
            total_statements = len(dictionary_of_states[prev_state]["User statements"])
            user_statements_array = [
                "#" + str(i) for i in range(1, total_statements + 1)
            ]
            nextstate = dictionary_of_states[prev_state]["User statements"][1][
                "Next state"
            ]
            try:
                list.sort(states)
                states.remove(nextstate)
                states.insert(0, nextstate)
            except Exception as e:
                log.error(e)
            context = {
                "data": [1],
                "statename": prev_state,
                "statenamereadonly": "readonly",
                "botstatement": dictionary_of_states[prev_state]["Bot statement"],
                "userstatements": user_statements_array,
                "userstatement": dictionary_of_states[prev_state]["User statements"][1][
                    "User statement"
                ],
                "isabutton": dictionary_of_states[prev_state]["User statements"][1][
                    "Is a button?"
                ],
                "nextstate": dictionary_of_states[prev_state]["User statements"][1][
                    "Next state"
                ],
                "states": states,
            }

    current_context = context
    return render(request, "convoscript/home.html", context)


def print_dictionary_of_states():
    print()
    print()
    print(json.dumps(dictionary_of_states, indent=4))
    print()
    print()


def save_current_state(request):
    global current_user_statement
    if "" in dictionary_of_states:
        dictionary_of_states.pop("")
    statename = request.POST["sname"]
    botstatement = request.POST["botstatement"]
    currentlyviewingstatement = request.POST["userstatement_dropdown"]
    if currentlyviewingstatement != "new":
        currentlyviewingstatement_id = (int)(currentlyviewingstatement.replace("#", ""))
    userstatement = request.POST["userstatement"]
    isabutton = ""
    try:
        isabutton = request.POST["yesorno"]
    except Exception as e:
        log.error(e)
    nextstate = ""
    try:
        nextstate = str(request.POST["nextstate"])
    except Exception as e:
        log.error(e)
    log.debug("statename is " + statename)
    log.debug("botstatement is " + botstatement)
    log.debug("currentlyviewingstatement is " + currentlyviewingstatement)
    log.debug("userstatement is " + userstatement)
    log.debug("isabutton is " + isabutton)
    log.debug("nextstate is " + nextstate)

    if statename not in dictionary_of_states:
        dictionary_of_states[statename] = {
            "State name": "",
            "Bot statement": "",
            "User statements": {
                1: {"User statement": "", "Is a button?": "", "Next state": ""}
            },
        }

    dictionary_of_states[statename]["State name"] = statename
    if statename not in states:
        states.append(statename)
        list.sort(states)
    dictionary_of_states[statename]["Bot statement"] = botstatement
    if currentlyviewingstatement != "new":
        dictionary_of_states[statename]["User statements"][
            currentlyviewingstatement_id
        ]["User statement"] = userstatement
        dictionary_of_states[statename]["User statements"][
            currentlyviewingstatement_id
        ]["Is a button?"] = isabutton
        dictionary_of_states[statename]["User statements"][
            currentlyviewingstatement_id
        ]["Next state"] = nextstate

    print_dictionary_of_states()


def save_previous_state(request):
    global current_user_statement
    if "" in dictionary_of_states:
        dictionary_of_states.pop("")
    statename = request.POST["sname"]
    botstatement = request.POST["botstatement"]
    currentlyviewingstatement = current_user_statement
    if currentlyviewingstatement != "new":
        currentlyviewingstatement_id = (int)(currentlyviewingstatement.replace("#", ""))
    userstatement = request.POST["userstatement"]
    isabutton = ""
    try:
        isabutton = request.POST["yesorno"]
    except Exception as e:
        log.error(e)
    nextstate = ""
    try:
        nextstate = request.POST["nextstate"]
    except Exception as e:
        log.error(e)
    log.debug("statename is " + statename)
    log.debug("botstatement is " + botstatement)
    log.debug("currentlyviewingstatement is " + currentlyviewingstatement)
    log.debug("userstatement is " + userstatement)
    log.debug("isabutton is " + isabutton)
    log.debug("nextstate is " + nextstate)

    if statename not in dictionary_of_states:
        dictionary_of_states[statename] = {
            "State name": "",
            "Bot statement": "",
            "User statements": {
                1: {"User statement": "", "Is a button?": "No", "Next state": ""}
            },
        }

    dictionary_of_states[statename]["State name"] = statename
    if statename not in states:
        states.append(statename)
        list.sort(states)
    dictionary_of_states[statename]["Bot statement"] = botstatement
    if currentlyviewingstatement != "new":
        dictionary_of_states[statename]["User statements"][
            currentlyviewingstatement_id
        ]["User statement"] = userstatement
        dictionary_of_states[statename]["User statements"][
            currentlyviewingstatement_id
        ]["Is a button?"] = isabutton
        dictionary_of_states[statename]["User statements"][
            currentlyviewingstatement_id
        ]["Next state"] = nextstate

    print_dictionary_of_states()


###################################################################################################
# This part deals with the "Download yaml file!" button


def represent_dictionary_order(self, dict_data):
    return self.represent_mapping("tag:yaml.org,2002:map", dict_data.items())


def setup_yaml():
    yaml.add_representer(OrderedDict, represent_dictionary_order)


setup_yaml()


def export(request):
    global current_context
    global dictionary_of_states

    log.debug("download yaml file")
    log.debug(dictionary_of_states)

    array_of_states_in_yaml_format = []
    for name in dictionary_of_states:
        state = OrderedDict()
        state["name"] = dictionary_of_states[name]["State name"]
        state["actions"] = dictionary_of_states[name]["Bot statement"]
        state["triggers"] = OrderedDict()
        for userstatement in dictionary_of_states[name]["User statements"]:
            state["triggers"][
                dictionary_of_states[name]["User statements"][userstatement][
                    "User statement"
                ]
            ] = dictionary_of_states[name]["User statements"][userstatement][
                "Next state"
            ]

        log.debug(state)
        array_of_states_in_yaml_format.append(state)

    log.debug(array_of_states_in_yaml_format)

    with open("generated-dialog.yml", "w") as outfile:
        yaml.dump(array_of_states_in_yaml_format, outfile, default_flow_style=False)
    outfile.close()

    #####################################################################################################

    filename = "generated-dialog.yml"  # this is the file people must download
    with open(filename, "rb") as f:
        response = HttpResponse(f.read(), content_type="application/vnd.ms-excel")
        response["Content-Disposition"] = "attachment; filename=" + filename
        response["Content-Type"] = "application/vnd.ms-excel; charset=utf-16"
        return response


#################################################################################################


def populate_form(request):
    obj = request.GET.get("obj", "")
    base64_bytes = obj.encode("ascii")
    message_bytes = base64.b64decode(base64_bytes)
    message = message_bytes.decode("ascii")
    obj = json.loads(message)
    log.debug("test is " + str(obj["statename"]))
    context = {
        "data": [1],
        "statename": obj["statename"],
        "statenamereadonly": "test",
        "botstatement": obj["botstatement"],
        "userstatements": user_statements,
        "userstatement": "",
        "isabutton": "",
        "nextstate": "",
        "states": states,
    }

    return render(request, "convoscript/home.html", context)


def convo(request):
    current_user = request.user

    active_convo = (
        Convo.objects.filter(user__id=current_user.id).order_by("-activated_on").first()
    )

    convo_id = request.GET.get("convo_id", active_convo.id)

    states = State.objects.filter(convo=convo_id)
    triggers = Trigger.objects.filter(from_state__in=states)
    state_and_trigger_data = []
    for i, state in enumerate(states):
        messages = Message.objects.filter(state=state)
        triggers = Trigger.objects.filter(from_state=state)
        state_and_trigger_data.append(
            {
                "name": state.state_name,
                "convo_name": active_convo.name,
                "convo_description": active_convo.description,
                "actions": {"en": [message.bot_text for message in messages]},
                "triggers": {
                    "en": {
                        triggers.intent_text: triggers.to_state.state_name
                        for triggers in triggers
                    }
                },
                "update_args": state.update_args,
                "update_kwargs": state.update_kwargs,
                "update_context": state.update_context,
                "level": state.level,
            }
        )

    return JsonResponse(state_and_trigger_data, safe=False)
