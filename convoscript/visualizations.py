from pathlib import Path
import sys
import yaml

from pyvis.network import Network


def generate_network(yaml_path, keys=["triggers", "buttons"]):
    """Generate HTML for network (graph) diagram based on conversation flow yaml file"""
    net = Network(directed=True)

    with open(yaml_path, "r") as stream:
        states = yaml.safe_load(stream)

    name2id = {d["name"].lower().strip(): idx for (idx, d) in enumerate(states)}
    for node_id, state in enumerate(states):
        name = state["name"].lower().strip()
        net.add_node(node_id, label=name, title=state["actions"]["en"])
    for src_id, src_state in enumerate(states):
        en_triggers = list(src_state.get("triggers", {"en": {}})["en"].items())
        en_triggers += list(src_state.get("buttons", {"en": {}})["en"].items())

        for utterance, dst_name in en_triggers:
            dst_name = dst_name.lower().strip()
            if dst_name not in name2id:
                net.add_node(
                    node_id,
                    label=dst_name,
                    title=dst_name,
                    description=f'Null state with name "{dst_name}"',
                )
            net.add_edge(src_id, name2id[dst_name])
    return net


def expand(data, key, nt, count, start_count):
    # adds nodes and edges for keys in yaml data
    if key in data.keys():
        for j in data[key].keys():
            for j in data[key].keys():
                for x in data[key][j]:
                    count += 1
                    nt.add_node(count, label=data[key][j][x], title=data[key][j][x])
                    nt.add_edge(start_count, count)
    return nt, count


if __name__ == "__main__":
    yaml_path = Path(__file__).parent.parent / "data" / "countByOne_0002.yml"
    if sys.argv[1:] and Path(sys.argv[1]).is_file():
        yaml_path = Path(sys.argv[1])
    net = generate_network(yaml_path)
    net.save_graph(str(yaml_path.with_suffix(".html")))
