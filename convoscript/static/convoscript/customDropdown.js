function customDropdown() {
  window.addEventListener("load", () => {
    // document.body.classList.remove("custom-dropdown");
    // document.body.classList.add("custom-dropdown");

    const customDropdownList = document.querySelectorAll(".custom-dropdown");

    customDropdownList.forEach((dropdown) => {
      const customOptionList = dropdown.querySelectorAll(
        ".custom-dropdown-option"
      );

      customOptionList.forEach((option) => {
        option.addEventListener("mouseover", () => {
          highlightCustomDropdownOption(dropdown, option);
        });
      });

      dropdown.addEventListener("click", (event) => {
        toggleCustomDropdownOptionsList(dropdown);
      });

      dropdown.addEventListener("focus", (event) => {
        activeCustomDropdown(dropdown, customDropdownList);
      });

      dropdown.addEventListener("blur", (event) => {
        deactivateCustomDropdown(dropdown);
      });

      dropdown.addEventListener("keyup", (event) => {
        if (event.key === "Escape") {
          deactivateCustomDropdown(dropdown);
        }
      });
    });
  });

  function deactivateCustomDropdown(customDropdown) {
    if (!customDropdown.classList.contains("active")) return;

    const customOptionsList = customDropdown.querySelector(
      ".custom-dropdown-options"
    );

    customOptionsList.classList.add("hidden");

    customDropdown.classList.remove("active");
  }

  function activeCustomDropdown(customDropdown, customDropdownOptions) {
    if (customDropdown.classList.contains("active")) return;

    customDropdownOptions.forEach(deactivateCustomDropdown);

    customDropdown.classList.add("active");
  }

  function toggleCustomDropdownOptionsList(customDropdown) {
    const customDropdownOptionsList = customDropdown.querySelector(
      ".custom-dropdown-options"
    );

    customDropdownOptionsList.classList.toggle("hidden");
  }

  function highlightCustomDropdownOption(customDropdown, customDropdownOption) {
    const customDropdownOptionsList = customDropdown.querySelectorAll(
      ".custom-dropdown-option"
    );

    customDropdownOptionsList.forEach((option) => {
      option.classList.remove("highlight");
    });

    customDropdownOption.classList.add("highlight");
  }
}
