function getConvo(convo_id) {
    if (convo_id === 'new') {
      // Handle creating a new conversation
      return;
    }
  
    // Make an AJAX request to fetch the conversation data
    fetch(`/get_convo/${convo_id}/`)
      .then(response => response.json())
      .then(context => {
        // Populate the form with the conversation data
        document.getElementById('statement').value = context.statement;
        document.getElementById('response').value = context.response;
  
        // Populate the graph with the conversation data
        drawGraph(data.graph);
      })
      .catch(error => console.error(error));
  }

  export default getConvo;