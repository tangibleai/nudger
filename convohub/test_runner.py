# myproject/test_runner.py

from collections.abc import Sequence
from typing import Any
from unittest import TestSuite
from django.test.runner import DiscoverRunner
import doctest


class CustomTestRunner(DiscoverRunner):
    def run_tests(self, test_labels, extra_tests=None, **kwargs):
        # print("ENTER")
        # print(kwargs)
        # print(test_labels)
        # print([app for app in test_labels])
        # app = __import__("users")
        # print(app.__dict__["models"])
        # results = doctest.testmod(app.__dict__["models"])
        # print(results.failed)
        # module = app.__dict__["users"]
        # print(module)
        super().run_tests(test_labels, extra_tests, **kwargs)

        # Discover and run doctests
        for app_name in test_labels:
            app = __import__(app_name)
            module = app.__dict__[app_name]
            results = doctest.testmod(module)
            if results.failed == 0:
                print(f"All doctests in {app_name} passed.")
            else:
                print(f"{results.failed} doctests in {app_name} failed.")

    def build_suite(self, test_labels, extra_tests, **kwargs):
        print("TEST LABELS", test_labels)
        return super().build_suite(test_labels, extra_tests, **kwargs)
