"""
ASGI config for convohub project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.0/howto/deployment/asgi/
"""
import os

from channels.routing import ProtocolTypeRouter, URLRouter
from channels.security.websocket import AllowedHostsOriginValidator
from django.core.asgi import get_asgi_application
from channels.auth import AuthMiddlewareStack

# import chat.routing
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "convohub.settings")

django_asgi_app = get_asgi_application()

import hub.routing

# AuthMiddlewareStack needed to authentication for websockets
application = ProtocolTypeRouter(
    {
        "http": django_asgi_app,
        "websocket": AllowedHostsOriginValidator(
            AuthMiddlewareStack(
                URLRouter(
                    [
                        *hub.routing.websocket_urlpatterns,
                    ]
                ),
            )
        ),
    }
)
