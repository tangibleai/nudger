# constants.py
# import json
import os
import logging
import pandas as pd

from dotenv import load_dotenv
from pathlib import Path

log = logging.getLogger(__name__)
load_dotenv()
ENV = dict(os.environ)

BASE_DIR = Path(__file__).resolve().parent.parent
DATA_DIR = ENV.get("DATA_DIR", None)
if DATA_DIR is None:
    DATA_DIR = Path(__file__).resolve().parent / "data"
if not DATA_DIR.is_dir():
    DATA_DIR.mkdir()
DEFAULT_CONVOGRAPH_YAML_PATH = DATA_DIR / "v3" / "working_demo.yml"


SPACY_MODEL = ENV.get("SPACY_MODEL", "en_core_web_md")

START_STATE_NAME = "start"
STOP_STATE_NAME = "stop"
ALLOWED_ACTIONS = "extract_lang extract_proper_nouns send_message send_triggers extract_user_text send_image execute_code extract_urls".split()

USER_TEXT_KEYS = "user_text user_utterance text utterance".split()


# FIXME: Change the duplicate keys as necessary
YAML_FORMAT_VERSION_TO_ASSETS_MAPPING = {
    "default": {
        "required_state_attrs": ("name", "actions", "triggers"),
        "allowed_state_attrs": ("name", "actions", "triggers"),
    },
    "3": {
        "required_state_attrs": ("name", "actions", "triggers"),
        "allowed_state_attrs": ("name", "actions", "triggers"),
    },
    "2": {
        "required_state_attrs": ("name", "actions", "triggers"),
        "allowed_state_attrs": ("name", "actions", "triggers", "buttons"),
    },
}


# Hub.consumers
RESPONSE_DELAY_PER_CHAR = 0.05  # Unused?
RESPONSE_DELAY_MIN = 0.2  # Unused?


# hub.models
MESSAGE_DIRECTION_CHOICES = [  # Unused?
    ("inbound", "inbound"),
    ("outbound", "outbound"),
    ("unknown", "unknown"),
]

SENDER_TYPE_CHOICES = [  # Unused?
    ("user", "user"),
    ("chatbot", "chatbot"),
    ("human agent", "human agent"),
]


# Language variables ##########################
# FIXME: Is there a way to consolidate these?

LANG_MAPPINGS = dict(
    en="en",
    es="es",
    uk="uk",
    fr="fr",
    english="en",
    spanish="es",
    french="fr",
    ukranian="uk",
)

# LANGUAGES = ENV.get("LANGUAGES", LANG_MAPPINGS.keys())
# assert all(x in LANG_MAPPINGS for x in LANGUAGES)
# dict_keys(['en', 'es', 'uk', 'fr', 'english', 'spanish', 'french', 'ukranian'])


# hub.models
LANGUAGE_CHOICES = [
    ("en", "en"),
    ("es", "es"),
    ("zh-hans", "zh-hans"),
    ("zh-hant", "zh-hant"),
]

# hub.load_yaml_v3   # Used in commented out code?
# hub.load_yaml
LANGS = {
    "English": "en",
    "Spanish (Español)": "es",
    "Chinese - Simplified (简体中文)": "zh-hans",
    "Chinese - Traditional (繁體中文)": "zh-hant",
}
# {'English': 'en', 'Spanish (Español)': 'es', 'Chinese - Simplified (简体中文)': 'zh-hans', 'Chinese - Traditional (繁體中文)': 'zh-hant'}

###################################################


# users.models
DEFAULT_BOT_WIDGET_IMAGE_PATH = "bot_widget_pics/default.jpg"
DEFAULT_PROFILE_IMAGE_PATH = "profile_pics/default.jpg"
try:
    with open(DATA_DIR / "constants" / "language_code.csv") as fin:
        df = pd.read_csv(fin)
        df.columns = ["language", "code"]
        df["language"] = (
            df["language"]
            .str.replace("(", " ", regex=False)
            .str.replace(")", "", regex=False)
        )
        LANG_MAPPINGS.update(dict(zip(df.language.str.split().str[0], df.code)))
        LANG_MAPPINGS.update(dict(zip(df.language.str.split().str[-1], df.code)))
        LANG_MAPPINGS.update(
            dict(zip([x.lower() for x in LANG_MAPPINGS.keys()], LANG_MAPPINGS.values()))
        )
except Exception as e:
    log.error(e)


DEFAULT_CONVO_NAME = "default"
DEFAULT_CONVOGRAPH_YAML_PATH = DATA_DIR / "v3" / "working_demo.yml"

DJANGO_DEFAULT_USERNAME = ENV.get("DJANGO_DEFAULT_USERNAME", "b")
DEFAULT_PROJECT = {
    "public_name": "Default project",
    "public_description": "Your first chatbot",
}
