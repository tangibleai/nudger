"""convohub URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('convoscript/', include('convoscript.urls'))
"""
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path, include, re_path

import hub.urls
from users import views as user_views
import convoscript.urls

from django.contrib.staticfiles.storage import staticfiles_storage
from django.views.generic.base import RedirectView

# Suitable for development, not production
from django.conf.urls.static import static
from django.views.static import serve
from django.conf import settings
from users.forms import CustomLoginForm
from users.views import login_view, logout_view

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", include("users.urls")),
    path(
        "login/",
        login_view,
        name="login",
    ),
    path(
        "logout/",
        logout_view,
        name="logout",
    ),
    path("api/", include("api.urls")),
    path("profile/", user_views.update_user_and_user_profile, name="profile"),
    path("signup/", user_views.create_user_and_user_profile, name="signup"),
    path(
        "qary-7.ico",
        RedirectView.as_view(url=staticfiles_storage.url("qary-7.ico")),
    ),
    path("quiz/", include(hub.urls)),
    path("convoscript/", include(convoscript.urls)),
    path("healthcheck.json", user_views.healthcheck, name="healthcheck"),
    path("accounts/", include("allauth.urls")),
]

if not settings.USE_S3:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += [
        re_path(
            r"^media/(?P<path>.*)$",
            serve,
            {
                "document_root": settings.MEDIA_ROOT,
            },
        ),
    ]
