""" Cleans and validates environment variables so they are coerced into the types required by settings.py

SEE ALSO: `class Settings` in [django-pydantic-settings](https://pypi.org/project/django-pydantic-settings/)
"""
import os

from dotenv import load_dotenv


def clean_env_dict(
    env_dict=None,
    path_variable_suffixes=("URL", "NAME", "ENDPOINT", "PATH", "DIR"),
):
    """Ensures that all variables in the dict do not contain trailing or leading slashes

    >>> clean_paths(dict(PATH='/what/ever/', DATA_DIR='c/data///'))
    {'PATH': 'what/ever', 'DATA_DIR': 'c/data'}
    >>> any([
    ...     v.endswith('/') or v.startswith('/')
    ...     for v in clean_paths(os.environ).values()
    ...     ])
    False
    """
    if env_dict is None:
        load_dotenv()
        env_dict = os.environ

    env_dict = dict(env_dict)
    if path_variable_suffixes:
        for k, v in env_dict.items():
            if isinstance(k, str) and k.split("_")[-1] in path_variable_suffixes:
                env_dict[k] = env_dict[k].strip("/")
    return env_dict
