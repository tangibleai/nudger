import json

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view
from django.contrib.auth.models import User


@csrf_exempt
@api_view(["GET"])
def get_authenticated_user(request):
    user_id = request.session.get("user_id")
    user = User.objects.get(id=user_id)
    return JsonResponse({"id": user_id, "username": user.username})
