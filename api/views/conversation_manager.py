import json
import yaml

from django.http import JsonResponse
from rest_framework.views import APIView

from hub.models import Convo, Project
from convohub.constants import DEFAULT_PROJECT, DJANGO_DEFAULT_USERNAME
from django.contrib.auth.models import User
from hub.conversation_manager.v3_1.conversation_manager import ApiConversation
from hub.utils import get_project


class Chat(APIView):
    def __init__(self):
        """Show variables used during every request processing"""
        self.user = None
        self.context = dict()

    def get(self, request):
        """Serves get request"""
        payload = request.data or request.query_params.dict()
        context = payload["context"] or {}
        self.context = json.loads(context) if type(context) == str else context

        self.save_username_in_context()
        self.save_convo_id_in_context()
        self.conversation_runner_update_context()

        response = {"context": self.context}
        return JsonResponse(response, safe=False)

    def save_username_in_context(self):
        username = self.context["username"]
        self.user = User.objects.filter(username=username).first() or User.objects.get(
            username=DJANGO_DEFAULT_USERNAME
        )
        self.context["username"] = self.user.username
        return self.context

    def save_convo_id_in_context(self):
        if "convo_id" in self.context:
            return self.context
        project = get_project(self.user)
        convo = Convo.objects.filter(project=project).order_by("-activated_on").first()
        self.context["convo_id"] = convo.id
        return self.context

    def save_lang_in_context(self):
        if "lang" in self.context:
            return self.context
        convo_id = self.context["convo_id"]
        convo = Convo.objects.get(id=convo_id)
        yml_conversation = yaml.safe_load(convo.file.read())
        lang = self.extract_lang_from_start_state(yml_conversation)
        self.context["lang"] = lang
        return self.context

    def extract_lang_from_start_state(self, yml_conversation):
        for state_data in yml_conversation:
            state_name = state_data["name"]
            if state_name == "start":
                return state_data["lang"]
        return "en"

    def conversation_runner_update_context(self):
        conversation_runner = ApiConversation(self.context)
        context_diff = conversation_runner.fill_context_with_next_state()
        self.context.update(context_diff)
        return

    def get_convo_id_by_conversation_url(self, url):
        """Given a url following which unlogged user can have a conversation return a related Convo record id"""
        default_convo = (
            Convo.objects.filter(project=Project.objects.get(**DEFAULT_PROJECT))
            .order_by("-activated_on")
            .first()
        )
        convo_id = default_convo.id
        if "/quiz/bothub" in url:
            convo_id = int(url.split("/")[-1])
        elif "/~/" in url:
            project_name = url.split("/~/")[-1]
            project = Project.objects.filter(public_name=project_name).first()

            convo = (
                Convo.objects.filter(project=project).order_by("-activated_on").first()
            )
            convo_id = convo.id
        return convo_id
