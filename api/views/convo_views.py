import io
import yaml

from datetime import datetime

from django.core.files.base import ContentFile
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view

from hub import serializers
from hub.models import Convo, ProjectCollaborator
from hub.models_convomeld import merge_convos
from hub.utils import get_file_format_version
from hub.convograph_validator import helpers
from hub.yaml_to_db_records.yaml_to_db_records import convoyaml_to_convomodel


@csrf_exempt
@api_view(["GET"])
def get_convo_list(request):
    payload = request.data or request.query_params.dict()
    user_id = request.session.get("user_id", payload.get("user_id", None))
    is_list_public = payload.get("is_list_public", True) in ("true", "True", True)

    try:
        project_collaborator = ProjectCollaborator.objects.filter(
            user__id=user_id
        ).first()
        project = project_collaborator.project
    except:
        is_list_public = True

    query_convo_by = {}
    if not is_list_public:
        query_convo_by["project"] = project
    else:
        query_convo_by = {"is_public": is_list_public}
    convos = [
        c for c in Convo.objects.filter(**query_convo_by).order_by("-activated_on")
    ]
    serialized_convos = [serializers.serialize_convo(c) for c in convos]
    return JsonResponse(serialized_convos, safe=False)


@csrf_exempt
@api_view(["POST"])
def merge_action(request):
    payload = request.data or request.query_params.dict()
    user_id = request.session.get("user_id", payload.get("user_id", None))
    user = User.objects.get(id=user_id)

    project_collaborator = ProjectCollaborator.objects.filter(user=user).first()

    convo_ids = payload.get("convo_ids", [])
    if len(convo_ids) == 1:
        return JsonResponse({})
    convos = [Convo.objects.get(id=id) for id in convo_ids]
    merged_convo_name = payload.get("name", " + ".join([c.name for c in convos]))
    merged_convo_description = payload.get(
        "description", f"Merged {' + '.join([c.name for c in convos])}"
    )
    merged_convo = merge_convos(
        project_collaborator.project,
        merged_convo_name,
        merged_convo_description,
        *convos,
    )
    return JsonResponse(serializers.serialize_convo(merged_convo), safe=False)


def toggle_convos_public_affiliation(convo_ids, is_public):
    convos = set()
    for id in convo_ids:
        convo = Convo.objects.get(id=id)
        convo.is_public = True if is_public else False
        convo.save()
        convos.add(convo)
    ordered_convos = sorted(convos, key=lambda c: c.activated_on, reverse=True)
    serialized_convos = [serializers.serialize_convo(c) for c in ordered_convos]
    return serialized_convos


@csrf_exempt
@api_view(["PUT"])
def make_public_action(request):
    payload = request.data or request.query_params.dict()
    convo_ids = payload.get("convo_ids", [])
    published_convos = toggle_convos_public_affiliation(convo_ids, True)
    return JsonResponse(published_convos, safe=False)


@csrf_exempt
@api_view(["PUT"])
def make_private_action(request):
    payload = request.data or request.query_params.dict()
    convo_ids = payload.get("convo_ids", [])
    unpublished_convos = toggle_convos_public_affiliation(convo_ids, False)
    return JsonResponse(unpublished_convos, safe=False)


def get_query_by_attributes():
    search_by = [
        "id",
        "name",
        "description",
        "project__public_name",
    ]
    for i, item in enumerate(search_by.copy()):
        search_by[i] = item + "__contains"
    return search_by


@csrf_exempt
@api_view(["GET"])
def search(request):
    payload = request.data or request.query_params.dict()
    user_id = request.session.get("user_id", payload.get("user_id", None))

    project_collaborator = ProjectCollaborator.objects.filter(user__id=user_id).first()
    project = project_collaborator.project

    search_value = payload.get("search", [])
    is_list_public = payload.get("is_list_public", True) in ("True", "true", True)
    search_by = get_query_by_attributes()
    unique_convos = set()
    query_convo_by = {}
    if not is_list_public:
        query_convo_by["project"] = project
    else:
        query_convo_by = {"is_public": is_list_public}
    for item in search_by:
        query_convo_by[item] = search_value
        convos = Convo.objects.filter(**query_convo_by)
        if convos:
            for c in convos:
                unique_convos.add(c)
        del query_convo_by[item]
    ordered_convos = sorted(unique_convos, key=lambda c: c.activated_on, reverse=True)
    serialized_convos = [serializers.serialize_convo(c) for c in ordered_convos]
    return JsonResponse(serialized_convos, safe=False)


def convert_str_date_to_datetime(js_date_string: str):
    """
    >>> convert_str_date_to_datetime("Wed Jul 28 1993")
    1993-07-28 00:00:00
    """
    js_date_format = "%a %b %d %Y"

    python_datetime = datetime.strptime(js_date_string, js_date_format)
    return python_datetime


@csrf_exempt
@api_view(["GET"])
def filter(request):
    payload = request.data or request.query_params.dict()
    user_id = request.session.get("user_id", payload.get("user_id", None))

    project_collaborator = ProjectCollaborator.objects.filter(user__id=user_id).first()
    project = project_collaborator.project

    is_list_public = payload.get("is_list_public", True) in ("True", "true", True)
    filter_by = {}
    if not is_list_public:
        filter_by["project"] = project
    else:
        filter_by = {"is_public": is_list_public}
    for filter in get_query_by_attributes():
        for arg, val in payload.items():
            if filter.startswith(arg):
                filter_by[filter] = val
                break
    activated_on_range = [None, None]
    if ("start_date", "end_date") <= tuple(payload):
        activated_on_range[0] = convert_str_date_to_datetime(payload["start_date"])
        activated_on_range[1] = convert_str_date_to_datetime(payload["end_date"])
        filter_by["activated_on__range"] = activated_on_range
    convos = set()
    for c in Convo.objects.filter(**filter_by):
        convos.add(c)
    ordered_convos = sorted(convos, key=lambda c: c.activated_on, reverse=True)
    serialized_convos = [serializers.serialize_convo(c) for c in ordered_convos]
    return JsonResponse(serialized_convos, safe=False)


@csrf_exempt
@api_view(["PUT"])
def activate_convo(request):
    payload = request.data or request.query_params.dict()
    convo_id = payload.get("convo_id", None)
    convo = Convo.objects.get(id=convo_id)
    convo.activated_on = datetime.now()
    convo.save()
    return JsonResponse(serializers.serialize_convo(convo), safe=False)


@csrf_exempt
@api_view(["POST"])
def clone_convo(request):
    payload = request.data or request.query_params.dict()
    convo_id = payload.get("convo_id", None)
    visitor_id = payload.get("visitor_id", None)
    visitor = User.objects.get(id=visitor_id)
    project_collaborator = ProjectCollaborator.objects.filter(user=visitor).first()
    project = project_collaborator.project
    convo = Convo.objects.get(id=convo_id)
    cloned_convo = convo
    cloned_convo.pk = None  # https://docs.djangoproject.com/en/4.2/topics/db/queries/#copying-model-instances
    cloned_convo.project = project
    cloned_convo.name = cloned_convo.name + "_cloned"
    cloned_convo.save()
    return JsonResponse("ok", safe=False)


@csrf_exempt
@api_view(["GET"])
def does_user_own_convo(request):
    payload = request.data or request.query_params.dict()
    convo_id = payload.get("convo_id", None)
    visitor_id = payload.get("visitor_id", None)
    visitor = User.objects.get(id=visitor_id)
    project_collaborator_records = ProjectCollaborator.objects.filter(user=visitor)
    if not project_collaborator_records:
        return JsonResponse(False, safe=False)
    for record in project_collaborator_records:
        project = record.project
        convo = Convo.objects.filter(id=convo_id, project=project)
        if convo:
            return JsonResponse(True, safe=False)
    return JsonResponse(False, safe=False)


@csrf_exempt
@api_view(["POST"])
def post_file(request):
    payload = request.data or request.query_params.dict()
    username = payload["username"]
    is_private = payload["is_private"]
    filename = payload["filename"]
    user = User.objects.get(username=username)
    project_collaborator = ProjectCollaborator.objects.get(user=user)
    is_public = not is_private

    file_as_str = payload["file"]
    file_as_yml = yaml.safe_load(file_as_str)
    format_version = get_file_format_version(file_as_yml)
    convograph_validator = helpers.get_validator_by_version(format_version)
    convograph_validator(file_as_yml).run()
    start_state_data = get_start_state_data(file_as_yml)
    convo_name = start_state_data.get("convo_name")
    convo_description = start_state_data.get("convo_description")

    in_memory_uploaded_file = create_in_memory_uploaded_file(
        yaml.dump(file_as_yml), filename
    )
    convo = convoyaml_to_convomodel(
        graph=in_memory_uploaded_file,
        project=project_collaborator.project,
        convo_name=convo_name,
        convo_description=convo_description,
        is_public=is_public,
    )
    return JsonResponse(convo.id, safe=False)


def get_start_state_data(file_as_yml):
    for state_data in file_as_yml:
        state_name = state_data["name"]
        if state_name == "start":
            return state_data


def create_in_memory_uploaded_file(yaml_content, file_name="uploaded_file.yaml"):
    yaml_bytes = yaml_content.encode("utf-8")
    content_file = ContentFile(yaml_bytes, name=file_name)
    return content_file
