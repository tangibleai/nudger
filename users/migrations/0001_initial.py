# Generated by Django 4.1.3 on 2023-06-26 07:25

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name="Profile",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "convo_name",
                    models.CharField(
                        blank=True, default="default", max_length=100, null=True
                    ),
                ),
                (
                    "profile_image",
                    models.ImageField(
                        default="profile_pics/default.jpg", upload_to="profile_pics"
                    ),
                ),
                (
                    "bot_widget_image",
                    models.ImageField(
                        default="bot_widget_pics/default.jpg",
                        upload_to="bot_widget_pics",
                    ),
                ),
                (
                    "user",
                    models.OneToOneField(
                        on_delete=django.db.models.deletion.CASCADE,
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
        ),
    ]
