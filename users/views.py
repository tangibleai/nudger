from django.core.mail import EmailMessage
import json
import logging

from pathlib import Path

from django.conf import settings
from django.core.files import File
from django.contrib import messages
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import AnonymousUser
from django.contrib.auth.models import User
from django.db.utils import IntegrityError
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.utils.datastructures import MultiValueDictKeyError
from django.utils.encoding import force_bytes, force_str
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.http import JsonResponse, HttpResponseRedirect

from convohub.constants import (
    DJANGO_DEFAULT_USERNAME,
    # DEFAULT_PROJECT,
    ENV,
    DEFAULT_CONVOGRAPH_YAML_PATH,
)
from hub.helpers.tokens import TokenGenerator
from hub.models import Convo, Project, ProjectCollaborator
from hub.utils import get_project
from scripts.create_default_convo_on_db import create_convo
from users.helpers import digitalocean, image
from users.models import Profile, UserImage, ExternalAccount
from .forms import (
    ProfileCreateForm,
    ProfileUpdateForm,
    UserCreateForm,
    UserUpdateForm,
    UserImageForm,
)
from .utils import make_serializable
from hub.tasks import remove_old_unactive_users

from .forms import PasswordResetForm, SetPasswordForm, LoginForm

log = logging.getLogger(__name__)


def home(request):
    file_css = "polyface/polyface.css"
    file_js = "polyface/polyface.js"
    log.debug(f"users.views.home() request.user: {request.user}")

    project = get_project(request.user)
    active_convo = (
        Convo.objects.filter(project=project).order_by("-activated_on").first()
    )

    if not active_convo:
        user = User.objects.filter(username=ENV.get("DJANGO_DEFAULT_USERNAME")).first()
        project = get_project(user)
        active_convo = (
            Convo.objects.filter(project=project).order_by("-activated_on").first()
        )
    log.debug(f"active_convo: {active_convo}")
    url = ENV.get("WEBSOCKET_URL") + f"/ws/quiz/{request.user}/{active_convo.id}"
    fetch_image_endpoint_url = "http://localhost:8000/quiz/bot_widget_icon"

    widget_image = get_widget_image(request)

    return render(
        request,
        "users/home.html",
        {
            "file_css": file_css,
            "file_js": file_js,
            "welcome_popup_on": json.dumps(False),
            "empty_message_allowed": json.dumps(False),
            "name": "Qary",
            "bot_picture": widget_image,
            "fetch_image_endpoint_url": fetch_image_endpoint_url,
            "ws_url": url,
            "is_s3": json.dumps(str(settings.USE_S3) in (True, "True", "true")),
            "project_name_slug": project.slug,
        },
    )


def get_widget_image(request):
    if isinstance(request.user, AnonymousUser):
        return get_widget_image_for_anonymous_user()
    return get_widget_image_for_logged_in_user(request)


def get_widget_image_for_anonymous_user():
    widget_image = "/media/bot_widget_pics/default.jpg"
    if settings.USE_S3:
        widget_image = settings.DEFAULT_DIGITALOCEAN_BUCKET_URL + widget_image
    return widget_image


def get_widget_image_for_logged_in_user(request):
    if hasattr(request.user, "profile"):
        return request.user.profile.bot_widget_image.url
    return get_default_user_widget_image()


def get_default_user_widget_image():
    user = User.objects.get(username=DJANGO_DEFAULT_USERNAME)
    return user.profile.bot_widget_image.url


def about(request):
    project = get_project(request.user)
    return render(request, "users/about.html", {"project_name": project.public_name})


def create_project_and_project_collaborator(user):
    project = Project.objects.create()
    default_user_convo = create_convo(
        project=project,
        file_path=Path(DEFAULT_CONVOGRAPH_YAML_PATH),
        convo_name="default_user_convo_name_test",
        convo_description="default_user_convo_description_test",
    )

    project_collaborator = ProjectCollaborator.objects.create(
        user=user, project=project, role="owner"
    )
    return default_user_convo, project_collaborator


def create_default_image(user):
    """
    Saves an instance of the local default.jpg to the appropriate upload_to path in UserImage

    Opens the file to allow upload_to to use the default path set in the model instead of the local path
    """
    default_path = "./media/profile_pics/default.jpg"
    with open(default_path, "rb") as file:
        image_file = File(file)
        image_instance = UserImage(user=user)
        image_instance.image.save("default.jpg", image_file)


def create_user_and_user_profile(request):
    project = get_project(request.user)
    user_form = UserCreateForm()
    profile_form = ProfileCreateForm()
    context = {"user_form": user_form, "project_name_slug": project.slug}
    if request.method == "POST":
        user_form = UserCreateForm(request.POST)
        profile_form = ProfileCreateForm(request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            email = user_form.cleaned_data.get("email")
            if User.objects.filter(email=email):
                context.update({"show_modal": True, "email": email})
            else:
                try:
                    user = user_form.save()
                    user.is_active = False
                    user.save()
                    profile_form.save(user=user)

                    send_email(request, user)

                    remove_old_unactive_users.delay()
                    return redirect("email-confirmation")

                except IntegrityError:
                    messages.error(
                        request,
                        f"Such user already exists. Please, use other email",
                    )
                    return redirect("signup")
    return render(request, "users/signup.html", context)


def send_email(request, user):
    url_origin = request.scheme + "://" + request.get_host()
    mail_subject = "Activation link has been sent to your email id"
    account_activation_token = TokenGenerator()
    message = render_to_string(
        "users/verify_email.html",
        {
            "user": user,
            "url_origin": url_origin,
            "uid": urlsafe_base64_encode(force_bytes(user.pk)),
            "token": account_activation_token.make_token(user),
        },
    )
    to_email = user.email
    email = EmailMessage(
        mail_subject,
        message,
        from_email=settings.EMAIL_HOST_USER,
        to=[to_email],
    )
    email.send()
    return


def render_confirm_email_message(request):
    return render(
        request,
        "users/email_message_template.html",
        {
            "message": "Confirmation link has been sent to your email! Please confirm your email address within 30 minutes to complete the registration"
        },
    )


def activate(request, uidb64, token):
    try:
        uid = force_str(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    account_activation_token = TokenGenerator()
    if user and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        messages.success(
            request,
            "Thank you for your email confirmation. Now you can login your account.",
        )
        return redirect("login")
    else:
        if user:
            Profile.objects.get(user=user).delete()
            user.delete()
        messages.error(request, "Activation link is invalid!")
        return redirect("signup")


def healthcheck(request):
    """Pulsar and other apps to check the health of the deployed webapp at `healthcheck.json`

    Always return 200 and a valid json str.

    WARNING: Users can view their request info that is received by qary.ai here.
    """
    log.error(f"users.views.healthcheck with {request}")

    return JsonResponse(
        {
            "request": make_serializable(request),
            "request.session": make_serializable(request.session),
        }
    )


@login_required
def update_user_and_user_profile(request):
    project = get_project(request.user)
    if request.method == "POST":
        u_form = UserUpdateForm(request.POST, instance=request.user)
        try:
            request.FILES["bot_widget_image"] = image.resize_image(
                request.FILES["bot_widget_image"], 70, 70
            )
            request.FILES["profile_image"] = image.resize_image(
                request.FILES["profile_image"], 300, 300
            )
        except MultiValueDictKeyError:
            pass

        p_form = ProfileUpdateForm(
            request.POST, request.FILES, instance=request.user.profile
        )
        image_form = UserImageForm(request.POST, request.FILES)
        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            messages.success(request, f"Your account has been updated.")
            # Post GET redirect pattern - avoid running another POST request
        if image_form.is_valid():
            image_form.save(user=request.user)
        return redirect("profile")

    u_form = UserUpdateForm(instance=request.user)
    p_form = ProfileUpdateForm(instance=request.user.profile)
    image_form = UserImageForm(instance=request.user)

    user_images = extract_user_images_data(request.user)

    # FIXME: Consider if there is a way to simplify "profile_pic" URL

    context = {
        "u_form": u_form,
        "p_form": p_form,
        "image_form": image_form,
        "profile_pic": digitalocean.get_private_image_url(
            "media/" + request.user.profile.profile_image.url.split("media/")[-1]
        )
        if settings.USE_S3
        else request.user.profile.profile_image.url,
        "user_images_data": user_images,
        "project_name_slug": project.slug,
    }
    context.update(**get_provider_configuration_assets("github", request.user))
    context.update(**get_provider_configuration_assets("gitlab", request.user))

    return render(request, "users/profile.html", context)


def get_provider_configuration_assets(provider, user):
    data = {f"{provider}_connected": False}
    account = None
    for a in ExternalAccount.objects.filter(user=user):
        if a.provider == provider:
            account = a
            data[f"{provider}_connected"] = True
    if data[f"{provider}_connected"]:
        data[f"{provider}_username"] = account.username

    return data


def extract_user_images_data(user):
    """Returns a queryset of UserImage objects with additional attributes

    Queries the UserImage model to get all user uploaded images.
    Iterate throught images and add `url` and `title` attributes to each image.
    """
    user_images = UserImage.objects.filter(user=user)
    for img in user_images:
        # NOTE: This references the S3 bucket if USER_S3.  "img.image.name" already has a "/"
        img.url = (
            digitalocean.get_private_image_url("media" + img.image.name)
            if settings.USE_S3
            else img.image.name
        )
        img.title = img.image.name.split("/")[-1]
    return user_images


def password_change(request):
    user = request.user
    if request.method == "POST":
        form = SetPasswordForm(user, request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Your password has been changed")
            return redirect("login")
        else:
            for error in list(form.errors.values()):
                messages.error(request, error)

    form = SetPasswordForm(user)
    return render(request, "users/password_reset_confirm.html", {"form": form})


def password_reset_request(request):
    if request.method == "POST":
        form = PasswordResetForm(request.POST)
        if form.is_valid():
            user_email = form.cleaned_data["email"]
            associated_user = User.objects.get(email=user_email)
            if associated_user:
                subject = "Password Reset request"
                account_activation_token = TokenGenerator()
                message = render_to_string(
                    "users/template_reset_password.html",
                    {
                        "user": associated_user,
                        "url_origin": request.scheme + "://" + request.get_host(),
                        "uid": urlsafe_base64_encode(force_bytes(associated_user.pk)),
                        "token": account_activation_token.make_token(associated_user),
                    },
                )
                email = EmailMessage(subject, message, to=[associated_user.email])
                if email.send():
                    messages.success(
                        request,
                        """
                        Password reset sent.
                        We've emailed you instructions for setting your password, if an account exists with the email you entered.
                        You should receive them shortly. If you don't receive an email, please make sure you've entered the address
                        you registered with, and check your spam folder.
                        """,
                    )
                else:
                    messages.error(
                        request,
                        "Problem sending reset password email, <b>SERVER PROBLEM</b>",
                    )

            return redirect("password_reset")

        for key, error in list(form.errors.items()):
            if key == "captcha" and error[0] == "This field is required.":
                messages.error(request, "You must pass the reCAPTCHA test")
                continue

    form = PasswordResetForm()
    return render(
        request=request,
        template_name="users/password_reset.html",
        context={"form": form},
    )


def password_reset_confirm(request, uidb64, token):
    try:
        uid = force_str(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except:
        if user in None:
            messages.error(request, "User with such email not found.")
            redirect("login")

    account_activation_token = TokenGenerator()
    if account_activation_token.check_token(user, token):
        if request.method == "POST":
            form = SetPasswordForm(user, request.POST)
            if form.is_valid():
                form.save()
                messages.success(
                    request,
                    "Your password has been set. You can now log in with it.",
                )
                return redirect("login")
            else:
                for error in list(form.errors.values()):
                    messages.error(request, error)

        form = SetPasswordForm(user)
        return render(request, "users/password_reset_confirm.html", {"form": form})
    else:
        messages.error(request, "Link is expired")

    messages.error(request, "Something went wrong.")
    return redirect("login")


def login_view(request):
    if request.method == "POST":
        form = LoginForm(data=request.POST)

        if form.is_valid():
            user = form.get_user()
            login(request, user)

            if form.cleaned_data.get("remember_me"):
                request.session.set_expiry(30 * 24 * 60 * 60)

            messages.success(
                request, f"Hello, {user.username}! You have been successfully logged in"
            )

            return redirect("users-home")
        else:
            for error in dict(form.errors).values():
                messages.error(request, error)
    else:
        form = LoginForm()

    return render(request, "users/login.html", {"form": form})


def logout_view(request):
    response = HttpResponseRedirect("/")
    response.delete_cookie("provider_name")
    response.delete_cookie("prior_user_id")
    logout(request)
    messages.success(request, f"You have been logged out")
    return response


def set_provider_in_cookie_and_redirect(request):
    """Only accepts POST requests"""
    if request.method == "POST":
        provider_name = request.POST.get("provider_name")
        provider_authentication_url = request.POST.get("provider_authentication_url")
        response = HttpResponseRedirect(provider_authentication_url)
        response.set_cookie("provider_name", provider_name, max_age=3600)
        return response
    return redirect("users-home")


def set_user_id_in_cookie_and_redirect(request):
    """Only accepts POST requests"""
    if request.method == "POST":
        provider_name = request.POST.get("provider_name")
        provider_authentication_url = request.POST.get("provider_authentication_url")
        response = HttpResponseRedirect(provider_authentication_url)
        response.set_cookie("original_user_id", request.user.id, max_age=3600)
        response.set_cookie("provider_name", provider_name, max_age=3600)
        return response
    return redirect("users-home")


def disconnect_provider(request):
    """Only accepts POST requests"""
    if request.method == "POST":
        provider_name = request.POST.get("provider_name")
        user = request.user
        ExternalAccount.objects.filter(user=user, provider=provider_name).update(
            user=None
        )
    return redirect("profile")
