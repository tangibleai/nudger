from django import forms
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django import forms
from django.contrib.auth.forms import PasswordResetForm
from django.contrib.auth.forms import SetPasswordForm
from django.contrib.auth.models import User
from django.forms.widgets import Widget
from django.utils.html import format_html
from phonenumber_field.formfields import PhoneNumberField

from .models import Profile, UserImage


class UserCreateForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ["email", "password1", "password2"]

    def save(self, commit=True):
        passed_data = self.cleaned_data
        email = passed_data["email"]
        user = User(username=email, email=email)
        user.set_password(passed_data["password1"])
        if commit:
            user.save()
        return user


class SmsCheckboxWidget(Widget):
    def render(self, name, value, attrs=None, renderer=None):
        checkbox_html = f"""<div class="pretty p-svg p-curve">
            <input type="checkbox" name="{name}" {'checked' if value else ''}/>
            <div class="state p-success">
                <svg class="svg svg-icon" viewBox="0 0 20 20">
                    <path
                        d="M7.629,14.566c0.125,0.125,0.291,0.188,0.456,0.188c0.164,0,0.329-0.062,0.456-0.188l8.219-8.221c0.252-0.252,0.252-0.659,0-0.911c-0.252-0.252-0.659-0.252-0.911,0l-7.764,7.763L4.152,9.267c-0.252-0.251-0.66-0.251-0.911,0c-0.252,0.252-0.252,0.66,0,0.911L7.629,14.566z"
                        style="stroke: white;fill:white;"></path>
                </svg>
                <label>I would like to receive SMS messages</label>
            </div>
        </div>"""
        return format_html(checkbox_html)


class ProfileCreateForm(forms.ModelForm):
    phone_number = PhoneNumberField(required=False)
    does_accept_sms = forms.BooleanField(
        widget=SmsCheckboxWidget(), required=False, label=""
    )

    class Meta:
        model = Profile
        fields = ["phone_number", "does_accept_sms"]

    def save(self, user: User):
        passed_data = self.cleaned_data
        Profile.objects.create(
            user=user,
            phone_number=passed_data["phone_number"],
            does_accept_sms=passed_data["does_accept_sms"],
        )


class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ["username", "email"]


class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ["profile_image", "bot_widget_image"]


class UploadImageFieldWidget(Widget):
    def render(self, name, *args, **kwargs):
        image_field = f'\
            <div class="form-group">\
                <label for="file_upload_field">Image:</label>\
                <input id="file_upload_field" type="file" name="{name}" accept="image/*">\
            </div>\
        '
        return format_html(image_field)


class UserImageForm(forms.ModelForm):
    image = forms.ImageField(required=True, label="")

    class Meta:
        model = UserImage
        fields = ("image",)

    def save(self, user: User, **kwargs):
        passed_data = self.cleaned_data
        user_image = UserImage.objects.create(user=user, image=passed_data["image"])
        return user_image


class CustomLoginForm(AuthenticationForm):
    username = forms.CharField(
        label="Email", widget=forms.TextInput(attrs={"autofocus": True})
    )


class SetPasswordForm(SetPasswordForm):
    class Meta:
        model = User
        fields = ["new_password1", "new_password2"]


class PasswordResetForm(PasswordResetForm):
    def __init__(self, *args, **kwargs):
        super(PasswordResetForm, self).__init__(*args, **kwargs)

    # captcha = ReCaptchaField(widget=ReCaptchaV2Checkbox())


class LoginForm(AuthenticationForm):
    remember_me = forms.BooleanField(required=False, label="Stay logged in for 30 days")
