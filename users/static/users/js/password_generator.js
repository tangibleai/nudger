function populateRandomPassword() {
    var newPassword = generateRandomPassword();
    document.getElementById("id_new_password1").value = newPassword;
    document.getElementById("id_new_password2").value = newPassword;
}

function generateRandomPassword() {
    var length = 8;
    var charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    var retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
}
