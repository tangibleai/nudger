import io
import logging
import sys
import yaml


from pathlib import Path

from django.conf import settings
from django.core.files import File
from django.core.files.base import ContentFile
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.db.models.signals import post_save
from django.dispatch import receiver

from allauth.socialaccount.models import SocialAccount
from convohub.constants import DEFAULT_CONVOGRAPH_YAML_PATH
from hub.convograph_validator import helpers
from hub.convograph_validator.convograph_validator_errors import (
    ConvoGraphValidationError,
)
from hub.models import Project, ProjectCollaborator
from hub.utils import get_file_format_version
from hub.yaml_to_db_records.yaml_to_db_records import convoyaml_to_convomodel
from users.models import Profile, UserImage


log = logging.getLogger(__name__)


# NOTE: Importing from scripts.create_default_convo_on_db gave an error (RuntimeError: populate() isn't reentrant)
def convert_to_inmemoryuploadedfile(text_io_wrapper, filename, content_type):
    content = text_io_wrapper.read()
    in_memory_uploaded_file = InMemoryUploadedFile(
        file=ContentFile(content),
        field_name=None,
        name=filename,
        content_type=content_type,
        size=len(content),
        charset=None,
    )
    return in_memory_uploaded_file


def create_convo(
    project,
    file_path,
    convo_name=None,
    convo_description=None,
):
    file_path = Path(file_path)
    conversation = None

    if not file_path.is_file():
        log.error(f"No file found: {file_path}")
        return
    elif not project:
        log.error("Create a default project first!")
        return
    error_msg = None
    try:
        conversation = yaml.safe_load(Path(file_path).read_text())
        format_version = get_file_format_version(conversation)
        convograph_validator = helpers.get_validator_by_version(format_version)
        convograph_validator(conversation).run()
    except ConvoGraphValidationError as exc:
        error_msg = f"{str(exc)} "

    if error_msg:
        error_msg = "Some issue occurred in default conversation! " + error_msg
        error_msg += "Use the format in the https://gitlab.com/tangibleai/convohub/-/blob/main/data/countByOne_0001.yml"
        log.error(error_msg)
        return
    if settings.USE_S3:
        with open(file_path, "rb") as f:
            file = ContentFile(f.read())
        file.name = file_path.name

    else:
        file = convert_to_inmemoryuploadedfile(
            io.TextIOWrapper(open(file_path, "rb")),
            file_path.name,
            "text/yaml",
        )
    convo_name = conversation[0].get("convo_name") if not convo_name else convo_name
    convo_description = (
        conversation[0].get("convo_description")
        if not convo_description
        else convo_description
    )
    convo_queryset = convoyaml_to_convomodel(
        graph=file,
        project=project,
        convo_name=convo_name,
        convo_description=convo_description,
    )
    log.info(
        f"Created convo:\n  file: {file}\n  project: {project}\n"
        + f"  convo_name: {convo_name}\n  description: {convo_description}"
    )
    return convo_queryset


def create_project_and_project_collaborator(user):
    project = Project.objects.create()
    default_user_convo = create_convo(
        project=project,
        file_path=Path(DEFAULT_CONVOGRAPH_YAML_PATH),
        convo_name="default_user_convo_name_test",
        convo_description="default_user_convo_description_test",
    )

    project_collaborator = ProjectCollaborator.objects.create(
        user=user, project=project, role="owner"
    )


def create_default_image(user):
    default_path = "./media/profile_pics/default.jpg"
    with open(default_path, "rb") as file:
        image_file = File(file)
        image_instance = UserImage(user=user)
        image_instance.image.save("default.jpg", image_file)


@receiver(post_save, sender=SocialAccount)
def social_account_social_account_added(sender, instance, created, **kwargs):
    if created:
        Profile.objects.get_or_create(user=instance.user)
        create_project_and_project_collaborator(instance.user)
        create_default_image(user=instance.user)
