# utils.py
""" python utilities for manipulating user, request, and session data """
import logging


log = logging.getLogger(__name__)


def make_serializable(obj, ignore_prefix="_", ignore_suffix="_", depth=2):
    """Represent any object as a dictionary of all its attributes

    Converts tuples to lists to be compatible with YAML and JSON serializaters.

    TODO: separate dict_depth kwarg to explore dict values for objects that need to be

    >>> make_serializable(make_serializable)
    '<function make_serializable at 0x...>'
    >>> make_serializable({})
    {}
    >>> d = make_serializable(log)
    >>> d.keys()
    dict_keys(['Logger'])
    >>> list(d['Logger'].keys())[:5]
    ['addFilter', 'addHandler', 'callHandlers', 'critical', 'debug']
    """
    if isinstance(obj, (str, float, int, bool)) or obj is None:
        return obj
    if isinstance(obj, bytes):
        return obj.decode()
    if depth < 1:
        return repr(obj)
    kwargs = dict(
        ignore_prefix=ignore_prefix,
        ignore_suffix=ignore_suffix,
        depth=depth - 1,
    )
    if isinstance(obj, dict) and callable(getattr(obj, "items", None)):
        return {k: make_serializable(v, **kwargs) for (k, v) in obj.items()}
    if isinstance(obj, (list, tuple)):
        return [make_serializable(v, **kwargs) for v in obj]
    if obj.__class__.__name__ in ("function", "method"):
        # ignore function and method attributes
        return repr(obj)  # obj.__name__
    d = {}
    attrs = [
        k
        for k in dir(obj)
        if not (k.startswith(ignore_prefix or " ") or k.endswith(ignore_suffix or " "))
    ]
    for k in attrs:
        try:
            d[k] = make_serializable(getattr(obj, k), **kwargs)
        except Exception as e:
            d[k] = repr(e)
    return {obj.__class__.__name__: d}
    # return str(object)
