import sentry_sdk


def report_through_sentry(label, issue, data):
    # msg_template = "{issue}- data: {data}"
    # msg = msg_template.format(issue=issue, data=data)
    sentry_sdk.capture_message(issue, label)
