from django.urls import path
from users import views
from hub.views import chat_page


urlpatterns = [
    path("", views.home, name="users-home"),
    path("about/", views.about, name="users-about"),
    path("~/<str:project_name_slug>", chat_page, name="chat-page"),
    path(
        r"verify_email/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/",
        views.activate,
        name="activate",
    ),
    path("about/", views.about, name="users-about"),
    path(
        "email_confirmation",
        views.render_confirm_email_message,
        name="email-confirmation",
    ),
    path("password_change", views.password_change, name="password_change"),
    path("password_reset", views.password_reset_request, name="password_reset"),
    path(
        "reset/<uidb64>/<token>",
        views.password_reset_confirm,
        name="password_reset_confirm",
    ),
    path(
        "set_provider_in_cookie_and_redirect",
        views.set_provider_in_cookie_and_redirect,
        name="set-provider-in-cookie-and-redirect",
    ),
    path(
        "set_user_id_in_cookie_and_redirect",
        views.set_user_id_in_cookie_and_redirect,
        name="set-user-id-in-cookie-and-redirect",
    ),
    path(
        "disconnect_provider",
        views.disconnect_provider,
        name="disconnect-provider",
    ),
]
