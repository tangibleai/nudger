from django.contrib.auth.models import User
from django.test import TestCase

from users.models import UserImage
from users.views import extract_user_images_data


class TestViewsHelpers(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username="testuser", password="testpass")
        self.user_uploaded_images = []
        self.create_user_images()
        self.extracted_images = extract_user_images_data(self.user)
        self.expected_images = UserImage.objects.filter(user=self.user)
        return

    def create_user_images(self):
        images_names = [
            f"media/{self.user.username}/default.jpg",
            f"media/{self.user.username}/default1.jpg",
            f"media/{self.user.username}/default2.jpg",
        ]
        for img_name in images_names:
            user_image = UserImage.objects.create(user=self.user, image=img_name)
            self.user_uploaded_images.append(user_image)
        return

    def test_extract_user_images_data(self):
        extracted_images_count = len(self.extracted_images)
        expected_images_count = len(self.expected_images)
        self.assertEqual(expected_images_count, extracted_images_count)
        return

    def test_extracted_images_have_required_attributes(self):
        for img in self.extracted_images:
            self.assertTrue(hasattr(img, "url"))
            self.assertTrue(hasattr(img, "title"))
        return

    def tearDown(self) -> None:
        self.user.delete()
        for img in self.user_uploaded_images:
            img.delete()
