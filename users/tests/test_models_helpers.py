from django.contrib.auth.models import User
from django.test import TestCase

from users.models import user_image_path, UserImage


class TestModelsHelpers(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username="testuser", password="testpass")
        self.image = UserImage.objects.create(
            user=self.user,
            image="default.jpg",
        )

    def test_user_image_path(self):
        filename = "default.jpg"
        expected_image_path = f"uploaded_images/{self.user.username}/{filename}"
        actual_image_path = user_image_path(self.image, filename)
        self.assertEquals(expected_image_path, actual_image_path)

    def tearDown(self):
        self.user.delete()
        self.image.delete()
