import os

from django.contrib.auth.models import User
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

from convohub.constants import (
    DEFAULT_BOT_WIDGET_IMAGE_PATH,
    DEFAULT_PROFILE_IMAGE_PATH,
    DEFAULT_CONVO_NAME,
)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    convo_name = models.CharField(
        max_length=100, null=True, blank=True, default=DEFAULT_CONVO_NAME
    )
    profile_image = models.ImageField(
        default=DEFAULT_PROFILE_IMAGE_PATH, upload_to="profile_pics"
    )
    bot_widget_image = models.ImageField(
        default=DEFAULT_BOT_WIDGET_IMAGE_PATH, upload_to="bot_widget_pics"
    )
    phone_number = PhoneNumberField(blank=True, null=False, default="")
    does_accept_sms = models.BooleanField(blank=True, null=False, default=False)

    def __str__(self):
        return f"{self.user.username} Profile"


def user_image_path(instance, filename):
    """Composes and returns a string of form 'uploaded_images/username/filename'"""
    username = instance.user.username
    return os.path.join("uploaded_images", username, filename)


class UserImage(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    image = models.ImageField(
        upload_to=user_image_path,
        blank=False,
        null=False,
    )


class Context(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    context = models.JSONField(blank=False, null=True, default=dict)


class ExternalAccount(models.Model):
    """A model that records access permissions for users to Project instances"""

    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    username = models.CharField(
        max_length=100,
    )
    email = models.CharField(
        max_length=100,
    )
    provider = models.CharField(
        max_length=100,
    )
