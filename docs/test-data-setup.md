﻿# Creating and Recreating Test Data

## Background
You may want to use test data to try out the functionality of the system as well as to explore how the model itself is working.  In sms_nudger/create_test_data.py, there is a set of functions to create test data based on some JSON files (i.e., sample_{tableName}.json) in the project root folder.  

## Set up steps

### Create test data
The test data is created under the assumption that the database is completely empty because the script needs to handle links between tables based on primary keys.

From the project root folder, run the file create_test_data.py in sms_nudger.

    >>> python sms_nudger/create_test_data.py

### Reset database
Delete the data in the database fully.  You need to do this to both remove all current data as well as reset the primary keys.

    >>> python manage.py reset_db --noinput

Recreate the database tables.

    >>> python manage.py makemigrations
    >>> python manage.py migrate

You should see something like the following:

      Applying contenttypes.0001_initial... OK
	  Applying auth.0001_initial... OK
	  Applying admin.0001_initial... OK
	  Applying admin.0002_logentry_remove_auto_add... OK
	  Applying admin.0003_logentry_add_action_flag_choices... OK
	  Applying contenttypes.0002_remove_content_type_name... OK
	  Applying auth.0002_alter_permission_name_max_length... OK
	  Applying auth.0003_alter_user_email_max_length... OK
	  Applying auth.0004_alter_user_username_opts... OK
	  Applying auth.0005_alter_user_last_login_null... OK
	  Applying auth.0006_require_contenttypes_0002... OK
	  Applying auth.0007_alter_validators_add_error_messages... OK
	  Applying auth.0008_alter_user_username_max_length... OK
	  Applying auth.0009_alter_user_last_name_max_length... OK
	  Applying auth.0010_alter_group_name_max_length... OK
	  Applying auth.0011_update_proxy_permissions... OK
	  Applying auth.0012_alter_user_first_name_max_length... OK
	  Applying sessions.0001_initial... OK
	  Applying sms_nudger.0001_initial... OKenter code here

Follow the steps in the *Create test data* section to recreate the data.
