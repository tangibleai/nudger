#### Project creation frontend

```commandline
npm create vite@latest
```

#### From an existing project

```commandline
git clone repo-url .
npm install
npm run dev
```

#### To django settings add
```python
INSTALLED_APPS = [
'rest_framework',
'django-cors-headers',
]

CORS_ALLOWED_ORIGINS = [
'http://localhost'
]
```

#### Connect backend to frontend in development 
- Create an .env for frontend with the key below
- VITE_REACT_APP_API_URL=http://127.0.0.1:8000



#### Configuration `package.json```
```text
  "scripts": {
    "dev": "vite",
    "build": "tsc && vite build",
    "lint": "eslint . --ext ts,tsx --report-unused-disable-directives --max-warnings 0",
    "preview": "vite preview --port 3001"
  }
```

#### `vite.config.ts` with default port
```text
export default defineConfig({
  plugins: [react()],
  server: {
  host: true,
  strictPort: true,
  port: 3000
  }
})
```

#### Create a production build
```commandline
npm run build
npm run preview
```

#### Resources
- [Djoser auth system](https://djoser.readthedocs.io/en/latest/getting_started.html)
- [Vite backend documentation](https://vitejs.dev/guide/backend-integration.html)
- [Build a Django REST API with the Django Rest Framework. Complete Tutorial.](https://www.youtube.com/watch?v=c708Nf0cHrs)
- [React & Django TUTORIAL Integration // REACTify Django](https://www.youtube.com/watch?v=AHhQRHE8IR8&t=26s)
- [Deploy Django into Production with Kubernetes, Docker, & Github Actions. Complete Tutorial Series](https://www.youtube.com/watch?v=NAOsLaB6Lfc)


#### React and Django integration
- Option-1 (Integration with django template tags)
  - https://www.youtube.com/watch?v=FCyYIVfDkhY
  - https://gist.github.com/lucianoratamero/7fc9737d24229ea9219f0987272896a2

- Option-2 (Integration with custom code in vite.config.ts but caused trouble for static files)
  - https://www.youtube.com/watch?v=9Iq-0OYkoX0&t=629s

- Option-3 (Nginx ingress, serving static file with nginx and creating routes for frontend and backend application)
  - https://www.youtube.com/watch?v=OVVGwc90guo
  - https://gitlab.com/codeching/docker-multicontainer-application-react-nodejs-postgres-nginx-basic

#### Notes
I tried option-1 and option-2. Option 2 caused problem on static files. Because django is prefixing static files with "/static/". Option 1 worked after I modified the template tag.

I haven't time for the option-3 but it is the most interesting one and modular.
