# Choosing a license for your conversation design

What kind of license should you choose for your chatbot?
Should you use a software license (GPL, MIT, etc) or a "creative commons" style license for content (text and images)?
This question hinges on whether you think of your chatbot as _content_ or _code_ ... or both.
In reality, your conversation design includes both the content and the code that you write.
But wait, you say, I didn't write any code!
If you think about it, a large portion of the value of a chatbot is in its ability to make decisions about how to proceed with the conversation and in the configuration of the NLU algorithms you use to help it make those decisions.
You worked hard to craft not only the words that your chatbot will say, but also the triggers and intent categories that your chatbot uses to make decisions.
So if you want to protect your chatbot from being used unfairly (against you, your organization, your users, or your beneficiaries) you should probably use a free open source software (FOSS) license.
At least then you know that anyone attempting to reuse or repurpose your chatbot will know how you intend them to reciprocate your openness.
And you will have the support of the Free Software Foundation (FSF) and Open Source Initiative (OSI) and a broad community of open source developers around the world.

### Training AI on CC content

Neither an open source content license nor an open source software license are likely to prevent large organizations from training their algorithms on your content.
In 2021, Creative Commons did not offer an opinion about the legality of training AI on CC content, but did explain that if that AI is not trained for the public good or public research then it would violate the goals of the Creative Commons organization.
So if you use a CC license for your chatbot design might encourace machine learning engineers to think twice about using your data to build enshittified AI.
In 2023, several lawsuits were filed against Open AI, Microsoft, and Google for alegedly violating the *closed source* (proprietary) licenses of some copyrighted works (books, photos, art).
So if you license your chatbot under a closed source license
But in the past, US courts ruled that it was "fair use" for machine learning algorithms to be trained on copyrighted works (including paywalled content) with impunity.
Essentially, corporations are allowed to train thier AI on copyrighted works in the same way the purchase licenses to train their employees.

In the EU is is legal for non-commercial research and cultural heritage organizations to use Creative Commons licenced content for text-and-data mining (TDM), another term for AI.
CC text content can also be used to train machine learning algorithms by commercial businesses as long as the individual rights holders (you) are allowed to opt out of those uses of your data.
Article 3 of the EU Directive on Copyright in the Digital Single Market (DSM) governs the noncommercial use of your data.
EU commercial use of copyrighted data is goverened under Article 4.

## Terms of Service 

What about the terms of service on your chatbot service?
It is possible to prevent others from training AI on your data by restricting its use the Terms of Service (ToS) for your chatbot, but the wording you choose in your ToS is not enough.
You must also enfoce those restrictions within your user interface with throttling, robots.txt instructions, black lists, and API restrictions.
Otherwise, it is difficult to legally enforce your ToS after the users have already retrieved your data and used it to train or test their own chatbots.
We will apply some sane limits on number of simultaneous sessions and the volume of requests for you chatbot content based on our experience in preventing denial of service attacks and bot scraping.
But we will not be able to prevent scraping if it is designed to imitate the behaviors of a typical chatbot user.

## Which FOSS license should I choose

If you want to avoid others using your chatbot to support a commercial service, you may want to choose an Apache license.
This will require the "competitors" that use your chatbot in a service to open source ("copy left") both their server and chatbot.
This way you can take advantage of any enhancements they have added, if you want to include them within your service.
The GPL-3.0 license is another popular copy-left license, but it may not require commercial users to open source their server software that is used to offer your chatbot to their customers.
And if you want to encourage others to use and modify your software for the maximum possible impact on the world, you can release it under the MIT license.
This is the best option if the chatbot is not part of your core business model.
An MIT license will ensure the widest popular adoption and reuse. 

Here is a summary table of some popular open source licenses that may help you decide on a license that is right for your project.
Percentages are their approximate popularity as a percentage of repositories on GitHub in 2018.


|                              | AGPL   | Apache   | BSDv3   | CC-0   | GPLv3   | LGPL   | MIT     |
|                              | 1.0%   | 22.0%    | 5.0%    | 1.0%   | 16.0%   | 6.0%   | 26.0%   |
|:-----------------------------|:-------|:---------|:--------|:-------|:--------|:-------|:--------|
| Proprietary service (server) | Y      | Y        | Y       | Y      | Y       | Y      | Y       |
| Modify & redistribute        | Y      | Y        | Y       | Y      | Y       | Y      | Y       |
| Sub-license                  | N      | Y        |         |        | N       | N      | Y       |
| Private use                  |        | Y        |         | Y      |         |        | Y       |
| Includes patents             |        | Y        |         | N      | Y       |        |         |
| May warranty services        | Y      | Y        | Y       |        | Y       |        |         |
| Must include (C) notice      | Y      | Y        | Y       |        | Y       | Y      | Y       |
| License includes notice      |        | Y        |         |        |         | Y      |         |
| Must include orig. license   |        |          |         |        | Y       | Y      |         |
| Include install instructions | Y      |          |         |        | Y       |        |         |
| Copy left & list changes     | Y      |          |         |        | Y       | Y      |         |
| May use trademark            |        | N        | N       | N      |         |        |         |

## References:

- [on OpenSource.StackExchange](https://opensource.stackexchange.com/a/5022/31370)
- Comparison of FOSS licenses [on Wikipedia]](https://en.wikipedia.org/wiki/Comparison_of_free_and_open-source_software_licenses)
- List of [OSI Approved Licenses](https://opensource.org/licenses/)
- List of [CC Licenses](https://creativecommons.org/share-your-work/cclicenses/)
- List of Popular licenses [by ActiveState](https://www.activestate.com/blog/the-developers-guide-open-source-software-license-comparison/)
- Should CC-Licensed Content be Used to Train AI? [on CreativeCommons.org](https://creativecommons.org/2021/03/04/should-cc-licensed-content-be-used-to-train-ai-it-depends/)

