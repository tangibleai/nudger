# Celery Commands for MOIA

## Celery
Terminal 1: celery -A convohub.celery worker --loglevel = info

## Celery Beat
Terminal 2: celery -A convohub.celery beat --loglevel=info --scheduler django_celery_beat.schedulers:DatabaseScheduler

## Flower
Terminal 3: celery -A convohub flower --loglevel=info
