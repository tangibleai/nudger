# Teachable AI

Imagine a chatbot designed to assist a conversation designer in building a tutor chatbot.
Here's a conversation design for a conversation design assistant. 
How meta1

## Setup

First you need to set up your conversation design by giving qary some names for the bot and each of the dialog participants (the chatbot and the human):

* **qary:** What name would you like to give this conversation plan?
    * **User:**  multiplication-quiz
* **qary:** OK. What role or name would the bot have in this conversation?
    * **User:**  Teacher
* **qary:** Sounds good. What role or name would the human user have in this conversation?
    * **User:**  Student
* **qary:** Perfect. I set that up in a new conversation for you.
* **qary:** Now you're ready to start the fun part of conversation design, dreaming up interesting and engaging conversations.

After getting the basics down, qary might also ask you about your user persona so you can think about the user experience for your chatbot.
It will also ask you about the personality or style of the chatbot itself.
Once you have that figured out, qary will proceed to help you construct a short piece of dialog.

## Interactive conversation design

Qary will give you a chance to name this conversation thread so you can find it later.

* **qary:** What will you call this first part of the conversation? 
    * **user:** multiplication-problem

Now comes the hard part for qary, and the fun part for the conversation designer.
Qary needs to figure out what questions to ask the user and detect when the bot should wait for a human response.

* **qary:** Now qary will shift to asking you questions about each turn. 
* **qary:** You basically want to have a conversation with yourself.
* **qary:** You just need to keep track of which "hat" your are wearing.
* **qary:** Sometimes you will play the roll of the chatbot, and other times you will play the role of the human user.
* **qary:** Give it a try for a couple back-and-forths of the conversation (__dialog turns__).
* **qary:** How would Teacher start the conversation?
* **qary:** _TEACHER:_
    * **User:** Let's try a multiplication problem.
* **qary:** What is the next thing Teacher would say?
    * **User:** What is 2 x 4?
* **qary:** What is an answer _STUDENT_ might say in reply to that question?
* **qary:** _STUDENT:_
    * **User:** 8 
* **qary:** How would the teacher reply to that?
* **qary:** _TEACHER:_
    * **User:** Great job! 
* **qary:** Anything else?
* **qary:** _TEACHER:_
    * **User:** no

## Adding variation

* **qary:** Great!
* **qary:** Let's go back to the beginning of your multiplication-problem conversation thread.
* **qary:** What are some different ways the teacher might ask that question "What is 2 x 4"?
* **qary:** _TEACHER:_
    * **user:** What is two times four?
* **qary:** What are some different ways the student might reply to that question "What is 2 x 4"?
* **qary:** _STUDENT:_
    * **user:** 9
* **qary:** What would the teacher say when a student says that?
* **qary:** _TEACHER:_
    * **user:** That seems a little too high to me. Give it another try. What is 2 x 4?


## Fleshing out your bot

Qary would then help you think of alternative things that the chatbot or user might say.
Perhaps it would stick with one section of the conversation before moving on to another mini-conversation.
This helps the user "stay in character" and remember the context of the mini conversation.
All of this data would be flowing into a database and presented in the web page to the left of the conversation with qary as if the user is having a real text message converation with themself, using two phones.

The data would be collected into sever dialog threads that could then be processed by convomeld to produce a rule-based chatbot.
Here's what the threads might look like "behind the scenes".

* **Teacher:** Let's try a multiplication problem.
* **Teacher:** What is 2 x 4?


## Interactive visualization


**qary.ai/role-play/**