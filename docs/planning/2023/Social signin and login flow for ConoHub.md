# Social signin and login flow for ConvoHub


## high priority
1. hide username field
2. bug: connect/disconnect link on profile page -- should remove social account instance in the DB
3. test ability to sign up with social media account first before creating "canonical" email for their account
4. email on convohub doesn't match social account e-mail - create viable workflow
5. multiple social signins connected to the account

## nice to have
- test gitlab/hub workflow - user edits yaml file and make sure we can load a public yaml file
- styling of the social connect
- ask for additional gitlab/github permissions