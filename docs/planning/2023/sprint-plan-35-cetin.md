# Sprint Plan

- [X] C: Fix variables on GitLab
- [X] C: Fix the CI/CD file
- [X] C: Fix failing tests
- [X] C: Write test for Public and Private convos
- [X] C: Fix environments on Render
