# Sprint 30

- [X] V: Error for the anonymous user in the home page.
- [X] V: On the home page even default user has been created the bundle didn't work
- [X] V: For the current user if you upload a convo and activate on the convo list it is not working unless you activate again in quizsystem. So it should be removed or fixed.
- [X] V: The widgets on the Mathbot quiz and Quiz system are not working when you activate convo from convo list.
- [X] V: Default user couldn't activate a convo.
- [X] S: Problem on the tests, I splitted the convo test, but it has old import which not exists right now.
- [X] C: Quizbot convo test failed.
- [X] Change logo on the main with qary logo
- [X] Create a simple dialog start, welcome, stop states.
- [X] Design a conversation for poly.
- [X] Remove print statement from settings.py.
  - Couldn't find a print statement.
