# ConvoHub Roadmap

## Vision

A platform for building teachable chatbots.

Intelligent rule-based chatbots are made possible with state of the art algorithms and UX that you can't find anywhere else (TurnIO, BotPress, ManyChat, Rasa, etc).

- Visual editor that helps conversation designers build more intelligent conversations
- Open community for bot mixing (sharing, combining, modifying) bots with convomeld, convocat, and convoscript
- Open standards for version control and content management (bots can be checked into `git`)
- Seamless LLM integration for more engaging dialog 

## Feature Frontier

The edges of our feature space that we are expanding to add new capabilities.

- Suggested next steps have `- [ ]` prefix.
- Longer term ideas have `- ` prefix.

### Actions (functions) and variables (`context` keys)
- [X] V: [Issue #5](gitlab.com/tangibleai/community/convohub/issues/53) designer can send buttons using "button_text": key within any trigger.
- [X] V: extract_proper_nouns integrated into demo.yml
- [ ] Mykhailo: default new user conversation design template for language learning.
- [X] V: extract_lang integrated into a working demo.yml
- [x] V: merge request review comments by hobson (3. ...)
- [x] V: fallback action try/except
- [x] V: fallback template rendering
- [X] C: get_wikipedia_summary(user_text='Barack Obama')
- [ ] G/H: incorporate mathtext as an extractor

#### _V: convo designer can create conditional triggers_
- [ ] Maria: swimlanes on convohub issue boards: https://gitlab.com/tangibleai/community/convohub/-/boards

- [ ] H: Table similar to delvin.models.Contact in ConvoHub.nudger.users for "public users" or "default user" so that public chatbots can be conversed with by anyone visiting the appropriate URL or telegram channel, etc
- [ ] [Issue #55](gitlab.com/tangibleai/community/convohub/issues/55) `context` variables (like language choice or name that persists across sessions) -- save context as JsonField in User table database after every action and trigger
- [x] V: semantic versioning of YAML syntax (e.g. version: 3.1 for conditional triggers)
- [ ] V: [Issue #54](https://gitlab.com/tangibleai/community/convohub/-/issues/54): conditional triggers ORed with `user_text` trigger (usually using `user_text: __next__`). 

#### _send_image action sends a media image from bot to user_
- [x] V: User can upload an image for use in convo design message to users or as chatbot avatar ()
- [ ] H: create issues and MRs for image bugs:
    - [ ] V: FIX: image link to s3 is not working in dashboard and send_image
    - [ ] V: FIX: image link reveals security token

- [x] V: function to scale raw user media images proportionally to prevent exceeded `faceofqary__width` and `faceofqary__height` stored in session `context` dict 
- context variables `{faceofqary: width: 128, height: 256, avatar: https://spaces.digitalocean.com/*.jpg}`  that detect faceofqary properties from the `request.session` data 
- [x] V: [Issue #52](gitlab.com/tangibleai/community/convohub/issues/52) `send_image(image_url=...)` 

- C: demo_image.yml with HTML/frontend display of image from external URL or the user's uploaded media: https://gitlab.com/tangibleai/community/convohub/-/issues/51 

#### Custom user variables in templates
- [x] V: "You have selected the language {{lang or language}}"
- [ ] H: `extract_proper_nouns()` and demo_names.yml "The first extracted proper noun is {{proper_nouns[0]}}"
- [ ] H: "The extracted proper nouns are {{proper_nouns}}"
- [X] V: "Your username is {{username}}"
- [ ] H: extract_emails() "Your email is {{emails[0]}}"
- user timezone start of convo date (day month year)
- user weather
- user news headlines

#### _LLM integration_
- H: service on replicate.com or 
- H: actions.reply_with_llm
- function filter_prompt_injections
- function filter_inappropriate_topics
- function find_best_next_state

### Messaging (SMS/email/slack) integration
- C: actions.send_email
- actions.send_sms
- twilio can send message to user

### visual builder (nested graph/network diagram)
- [ ] J/Vish: Fix qary.ai/convoscript/?convo_id=1234 form to connect to a convo ID and state name within the DB
- [ ] J: GET params with convoid and statename to render page for that state and convograph: `qary.ai/convoscript/?convoid=123& statename=start`
- [ ] clicking on node dives deeper into hierarchy of convo subgraphs using state_path or state_name string containing levels as a path

### Advanced conversation designer/developer features
- upload spreadsheet in Greg format
- upload convoscript text file in Ruslan format (human: ...\n ai: ...)
- Download HTML snippet with user TOKEN that allows them to add a chat widget to their website
- API endpoint so people can add a chatbot widget to their application
- API endpoint and see other conversations (monitor thier chatbot conversations)

### Chatbot frontend (UI)
- [x] V: handle asynchronous messages and ensure they are presented in the correct order 

#### Chat interfaces (accessibility)
- [x] V: `update_context` that accepts context_diff dicts
- [ ] C/H: research Loki, Graphana 
- [ ] C: chat over REST API
- chat over WebSocket API
- [ ] C: python SDK on PyPi to allow users to chat with their bot
- chat over Slack using convo designers Slack oauth2 account info
- chat over email using qary+botname@qary.ai email account hosted on some cheap e-mail provider like sendgrid
- chat over Telegram using a channel created by the convo designer
- chat over Mastodon using account info provided by the designer
- chat over Matrix using account info provided by the designer
- chat over Signal using designer-provided phone number/account credentials
- chat over Discord

#### paywalled? chat interfaces (accessibility)
- chat over WhatsApp (TurnIO wormhole with users TurnIO account info)
- make useable for the blind with screen readers
- Voice interface?

### Payment / subscriptions & user account management 

#### Custom homepage for ever user's activated bot
- H/G: static django `convo_home.html` template populated with convo.description
- H/G: add markdown filter to convo_home.html template. [see bottom of this tutorial](https://learndjango.com/tutorials/django-markdown-tutorial)
- H/G: `views.convo_home(): return render_template(convo_home.html)`
- H/G: urls.py with `/username/` route to views.convo_home() for testing their activated bot.
- H/G: `quizbot.models.Convo.page = TextField()` prepopulated with markdown text using convo.description
- Markdown from quizbot.models.Convo.page` rendered with [`markdown` package](https://pypi.org/project/Markdown/) in views.py to page similar to Hugging Face "Spaces" or GitLab README.md pages

#### Improved onboarding UX and security
- [ ] Fix the security hole on line 36 in users.views.home() `ENV.get("DJANGO_DEFAULT_USERNAME")` should not be required for Convo.object.filter to find a convo for the home page
- [-] #58 G: ?"sign up" instead of Register in menu
- [x] #44 G: test existing register
- [-] #59 G: email verification
- [-] #59 G: one click signup and sign-in with social media account authentication with 
- [x] G: additional signup with Gmail
- [ ] G: additional signup option Discord
- action on Convohub/Dashboard table to Copy (Fork) a bot --> creates copy and FK link to that session's user
- user profile page (or User's Dashboard) lists names of all bots on their account 
- RBAC such that only users linked to a Conv may edit any of that Convo's states and triggers
- RBAC such that public chatbots can by copied/forked and viewed or activated by anyone
- user can use SMS as the TFA auth method
- user create time-based TFA link to thier account
- user can generate API_KEY

### Bot templates (examples)
- H: example bot that recommends blog posts, news articles, RSS feeds, or podcasts, wikipedia articles that are interesting once per week
- [x] V: demo.yml with buttons and variables in bot messages     
- Maria: Syndee (imposter syndrome bot)
- Mykhailo: Emotional support/life-coach assistant (Legal assistance for immigrants) 
- Myk: 

### Platform security
- V: Copy or Fork action on Dashboard/ConvoHub
- when a user attempts to edit a public bot it is copied and they are given ownership of a "fork" and the copy defaults to public
- ensure only authorized users can read or write bot content that they own

### Bot project dashboard / management
- H: fix bug with dashboard pulldown
- [ ] H: fix bug with rori v0 upload
- G: update `quizbot.spreadsheet_to_db` functions to implement variables in v3
- G: utilize `quizbot.spreadsheet_to_db` during upload of spreadsheets instead of yaml
- download convograph to yaml v3 on Dashboard/ConvoHub page
- download convograph to Greg's spreadsheet format on Dashboard/ConvoHub page
- action on Convohub/Dashboard table to Copy (Fork) a bot --> creates copy and FK link to that session's user
- [ ] V: action in pulldown to enable edit (triggering GET request to convoscript)
- action in pulldown to enable merge 

### Social (bot sharing, remixing, contributing, etc.)
- V: action in pulldown to generate URL to a user's faceofqary demo page
- Create a link for users to share with friends to test their bot
- Generate "share with social media" text, such as "Chat with my bot at https://qary.ai/myusername/ or build your own at https://qary.ai/"
- Share on Mastadon button at bottom of convo_home.html
- Share on Lemmy button
- Share with social media button that generates e.g. "Chat with my bot at https://qary.ai/myusername/ or build your own at https://qary.ai/"
 
### Multilingual site (platform)
- G: follow Django tutorials on internationalization and use of gettext strings `_"Hola!"` and `_f"Hola {{name}}"`

### Multilingual bot development
- H: demo.yml that allows user to select lang=Ukrainean and subsequent content is in Ukrainian 

### Analytics dashboard (Delvin dashboard for each active Convo)
- [ ] V: create a ContextLog models.py and save `context` dict with every `update_context()`
    - [ ] V: convohub saves userid, timestamp, context (jsonfield) in a new ContextLog table, after each transition
- M: Delvin can ETL the messages in ContextLog
- V: convohub logs message/context objects in Delvin's preferred logging service endpoint (e.g. Kafka, Firebase, Redis, BigQuery)
- M: Delvin queries the ConvoHub ContextLog in realtime or with batch processing to have near real-time analytics
- H: Admin has dashboard to select and monitor any single chatbot room
- H: Admin dashboard shows list of all current convo sessions and their stats
- H: Admin can select a convo on their dashboard and see the convo live
- H: Admin can enable "wait for admin" on a convo monitoring view that allows them to edit and send bot messages.
- H: Admin can select/enable a timeout on the "wait for admin" feature

### Developer documentation (terms of service, privacy, tutorial, etc.)
- link to software LICENSEs on front page footer
- link to ToS on front page footer
- Privacy Policy on front page footer
- All public convo_home.html footers have links to privacy policy, ToS, and LICENSE.md

### Public marketing documentation / copy (front page content)
- User's "spaces" or "convo_home.html" pages link to signup/signin pages
- User's convo_home.html footer and/or header with "powered by ConvoHub" linking to getting started documentation
- Front page explains what ConvoHub is and how to get started
- Front page includes suggestions of things to say to the front page chatbot to start learning how it works

### Backlog

#### User account & onboarding
- G: additional signup option GitLab
- G: additional signup option Slack

- rename User and Profile model to reflect ConvoDesigner role

#### actions and extractors
- V: example bot that can provide schedule or school calendar information
- V: at university, what classes do I have now (hardcode states for times) - 3rd year at university (software engineering, more popular and more difficult than CS)
- H: send image frontend (faceofqary) displays buttons side by side where possible and optimally sizes button margin/border
- H: actions.wait_for(dict(wait=1))  # waits one second before performing next action
- H: extract_email integrated into a working demo.yml
- H: actions.extract_url integrated into a working demo.yml
- [ ] G: extractors.extract_math_expression() as action in actions_system
- [ ] G: extractors.extract_intent_tags() for multilabel intent classifier (mathtext) as action in actions_system
- actions.signup_email
- actions.signup_email_username
- actions.summarize
- extract_text integrated into a working demo.yml - persist a `text`
- extract_bad_words
- extract_command - bash-like command such as `wiki obama`
- extract_emotions/extract_intents
