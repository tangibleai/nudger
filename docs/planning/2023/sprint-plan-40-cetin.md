# Sprint Plan

* [X] Tokenize to sentences with nlpia
* [X] Fix convo creation script
* [X] Fix spacy_language_model script
