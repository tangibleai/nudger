# Hobs Sprint-36

[gitlab-issue-36](https://gitlab.com/tangibleai/community/nudger/-/issues/36)

## Hobson
- create v2/demo.yml that says hello and goodbye
- create v3/demo_lang.yml that removes "en" keys from triggers
- create branch and MR `feature-demo-v3-yaml` and merge to staging
- create v3/demo_send_message.yml that uses v2 __next__ and also has a list of actions (but only one send_message action per list) for send_message
- create v3/demo_actions.yml that combines all __next__ states into list of send_message actions within a single state
- create branch and MR `feature-demo-v3-yaml` and merge to staging

## stretch goals (next week)
- flow diagram of how yaml data structure is parsed to decide actions and state transitions
- create v3/demo_count.yml that adds a max_count attribute to a trigger after which it is diabled (max_count for hints in a math quiz will be 2 so that at the 3rd incorrect answer the `__default__` target will be the new state)
- create v3/demo_extract.yml that uses extract_propper nouns to populate context
- create v3/demo_template.yml that uses jinja2 propper_nouns key to echo the user's name

# Vlad Sprint 36

- [ ] add quizbot.constants.py and ensure it contains all constants that are only required by the quizbot app (LANGUAGES, ALLOWED_ACTIONS)

- [ ] read/understand/test hobs `v2/demo.yml` that should say "what is your name" and reply 'Hi!' and then Goodbye
- [ ] review hobs `v3/demo_lang.yml` that says hello/goodbye but removes 'en' key from actions and triggers, **suggest changes** in 36-hobs-... merge request
- [ ] review and suggest improvements to hobs `v3/demo_send_message.yml` which uses `send_message` action
- [ ] read/understand/fix hobson's `v3/demo_actions.yml` that says hello, what's your name, "Nice to meet you {{context['propper_nouns']}}" goodbye using run('extract_propper_names')
- [ ] ensure that one new consumers.py or bot_manager function takes a single `context` argument as input
- [ ] move `run`, `update_context`, and `is_allowed()` functions from actions.py to action_runners.py
- [ ] move `extract_*` functions to `extractors.py`
- [ ] migrate the database to have a `quizbot.models.Message.context = models.JSONField(null=True, blank=True, default={})`  # FIXME VladJSONB `context` dict 
- [ ] `quizbot.models.State.actions = models.JSONField(null=True, blank=True, default=[])`
- [ ] check for type of actions object, if `dict` then process with v2 (`'en': ...`) if `list` process with v3 (`[{name: extract_proper_nouns}, {name: send_message}]`  )
- [ ] test for `v2/demo.yml`
- [ ] make it execute a simple `v3/demo.yaml`

### Backlog (bad idea)

- [ ] migrate the database to have actions `quizbot.models.Action.states = models.ManyToMany('State')`
- [ ] `Action.name = models.TextField() or CharField`
- [ ] `Action.kwargs = models.JsonField()`
- [ ] `Action.args = models.JsonField()`
- [ ] create a version switch for reading v2 or v3 yaml files