# Sprint Plan

## Notes: Oct 24, 2023

- Regarding Convoscript and Convograph:
  - I think the form needs a re-design to make it obvious that all of the CRUD operations are possible for the user. I can recall my first interaction with the form as not being intuitive at all. I did not understand what was happening.

- My understanding of the UX for Convoscript and Convograph, stated as user stories:
  - As a user, I can create a chatbot conversation.
  - As a user, I can create chatbot conversation states that represent the possible user inputs/triggers and subsequent chatbot actions/statements.
  - As a user, I can switch between my chatbot conversations
  - As a user, I can edit/delete the states in my chatbot conversations
  - As a user, I can click on a graph state node to view that state data in the form
  - As a user, I can click on previous or next state buttons to view the prior or next state
  - As a user, I can click on a Add New State button to create a new chatbot conversation state
  - As a user, when I click on next or previous states, the graph view will focus on the subsequent state node


- Form:
  - CRUD UX/UI:
	- Question: designing the form to allow user to do these operations intuitively?
      - Creating
	  - Reading
	  - Updating/Editing
	  - Deleting


## Tasks
- 
- Data model for the context sent to the convoscript html templates needs to be re-done
  - [x] Added convo ids to context