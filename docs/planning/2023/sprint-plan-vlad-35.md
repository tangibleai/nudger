## Sprint plan 35 (Sep 4, 2023)

- [x] integrate actions
- [x] simplify `quizbot/consumers.py` and `quizbot/actions.py` as good as possible
- [x] simplify widget code (create merge request to `faceofqary`) repo
- [x] naming conventions during all refactor, simplifications ...
- [x] finish developing Api application
- [x] move all non-views functions to api and make them as endpoints
- [x] from bothub and dashboard js files send requests to api endpoints
- [x] refactor `quizbot/consumers.py` + `quizbot/tasks.py`:
  - [x] leave only overriden methods in `QuizRoomConsumer` consumer class
  - [x] remove task from `quizbot/tasks.py` and in consumers use only left one
- [x] move constants module from `quizbot` to `nudger` and fix imports as well
- [x] remove useless modules and parts of code in `quizbot` application


## Sprint plan 36 (Sep 11, 2023)

- [x] fix issue with CORS by moving to whitenoise essentially
- [x] implement support for actions
  - [x] move convograph linter to separate folder called `convograph_validator`
  - [x] create there `core`, `v2` and `v3`. Inherit core classes implementing support for v2 and v3 validators
- [x] create a separate bot manager for v3 format


## Sprint plan 37 (Sep 25, 2023)

- [x] use `version` keyword in start state of conversation to point on number of file format
- [x] create a separate `Action` model in database to store action
- [x] final integration of actions
  - [x] separate v2 and v3 functionality


## Sprint plan 38 (Oct 2, 2023)

- [x] use groups to send messages to
- [x] query database in v3 conversation manager instead of handing a yaml to python representation
- [x] update a faceofqary widget to order messages
- [x] update context before and after execution of action
- [x] apply clean code rules to actions, action runners and conversation managers
- [] integrate buttons with v3 conversation manager
- [x] use Jinja2 template system to render messages


## Sprint plan 39 (Oct 9, 2023)

- [x] provide option for users to upload and views images on profile page
- [x] add `send_image` action
- [x] support for buttons in v3 conversion
