The Rori content team provides a lot of the math quizzes in CSV format.  It can be easy for conversation designers and content developers to work with the CSV format for individual quizzes.  However, we need to convert the quizzes into a format our chatbots can read.  The Nudger chatbot stack uses YAML as the foundational data structure for conversational content.

We'd like you to develop a script that can convert math quiz CSV files into YAML files.


## Sprint Plan 5 Tasks
* [ ] Integrate the script into the [convomeld repository](https://gitlab.com/tangibleai/community/convomeld) as a module
* [ ] Create a [PyPI account](https://pypi.org/)
* [ ] Send Hobs and Greg the PyPI account username
* [ ] Order the microlessons from the spreadsheet (G9.A3.4.1.1) for one domain only (such as "Algebra")
  * Order the microlessons by grade level first (G9)
  * Order the microlessons by skill code second (A3.4.1)
  * Order the microlessons by microlesson number (1)
  * The last microlesson within a grades skills should connect to the first microlesson of the next grade's skills
* Create doctests as helpful for reading code


## Sprint Plan 4 Tasks

For this week, we will focus on getting spreadsheet tabs concatenated correctly.  Our main focus will be on reflecting the some of the more recent conventions in YAML format specification.

* [ ] Conventions to be aware of...
	* `start` / `stop` states are mandatory
	* Every state needs to have `name`, `convo_name`, `convo_description`
	* `name` should be include both `convo_name` and `name`: M1.G1.1.1.2.4/start
	* `convo_description` should be formatted as `####M1.G1.1.1.2.4/start\nA lesson about quadradic equations\n\n`
	* `convo_description`'s description can be whatever you want for now
	* `convo_description` needs to build (ie., append each convo_description to all other convo_descriptions before it)
	* `nlp` should be present in each state, but right not it is hardcoded to use keyword searches
	* All states, including the start state, should have a "default" state that points back to the current state (ie., M1.G1.1.1.2.4/q1 should "target" M1.G1.1.1.2.4/q1)
	* The stop state will always default back to the start state to allow for restarting the conversation.
	* Generally, it's preferable for each state to only have one "action" (1 message, not 2).  An action (ie., message) can be an empty string if necessary.
* [ ] Gather all microlesson titles from all (2) microlessons into a variable called `convo_names` in the format: ["M1.G1.1.1.2.4", "M1.G1.1.1.2.5"]
* [ ] Gather all states from all (2) microlesson into a variable called `convo_states` (optional- just an idea)
* [ ] Gather all descriptions in from the microlessons into a variable called `convo_descriptions` in the format in the format specified above
* [ ] Create an empty start state that includes all the necessary fields (ie., `name`, `convo_name`, etc.) to move to the microlesson start state, but doesn't have any messages, etc.
	* [ ] New start state `name`: start
	* [ ] New start state `convo_name`:   `f'{convo_names[0]}__{convo_names[1]}'`
	* [ ] New start state `convo_description`: `f'####{convo_names[0]}\n{convo_descriptions[0]}\n\n####{convo_names[1]}\n{convo_descriptions[1]}'`
	* [ ] New start state actions: `en: []`
	* [ ] New start state triggers: `en: __next__: f'{convo_names[0]}/start'`
* [ ] Make sure all state names that the script produces follow the convention: `{convo_names[0]}/{state["name"]}` (ie., M1.G1.1.2.2.4/start)
* [ ] Ensure all "button targets" and "trigger targets" link to the proper state name
* [ ] Give a status report (~5 min) about where you are at with the task this week

Note: It would be useful to schedule meetings with Hobson and/or Cetin directly to resolve questions related to this week's tasks as they have the most up-to-date information.

```
# def concatenate_conversations(convo0, convo1):
#   """
#   0) `convo_names = [convo0[0]["convo_name"], convo1[0]["convo_name"]]`
#   1) `convo_descriptions = [convo0[0]["convo_description"], convo1[0]["convo_description"]]`
#   2) new empty start state,
#   3) Give new start state a name: f'{convo_names[0]}__{convo_names[1]}'
#   4) Give new start a description: f'####{convo_names[0]}\n{convo_descriptions[0]}\n\n####{convo_names[1]}\n{convo_descriptions[1]}'
#   5) Add triggers: en: __next__: f'{convo_names[0]}/start'
#   6) actions: en: []
#   7) Rename all the states in convo0: `for state in convo0 f'{convo_names[0]}/{state["name"]}'`
#   8) rename all the trigger targets
#   9) ...repeat 6 and 7 for convo1
#   """
#   return convo0_then_convo1
```
