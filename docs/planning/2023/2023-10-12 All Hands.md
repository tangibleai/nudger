# 2023-10-12 All Hands

## Vish

#### Rose: dropdown menu on ConvoHub to select conversation in converscript
#### Bud: save and undo button are in progress but not working on all examples
#### Thorn: trying to implement OpenCyc (challenging) (suggested NELL as a place to start)

## Maria

#### Rose: Delvin visualization that allows user to click down to an individual conversation analytics (pushed streamlit farther than it is supposed to go). Login using djsoer on Delvin
#### Bud: React and state management with `redux`. Learning how to user `djoser` for user
#### Thorn: middle east horror

## Greg

#### Rose: Translation for SFCG done Arabic and French (translators did image translations) -
#### Bud: new endpoints for Rori mathtext/delvin
#### Thorn: big glasses so have to ge new ones, high school glasses wore out.

## Hobs

#### Rose: pens down on book
#### Bud: pens up on code blocks, SciFi - Interference sequel to Semiosis
#### Thorn: middle east and my inability ignore WW III

## Ideas
- Hobson bug Mohammed about invoice for translation
- Cycle between altruists and sociopaths and supercooperators 
- tragedy of commons is relevant to desctruction of environment (climate change) - came up with nonprofit that nudges by promoting, publicizing others doing good stuff to prevent climate change
- experiment like prisoner's dillema with multiple people to model tragedy of commons
- SBF getting justice, claiming to be a supercooperative altruistic utilitarian
