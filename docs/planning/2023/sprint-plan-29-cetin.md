# Sprint 29

- [X] Remove sms_nudger, and move anything related to project inside users.
- [X] Ask to Greg to log messages to Sentry. If Qary are showing up there?
- [X] Create an issue on Gitlab for bug report, describe it put the log.
- [X] Create a staging server create a staging branch from main.
- [X] Second database, second object, second Redis, Celery.
- [X] Delete unused packages from Nudger
- [X] Move helpers
- [X] Rename Error in helpers and don't use standard names for Classes, Functions etc.
- [X] Create a refactor branch, add people to review, after that merge to staging.
- [X] use TDD
- [X] Create a two language yaml file, as english2, and test the functions.
