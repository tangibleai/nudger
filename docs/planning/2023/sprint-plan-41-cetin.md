# Sprint Plan

#### Tasks active
* [X] Merged staging to main, fixed deployment.
* [X] Celery inside the main service.
* [X] Update .env's on bitwarden values and name from nudger to convohub
* [X] Update on wiki.py
* [X] Update on pulsar, failed after the convo change. Made it more generic.
* [X] Deploy a simple docker container on DO.
