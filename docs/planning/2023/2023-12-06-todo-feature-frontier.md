## Urgent

1. [ ] bug: user attempts to sign up with exisitng e-mail and gets 500 error
   - [ ] display warning that e-mail is already taken
1. [ ] create a href for yaml file paths in all tables so that file can be downloaded
1. [ ] send e-mail confirmation during signup
1. [ ] <a href> hint text below Signup form "Need help?" which links to new "Reset password" form.
   - [ ] Reset password form generates random temporary password and sends it to user in e-mail.
   - [ ] New form that allows user to change their password by entering their existing password (which may be the random password recently e-mailed to them) and confirming a new password twice in two separate fields
   - [ ] Add reset cirular icon button (2 arrows in a circle like in BitWarden) with mouseover text "generate random password" to all password fields
   - [ ] Add reset cirular icon button (2 arrows in a circle like in BitWarden) with mouseover text "generate random password" to all password fields
   - [ ] Add 
1. [ ] returning to convohub within 30 days should not require user to log in again
   - [ ] Login form has checkbox with text "Stay logged in for 30 days"
   - [ ] 
   - [ ] store user fingerprint (IP address, browser, OS) in cookie on users machine
   - use cookie to automatically log in the user if IP address and other fingerprin
1. [ ] bug: user attempts to sign up with exisitng e-mail and gets 500 error
   - [ ] fix: rather than 500 page, validate Django signup form before submission and display Django Alert that e-mail is already taken
   - [ ] feature: when user signs up with duplicate e-mail display modal dialog box that asks whether the user would like to sign in or reset their password for that e-mail address
1. [ ] oauth2 social signin using djoser
   - [ ] github
   - [ ] gitlab
   - [ ] discord
   - [ ] mastodon (requires oauth rather than oauth2) -- https://docs.joinmastodon.org/methods/oauth/
1. [ ] improve signup flow by allowing user to sign in using "magic link" in e-mail to the address on file
2. [ ] after signup redirect to ConvoHub page with high quality public bots at top of list
3. [ ] sortable columns to help users find bots
    - [ ] number of states
    - [ ] number of clones
    - [ ] number of clones that are activated and unmodified
    - [ ] user stars (requires additional database field and UX design)
4. [ ] Add download action to Actions pulldown on Dashboard and ConvoHub table pages
5. [ ] Change '------' puldown action to "Action"
6. [ ] Style the action pulldown to look like the FILTER pulldown.
7. [ ] Hide the filter pulldown.
8. [ ] Use convo app as pattern for flowbite table component within ConvoHub table page
   - [ ] clone and run the convo repo
   - [ ] talk to Greg about his plan for incorporating into convohub
   - [ ] connect to ConvoHub rest api for Convo model rather than local sqlite Convo Model
   - [ ] table columns need to match Convo fields and ConvoHub columns
9. [ ] Logout should redirect to the home page

## Backlog

- gitlab social sign-in for convohub using djoser & oauth2 like:
  - https://github.com/hvitis/social-djoser-template
  - https://github.com/sunscrapers/djoser
- convo or convohub graph visualization with svelte+three.js: https://madewithsvelte.com/svelte-cubed
- use flowbite svelte components for djoser signup/login/resetpw/verify e-mail passwords
  - https://flowbite-svelte.com/docs/components/forms#sidebar-label
- api.qary.ai/project-name/ should enable conversation management POST requrest (get-next-state, list_convos, etc)
- integrate api.qary.ai into qary-cli
- user can upload a spreadsheet with their chat script to create a chatbot ([spreadsheet_to_db](https://gitlab.com/tangibleai/community/convohub/-/blob/staging/hub/spreadsheet_to_db.py?ref_type=heads))
  - The user can upload a .csv file.
  - The user can upload a .xlsx file.
  - The user can implement a chatbot that just sends a series of text messages.
  - The user can implement a chatbot that sends an image the user has uploaded to their Profile (UserImage, not a profile icon).
  - The user can implement a chatbot that sends a message with one button option.
  - The user can implement a chatbot that sends a message with two button options.
  - The user can implement a chatbot that accepts user input.
  - The user can implement a chatbot that saves a variable and uses a condition to check for the variable when choosing the next state.
  - The user can implement a chatbot that saves multiple values for a single conversational turn (ex. name, wrote_name (for churn funnel or A/B tests))
  - The user can implement a chatbot that saves a variable and uses three conditions to check for the variable and the variable's characterics when choosing the next state.
  - The user can implement a chatbot that saves a user's button selection to a variable.
  - The user can get helpful error messages when uploading an invalid spreadsheet to guide them on how to correct the spreadsheet ([linter](https://gitlab.com/tangibleai/community/convohub/-/blob/staging/hub/spreadsheet_linter.py?ref_type=heads)).
