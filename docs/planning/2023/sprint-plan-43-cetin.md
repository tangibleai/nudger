# Sprint Plan

#### Tasks active
* [X] Create docker images from the delvin repository
* [X] Push the images to Docker Hub
* [X] Create a sample kubernetes cluster on Digital Ocean
* [X] Create a sample deployment on Digital Ocean
* [X] Create a sampe GitLab CI/CD pipeline that tests the commit and pushes images to Docker Hub
