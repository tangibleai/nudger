# Ideas 

## Backlog

### MOIA Frontend (chameleon)

* [] R?: Move GitLab images: Research render.com object storage
* [] R?: Move GitLab images: Move Poly images to Digital Ocean spaces
* [] R?: Move GitLab images: Create a route in the Django app for each image to use object storage (redirect image and CSS URLs even the `build.js`)
* [] R?: Research serving static assets for the Django app using object storage
* [] R?: Improve the UI of Poly in small screens
* [] R?: Ellipsies spinner (poly is typing) three dots that loads continuously one at a time 
* [] R?: Play a sound on every message (optional)
* [] R?: Play a sound on every message that happens after a long delay
* [] R?: Play the same sound when the user clicks on Poly to start the chat
* [] R?: Play a sound when user hits Enter
* [] R?: Associate the ellipsies with "typing" sound
* [] R?: Add a new menu option to select a language
* [] R?: Add a button near the send button to bring up languages menu (like orange Rambot)
* [] R?: Grey out the buttons
* [] R?: Disable the greyed out buttons

### MOIA Backend

* [] R?: Architect the backend to send one payload only to the frontend
* [] R?: Translate the conversation into other languages (Arabic, French, Portuguese, etc)
* [] R?: Create a django view with a dashboard only accessible by admin users
* [] R?: Create a dashboard for Chameleon/nudger data 
* [] R?: Fix me: Recognize variations on Espanol etc (nlp)
* [] R?: Fix me: Convert the has to a cryptogrpahic hash that includes a salt (key) for greater security
* [] Create release notes for 0.0.01 and 0.0.2
* [] R?: Fix me: [#5](https://gitlab.com/tangibleai/nudger/-/issues/5) Improve the Celery queueing / execution of messages
* [] G?: Add a global trigger to reset conversation (copy/paste to all states - target: welcome state, reset - content dictionary)
* [] Create a dialgue for bot feedback (add to existing YAML)
* [] G?: Add security around eval function (Getattr instead)
* [] G?: Be able to specify the variable name in the context dictionary from the YAML file (save whatever the user says into the variable in contenxt)


## References:

* [Chirpy Cardinal](https://stanfordnlp.github.io/chirpycardinal/live_demo/)
* [The Chatbot That Delivers Results](https://www.getjenny.com/chatbot)
* [Customer service is great, but it's even better when it's combined with higher sales](https://www.tidio.com/)
* [Find your future customers with actionable business data](https://www.vainu.com/)
* [The Most Powerful No-Code Chatbot Builder](https://landbot.io/)
* [Meet Madi](https://www.madison-reed.com/blog/meet-madi)
* [Engage your customers instantly](https://manychat.com/)
* [Save big on your next hotel](https://help.priceline.com/)
* [Spend more time closing and less time qualifying](https://www.structurely.com/)
# Nudger improvement ideas

## High priority

- [ ] RC?: 
- [ ] RC?: 
- [ ] R?:
- [ ] C?:

## Medium priority

- [ ] Rochdi, Cetin: Exception inside chatbot models 'context_functions'. 'NoneType' object has no attribute 'keys'. Before, it was masked with a 'pass', replaced it with a 'print' now too much exception happening. Something might be wrong with the logic.  

## Low priority

- [ ] R?: 
- [ ] C?:
