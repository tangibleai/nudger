# Bugs

## Out of order messages or triggers

### Environment

Replace the delay of 3 with 0.05s on line 148 in `chat_async/consumers.py`

### Steps to reproduce

1. User clicks on the bot widget
2. Poly sends one message 
3. Poly delays a short ammount of time, and it sends a second message
4. Poly delays about 3s to send triggers all at once
5. User clicks on Spanish (Espagnol)
6. Poly display two messages in correct order with appropriate delay
7. Poly displays two triggers (Si, No) with expected delay
8. When user clicks on Si
9. Poly displays one message and two triggers (Si, No) correctly
10. User clicks on No
11. Poly displays two messages in incorrect order
12. After a delay, Poly displays three triggers 

### Should be

In step 11, Poly should send "Bienvenido" message first.


## Understand the sequence of events for logging/sending state (consumers.py > tasks.py).  Async the I/O bound tasks, and have them happen in the right order.




