0. Social sign-in
    - Svelte dialog box for social sign-in flow.
    - Link with "Forgot your password?" to reset password using e-mail link
    - SocialAccount table foaccount credentials with foreign key to User table
    - user can create account with social account
    - log in with social account
    - if they try to log in with e-mail in our social account table it fails without explaining why
1. DevOps
    - merge staging into main
    - delete staging
    - connect Docker microservice for NLU-FastAPI
    - Dockerize the celery/redis server and deploy to Render to replace existing celery instance
    - Dockerize the main convohub django server
    - [x] Database is external already
    - Backlog: split visualeditor into microservices
2. incorporate MAITAG endpoint into ConvoHub understanders.py for intent recognition (Trigger matching)
3. convoscript endpoint (convomeld algorithm) to help Mykhailo build conversations
# Social signin and login flow for ConvoHub


## high priority
1. hide username field
2. bug: connect/disconnect link on profile page -- should remove social account instance in the DB
3. test ability to sign up with social media account first before creating "canonical" email for their account
4. email on convohub doesn't match social account e-mail - create viable workflow
5. multiple social signins connected to the account

## nice to have
- test gitlab/hub workflow - user edits yaml file and make sure we can load a public yaml file
- styling of the social connect
- ask for additional gitlab/github permissions