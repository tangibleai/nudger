# Sprint 39 (Dec 16 - Dec 27)

## High priority

* [x] R1-0.5: Reproduce the bug that causes changes in the bundle style
* [] R0.5: Fix the message input size and Poly name spacing
* [] R1: Fix the shape of Poly widget icon
* [] R1: Fix the Bug 3 in `deploy-bugs.md`
* [] R1: Fix the Bug 4 in `deploy-bugs.md`
* [] R1: Reply from the backend to the invalid input by **Sorry, I don't understand you**
* [] R2: Allow the backend to normalize the user input (chat_async/model.py line 295)

## Medium priority

* [] R1: Create a test that reproduces one of the bugs in `deploy-bugs.md`
* [] R1: Fix any of the integrations tests that failed
* [] R2: Do more of the previous steps for more testing

#### Low priority

* [] R?: See if the `InteractionData` is being recorded
    * [] R?: Save the JSON line disctionary into a JSON field (Go to **chat_async/model** and check the line 417 in the `InteractionData`)
* [] R1: Measure the response time in the task t0=time.time();runtest();t1=time.time();response_time=t1-t0
* [] R0.2: assert response_time < threshold (threshold = 2) (eg, **self.assertLessThan(right=threshold, left=response_time**) 
)
* [] R0.5: Find wespeak... web page in archive.org
* [] R0.5: Download the wespea... web page from archive.org
* [] R1: Move the language selection to the widget menu
* [] R2: Improve the normalization to handle typos using NLP
* [] R2: Modularize the widget settings to be modified by external services 
* [] R1: Clean up the `main` branch of `moia-poly-chatbot` repository from unused files
* [] R0.2: Merge the `secure` branch into `main`

### Done Sprint 37 (Dec 09 - Dec 16)

* [x] R0.5-0.2: Make the buttons look like buttons not messages
* [x] R0.2-0.1: Delete the normalization functionality from the frontend
* [x] R0.2-0.2: Delete the validation functionality from the frontend
* [x] R0.5-0.2: Copy the English dialog and paste it after the Spanish dialog that starts from **temperory_sp**
* [x] R1-0.2: Repeat the previous task for **Chinese Simplified** and **Chinese Traditional**
* [x] R1: Create a list within `/monitor` view to include all MOIA reports

Example:

* [2022 November](2022_november.pdf)
* [2022 November Year to Date](2022_november_ytd.pdf)

* [x] R?: Modify the settings of `moia` secure app for basic security
* [x] R0.5: Copy and paste the `users` app from the `maitag` repository
* [x] R0.5: Add and edit the necessary authentication settings
* [x] R?: Provide Django session authentication to the secure version of `moia`
* [x] R?: Give access to the `/monitor` view to the logged in users only
* [x] R?: Add a new endpoint for logged in users to change their passwords
* [x] R?: Make the `moia` templates prettier
* [x] R?: Add form validations to the login and password change page
* [x] R1: Give the HTML elements ids to make them unique globally
* [x] R0.5: Fix the shape of Poly profile image