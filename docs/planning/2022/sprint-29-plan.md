# Sprint 29 (Oct 07 - Oct 14)

### Backend (high priority)

* [x] R2-1: Connect the chain tasks to the channel consumers
* [x] R2-1: Call the chain within the channel consumers
* [] R2: Test the bugfix and share it with your team

### Frontend (less priority)

* [] R1: Include user feedback in a new file called **feedback.md**
* [] R1: Add ellipsis spinner that loads continuously before every bot message
* [] R2: Improve the UI based on user feedback 

## Done: Sprint 27 (Sep 30 - Oct 07)

* [x] R1-1.2: Take a refresher look at Tangible AI [security policy](https://gitlab.com/tangibleai/team/-/blob/main/handbook/security-policy.md) before you access MOIA data
* [x] R1-0.5: Have Maria grant you access to the MOIA data
* [x] R0.5-0.5: Enable 2FA for Bitwarden and your Tangible AI GMAIL address
* [x] R2-2: Generate the MOIA analysis report for September before next Wednesday
* [x] R1-1.5: Prepare for MOIA report presentation
* [x] R2-0.5: Expanding the Poly chain example to help you fix the OOO bug in real Poly
* [x] R1-1: Create a task to get the current state object
* [x] R1-0.5: Create a task to get the bot text messages
* [x] R1-0.5: Create a task to get the bot triggers
* [x] R2-1: Implement a chain that includes the tasks above
* [x] R1-0.5: Understand `apply_async()` and the attribute `countdown`


