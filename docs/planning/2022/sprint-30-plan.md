# Sprint 30 (Oct 14 - Oct 21)

* [X] H1: Send Rochdi Google Doc for 2020 MOIA proposal
* [x] R1-0.1: Ask Greg if he remembers a list of MOIA high-level features and tasks
* [x] R1-1: Have Greg explain to you the list of MOIA high-level features
* [x] R1-0.2: Clone MOIA-poly-chat repository and run it locally
* [x] R1-0.5: Get familiar with `MOIA-poly-chat repository`
* [x] R0.1-0.1: Create a branch called `secure` to integrate the `chat-async/` into it
* [x] R0.5-0.2: Clean the project from the unused file, apps, settings, etc
* [x] R1: Deploy MOIA-poly-chatbot to Render
* [x] R1: Merge the chat-widget into the Poly home template 
* [x] R0.2-0.2: Add a favicon to the base template of the app

## Done: Sprint 29 (Oct 07 - Oct 14)

* [x] R2-1: Connect the chain tasks to the channel consumers
* [x] R2-1: Call the chain within the channel consumers