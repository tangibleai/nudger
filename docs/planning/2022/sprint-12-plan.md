# Sprint Plan

## Sprint 12: June 6 - June 13

* [ ] G: GET request that allows you to interact with a bot to get the next state (request needs to run over a REST view / same fields send in a get request to API URL / respond with JSON)
* [ ] G4: Follow TDD tutorial (once on theirs, attempt on my own w/o looking at answers)
* [ ] G: Add a textbox as an alternative to the buttons (hit submit without hitting buttons / select both text and buttons - need business logic - What to do?)
* [ ] G: Retrieve blueprint.yml from mazzana app
* [ ] G: Make a blueprint.yml file for render.com (based on Rochdi's) and set up to execute
* [ ] HG: Segment chat history by user (low priority)

### Done
#### Sprint 10 and Sprint 11
* [X] G1: meet with Vish to explain how poly_api `models.py` and `views.py` work on moia (and soon on nudger)
* [X] G1: meet with vish to explain how you integrated poly_api into nudger
* [X] H1: give Greg new sendgrid keys
* [X] H4: get qary passing (`from qary.init import maybe_download` fails in nudger)
* [X] G2: finish debugging greg's branch so that cicd passes tests
* [X] G4: push/merge (merge requsest already created) to staging and make sure admin interface works for nudger and poly_api apps
* [X] G1: make sure django poly_api app and your chat view works on a deployed instance of nudger.qary.ai
* [X] G1: make sure poly_api REST api works on nudger.qary.ai
* [X] G1: communicate with maria some curl, wget, or postman requests that she can run
* [X] updated chat interface CSS styling and the interaction history
* [X] HG: create account on render.com and associate it with Maria's account to get free CPU hours

### Backlog

#### YAML upload/django-CRUD feature (convoscript)

* [ ] V8: yaml upload form/view
* [ ] V20: javascript app for pretty frontend convoscript
* [ ] V8: django views for CRUD (convoscript)


#### Main (chat.qary.ai)

* [ ] HG4: use terraform to create new droplet with nginx.service and gunicorn.service running
* [ ] HG1: `git push origin staging` and `deploy.sh` with new droplet IP from tf output above to deploy staging to new instance of staging branch


#### Other

* [ ] HG: integrate User model from sms_nudger into poly_api (just make sure poly_api doesn't have any FKs to a different User model than sms_nudger), deploy and verigy user signup for nudger still works. 
* [-] HG: create gitlab repo for render.com django experiment
* [ ] HG: add gitlab-ci.yml and blueprint.yml for render.com within django experiment repo for some empty/boilerplate example django app
* [ ] HG: add badge on README.md that tells you when last push to main successfully deployed to render.com
* [ ] HG: gradually incorporate nudger django app into render.com django experiment repo and keep deploying it until deployment breaks
* [ ] HG: attempt to deploy the Docker image for prosocial-ai-playground repo to render.com
* [ ] H2: Merge feature-data-file-extraction and feature-test-creation to main and deploy to render.com or GCP depending on how render.com django and GCP resize experiments above went


### References
* [Demo Website](https://nudger-test-tusf.onrender.com/reply/poly/interactive)
* [Test-Driven Development Tutorial](https://brntn.me/convoscript/django-tdd-practice-time-api)
