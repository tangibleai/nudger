# Sprint(s) for building ConversationalPipeline

Huggingface has a ConversationalPipeline class that I'd like to use to define our rule-based chatbot.

0. Go through this tutorial: https://huggingface.co/blog/gradio-spaces
1. Deploy one of these examples to Huggingface Spaces: `https://programtalk.com/python-more-examples/transformers.ConversationalPipeline/`
2. Create a ConversationalPipeline object that implements a simple rule based chatbot
3. Deploy and test the new pipeline huggingface Spaces or their production cloud service (I'll give you access to the Tangible AI account)
4. modify the rule-based chatbot to consume our nudger/v3.dialog.py or qary/v4.dialog.yml file format that defines what the bot says and the triggers/rules
5. deploy and test the new data-driven rule-based chatbot on huggingface Spaces or their production cloud service
6. figure out ;) how to integrate the generative chatbot from 1 into the rule-based chatbot of 5