# Sprint 38 (Dec 09 - Dec 16)

## Higher priority

* [x] R0.5-0.2: Make the buttons look like buttons not messages
* [x] R0.2-0.1: Delete the normalization functionality from the frontend
* [x] R0.2-0.2: Delete the validation functionality from the frontend
* [x] R0.5-0.2: Copy the English dialog and paste it after the Spanish dialog that starts from **temperory_sp**
* [x] R1-0.2: Repeat the previous task for **Chinese Simplified** and **Chinese Traditional**
* [x] R1: Create a list within `/monitor` view to include all MOIA reports

Example:

* [2022 November](2022_november.pdf)
* [2022 November Year to Date](2022_november_ytd.pdf)

* [x] R?: Modify the settings of `moia` secure app for basic security
* [x] R0.5: Copy and paste the `users` app from the `maitag` repository
* [x] R0.5: Add and edit the necessary authentication settings
* [x] R?: Provide Django session authentication to the secure version of `moia`
* [x] R?: Give access to the `/monitor` view to the logged in users only
* [x] R?: Add a new endpoint for logged in users to change their passwords
* [x] R?: Make the `moia` templates prettier
* [x] R?: Add form validations to the login and password change page
* [x] R1: Give the HTML elements ids to make them unique globally
* [x] R0.5: Fix the shape of Poly profile image

## Medium priority

* [] R1: Reply from the backend to the invalid input by **Sorry, I don't understand you**
* [] R2: Allow the backend to normalize the user input
* [] R1: Create a test that reproduces one of the bugs in `deploy-bugs.md`
* [] R1: Fix any of the integrations tests that failed
* [] R2: Do more of the previous steps for more testing

#### Low priority

* [] R1: Measure the response time in the task t0=time.time();runtest();t1=time.time();response_time=t1-t0
* [] R0.2: assert response_time < threshold (threshold = 2) (eg, **self.assertLessThan(right=threshold, left=response_time**) 
)
* [] R0.5: Find wespeak... web page in archive.org
* [] R0.5: Download the wespea... web page from archive.org
* [] R1: Move the language selection to the widget menu
* [] R2: Improve the normalization to handle typos using NLP
* [] R2: Modularize the widget settings to be modified by external services 
* [] R1: Clean up the `main` branch of `moia-poly-chatbot` repository from unused files
* [] R0.2: Merge the `secure` branch into `main`
* [] R?: Replace the YAML file with anything (recommended: Cetin) in **qary.ai**
* [] R?: Replace the Poly icon and profile with qary logo in **qary.ai**
* [] R?: Help Cetin deploy his `nudger` to our domain **qary.ai**

### Done Sprint 37 (Dec 02 - Dec 09)

* [x] R0.5-0.2: Review Cetin's merge request and merge it into `main`
* [x] R0.5-0.5: Generate new bundle files that connects to `nudger/quizbot`'s web socket
* [x] R2-2.5: Help Cetin to deploy `quizbot` to qary.ai and get the widget working
* [x] R1.5-1.2: Create MOIA report analysis for November
* [x] R0.2-0.2: Replace the front image with "for testing only"
* [x] R0.2-0.2: Replace the text **We Speak NYC** with **We Test Websites**
* [x] R1-0.9: Keep only the `index.html` and few pages in the [test-poly-wespeaknyc](https://gitlab.com/tangibleai/test-poly-wespeaknyc) repository
* [x] R0.2-0.2: Go to the **wespeakny.cityofnyc.us** website and reproduce the [Bug 1](https://gitlab.com/tangibleai/moia-poly-chatbot/-/blob/secure/docs/deploy-bugs.md#bug-1-1)
* [x] R0.1-0.1: Send wespeaknyc.qary.ai to someone to test the widget on Windows