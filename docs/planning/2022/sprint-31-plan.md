# Sprint 31 (Oct 21 - Oct 28)

* [x] R2: Get the Poly widget working in qary.ai home page (related to the tasks below)
    * [x] R: Create a PostgreSQL database on Render to handle concurrency
    * [x] R: Change the `./start-celery.sh` commands to the new command
    * [x] R: Test the widget in production to see if messages show up
    * [] R?: <del> Create a Flower web service to monitor Celery tasks </del>
    * [] R?: <del> Check timezones ib both servers </del>
    * [] R?: <del> Increase  the size of the server (3 cores) </del>
    * [] R?: <del> Start celery in the django server </del>
    * [] R?: <del> Delete the celery background worker </del>
* [x] HR1: Read and edit the proposal draft by adding the new upcoming features
* [x] R1-0.5: Add the database models of `chat-async/` to `poly/chat` app
* [x] R1-0.8: Setup Poly conversation data locally and in prodcution
* [x] R0.5-0.8: Deploy/test the admin page and database in production
* [x] R0.5-0.5: Configure the project settings to use web sockets and channels
* [x] R0.5-0.2: Configure the project settings to use Celery 
* [x] R1-0.2: Integrate the `ChatConsumer()` class into `poly/chat` consumers
* [x] R0.5-0.4: Add room templates to the app
* [x] R0.5-0.1: Add `chat-async` Celery tasks to the `poly/chat` app
* [x] R0.5-0.5: Run and test the integration locally
* [x] R1-0.5: Create a Celery background worker that will process Celery tasks
* [x] R1-0.5: Render Redis instance as the Celery broker 
* [x] R1-0.2: Run Flower with Celery to monitor Celery tasks
* [x] R1-0.5: Deploy Poly to wespeaknyc.qary.ai

## Done: Sprint 30 (Oct 07 - Oct 14)

* [X] H1: Send Rochdi Google Doc for 2020 MOIA proposal
* [x] R1-0.1: Ask Greg if he remembers a list of MOIA high-level features and tasks
* [x] R1-1: Have Greg explain to you the list of MOIA high-level features
* [x] R1-0.2: Clone MOIA-poly-chat repository and run it locally
* [x] R1-0.5: Get familiar with `MOIA-poly-chat repository`
* [x] R0.1-0.1: Create a branch called `secure` to integrate the `chat-async/` into it
* [x] R0.5-0.2: Clean the project from the unused file, apps, settings, etc
* [x] R1: Deploy MOIA-poly-chatbot to Render
* [x] R1: Merge the chat-widget into the Poly home template 
* [x] R0.2-0.2: Add a favicon to the base template of the app
