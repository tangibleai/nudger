# Sprint 26 (Sep 16 - Sep 23)

### Backend

* [ ] R1: Test the chatbot now that Hobson has added an additional delay term to see if any messages appear out of order
* [] R1: Find good tutorials/resources to learn and understand Celery
* [] R2: Practice/experiment what you learn your [simple chat app](https://gitlab.com/rochdikhalid/django-channels)
* [] R1: Copy and paste the `/chat_async` tables into your chat app
* [] R1: Load the Poly data using the YAML file in the `chat_async` folder
* [] R1: Incorporate the Poly data into your chat app's web socket protocol
* [] R2: Implement a Celery chain for scheduling Poly messages

### Frontend

* [] R2: Serve up all frontend assets within the `nudger` app as static files
* [] R2: Improve the CSS properies of the widget icon 
* [] R1: Make the widget icon configurable
 
## Done: Sprint 25 (Sep 09 - Sep 16)

* [x] H1-.1: Bugfix for outoforder messages: add a delay to each scheduled message that is dependent on the index variable i of the for loop
* [x] R0.2-0.2: Reduce the header size 
* [x] R1-1: Change the robot icon in the chat widget to Poly
* [x] R1-1.5: Create a script tag that embed the chat widget component in any website
* [x] R1-0.5: Test the tags locally and in production
* [x] R1-1: Consolidate the global css with the build css (within components)
* [x] R1-1: Document the instructions for any developer to deploy our widget on their own website
* [x] R1-2: Serve up the bundle files within the `nudger` app as static files
* [x] R1-1: Do some manual testing to find where the out of order bug occurs in Poly

## Backlog


* Bugfix for outoforder messages: make sure all function calls within for loop are synchronous so that no parallelization of the delay calculations is happening
* Bugfix for outoforder messages: every message has a minimum delay relative to the previous message of > than 0
