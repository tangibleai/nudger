# YAML Script Documentation

## Walkthroughs
### Steps to Upload a Conversation Script
- Log in
- Click on the hamburger menu in the top right of the page.
- Click Dashboard.
- Click "Choose File", and select your conversation file.
- Click "Upload".
- Your conversation script should be uploaded to the "Public Convos" part of the dashboard.  You can test your bot by clicking the chat widget.
	- NOTE: If the conversation script has a formatting error, ConvoHub will not upload the conversation.  Instead it will provide guidance on how to improve the formatting of the script.  After fixing the error, you will need to upload the modified script file.

### Steps to Upload an Image for a Chat
* Log in
* Click on the hamburger menu in the top right of the page
* Click on Profile
* Under "Upload a new image", click "Choose File", and select your image file.
* Click "Submit"
* Your image file should be displayed under "Your photos" on the page.  Note that you need to use the file name displayed on this page in your conversation scripts in order for ConvoHub to use the image.


## Metastates
Certain states are required to be in your YAML conversation file.  They wrap the dialog that your conversation has.

### Start State
The `start` state comes at the beginning of a ConvoHub YAML file.  It describes the metadata of the conversation script itself.

| Component | Platform | Values |
|---                 |---          | ---        |
| `name` | The name of this start state | str: `start` (must be `start`) |
| `convo_name` | A name for this version of the chatbot | str: `demo-v1.0.0` |
| `convo_description` | A description of this version of the chatbot | str: `A ` |
| `nlp` | The type of natural language processing used for triggers. | str: `keyword` or `re_search` |
| `lang` | The language of the conversation.  | str: Currently `en` for English |
| `version` | The version of the ConvoHub YAML format used to format your script. | int/float: `1`, `2`, `3`, or `3.1` |
| `triggers` | The trigger object that points to the next state of the conversation. This should always have `__next__` as the `user_text` to automatically advance the conversation. | YAML obj |

``` yaml
  name: start
  convo_name: bookbot-1.2.3
  convo_description: An updated version of BookBot with science fiction books added.
  nlp: keyword
  lang: en
  version: 3.1
  triggers:
    - user_text: "__next__"
      target: greeting
```

### Stop state
The `stop` state concludes the conversation.  It comes after the final state of the conversation script.  Multiple terminal states in a conversation may lead to the `stop` state, but there should only be one `stop` state.

``` yaml
- name:  
  actions:
    - send_message:
        bot_text: Bye
   triggers:
    - user_text: "__next__"
      target: stop   # References the stop state

- name: stop  # Stop state
  triggers:
    - user_text: "__default__"
      target: start
```

 


## Actions
The following snippets describe the actions allowed in the YAML file script and show examples of how to use them.

### `send_image`
Sends an image through the chat.  The image must be uploaded to your profile in order for ConvoHub to display the image during a chat session.

``` yaml
  actions:
    - send_image:
        image_name: default.jpg
```


### `send_message`
Sends a text message through the chat.  The image must be uploaded to your profile in order for ConvoHub to display the image during a chat session.  You may send one or more messages within a single action.

``` yaml
  actions:
    - send_message:
        bot_text: "Hi, there!👋🏾  I'm BookBot, a chatbot to help you choose a book and keep track of your progress."
    - send_message:
        bot_text: "There are so many great books to read!📘  Let's get start."
```


### `extract_user_text`
Uses the response to a previous state to assign a value to a variable.  This variable can then be referenced in subsequent messages.  Note that the order matters - you must extract a value and assign it to a value before using it.

``` yaml
  actions:
    - extract_user_text:
        key_to_update: name
    - send_message:
        bot_text: "Nice to meet you, {{ name }}!"
```


## Triggers

### Button Input
Represents text the user can click on to respond to a question.  A trigger block can have one or more button inputs.  These buttons can go to the same or different states.  

NOTE:  Even though users are offered button options, they may still type text.  Typed input is case insensitive when compared to `button_text` (ie., "restart" and "Restart" and "ReStArT" are equivalent).

``` yaml
  triggers:
    - user_text: "Finished"
      button_text: "Finished"
      target: bye
    - user_text: "Restart"
      button_text: "Restart"
      target: start
```


### Typed Input

#### `__default__` state 
Stops the conversation for user input.  If no other trigger condition is met, the `__default__` trigger will activate.

``` yaml
  triggers:
    - user_text: "__default__"
      target: vlad
```


#### `__next__` state
Automatically moves on to the `target` state.

``` yaml
  triggers:
    - user_text: "__next__"
      target: ask-age
```


### Control Logic
Using triggers, you can control the logic of the conversation.  Below are some examples.

#### Required Input or move on
In this instance, the `__default__` trigger specifies what happens when the single `button_text` condition is not met.  This is useful for avoiding repetition and moving the conversation forward.

``` yaml
- name: ask-privacy-policy
  actions:
    - send_message:
        bot_text: Please agree to the privacy policy at linklinklink.
  triggers:
    - user_text: "__default__"
      target: bye
    - user_text: "Agree"
      button_text: "Agree"
      target: accept-policy
```


#### Required Input or repeat
In this instance, the `__default__` trigger specifies the same state name, `ask-privacy-policy`.  Should a user enter other input, this entire state will repeat in the chat and prompt the user again.

``` yaml
- name: ask-privacy-policy
  actions:
    - send_message:
        bot_text: Please agree to the privacy policy at linklinklink.
  triggers:
    - user_text: "__default__"
      target: ask-privacy-policy
    - user_text: "Agree"
      button_text: "Agree"
      target: accept-policy
```


### Conditions

#### `condition: contains:`
`contains` checks whether the context dictionary associated with the user has a certain key-value pair.  The key is the variable listed after `contains`.

In the example below, the conversation would automatically advance (`__next__`) if the key `name` in the context dictionary for a particular user had a value.

``` yaml
  triggers:
    - user_text: "__next__"
      target: ask-age
      condition:
        contains: name
    - user_text: "__next__"
      target: ask-name
```


#### `{variable}__contains`
Checks if a variable held in the context dictionary has a specific value.  Searches for any match, including individual characters, sets of characters, or whole words.  For example, `name__contains: a` would match "a", "cat", or "the library" because all have an "a".

``` yaml
 triggers:
    - user_text: "__next__"
      target: "books-with-an-a"
      condition:
        name__contains: a
```


#### `{variable}__len__{comparison operator}`
Checks if a variable held in the context dictionary has a value that meets a specific type of comparison or comparisons. You may specify common operators, such as `lt`, `gt`, `lte`, `gte`, or `eq`. 

Putting multiple conditions together in one `conditions` section creates an "AND" relationship between the conditions.

``` yaml
 triggers:
    - user_text: "__next__"
      target: "short-list-of-a-books"
      condition:
        name__len__lte: 5
        name__len__gt: 2
```


#### `{variable}__lower`
Lowercases all characters for a given variable held in the context dictionary.  This can be used in conjunction with other conditions to normalize text responses for comparisons.  This *does not* change the original value of the variable permanently.

``` yaml
  triggers:
    - user_text: "__next__"
      target: "books-starting-with-g"
      condition:
        name__lower__startswith: g
```


#### `{variable}__startswith`
Checks if the value for a given variable held in the context dictionary starts with a specific value.  The search can be done for a single character, a set of characters, or full words.  This is case-sensitive, meaning "A" and "a" are not equivalent.

``` yaml
  triggers:
    - user_text: "__next__"
      target: "books-starting-with-g"
      condition:
        name__startswith: G
```


#### `{variable}__endswith`
Checks if the value for a given variable held in the context dictionary ends with a specific value.  The search can be done for a single character, a set of characters, or full words.  This is case-sensitive, meaning "A" and "a" are not equivalent.

``` yaml
  triggers:
    - user_text: "__next__"
      target: "green-gables"
      condition:
        name__endswith: Gables
```


#### `{variable}__eq`
Checks if the entire user message exactly matches a specific value. This is case-sensitive, meaning "A" and "a" are not equivalent.

``` yaml
  triggers:
    - user_text: "__next__"
      target: interpreter-of-maladies
      condition:
        name__eq: "Interpreter of Maladies"
```


