# Setup a New Local Instance of Nudger

## Install
- `git clone git@gitlab.com:tangibleai/nudger.git`

- Download or build .env file

## Local redis service

```bash
docker run -p 6379:6379 -d redis:5 
```

## Set Up
- `cd nudger`

- `python manage.py createsuperuser`
  - Make username
  - Make password

- `python manage.py makemigrations`

- `python manage.py migrate`
  - If on Ubuntu, you may need to do `python manage.py migrate --run-syncdb`

- `python manage.py shell`

- `from chat_async.load_yaml import *`

- `create_states_and_messages()`
- `create_triggers()`

## Solving a Possible Database Error
You may get an error like this while creating states or triggers: *IntegrityError: UNIQUE constraint failed: chat_async_state.statename*

If so, follow the steps below:
- `exit()`
- `python manage.py runserver`
- Open browser to localhost/admin
- Log in
- Delete all states and triggers entries in the database
- `python manage.py shell`
