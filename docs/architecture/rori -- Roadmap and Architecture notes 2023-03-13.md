# Tangible AI Conversation Design Architecture

## Background

### Rori features

Tangible AI was hired to advance 3 capabilities:

1. NLU - better recognition of numbers and student intents
2. Data science - analytics to improve student engagement
3. Conversation management - scalable way to manage student progress through microlessons

### Conversation management (task 3 above)

The conversation designer(a Rori teacher or staff member) must configure a chatbot(conversation manager) in two different ways, 1. conversation content, and 2. conversation logic.

1. ** content ** -  the words(content) they'd like to be a part of the conversation.
2. ** logic ** - conversation flows(connections in a graph) that they'd like the bot to implement

#### Content (natural language words)

The teacher ( or school, or Rori staff) must define two kinds of content:

- **bot messages** (nodes or states): e.g. "Let's count by ones, what comes after 2"
- **user messages** (edges or transitions): text utterances that are understood or recognized by the bot: e.g. "3" and "three"  (+ `is_button` info)

#### Logic (for conversation manager)

The overall design of the conversation graph and how the user flows through the conversation is called the conversation design, conversation flow, or dialog tree.
This defines how and when the content(messages) are used to create the user experience.

## Qary (nudger) features

### Plan

Our baseline approach and current plan(Cetin).

- Greg & Maria: spreadsheet file
- Cetin: yaml file CMS for general conversation design(microlessons)
- Greg: Stacy translates spreadsheet into qary yaml files.
- Greg: Stacy translates spreadsheet into turnio stack.
- Cetin: database tables Trigger and Message with an admin interface(staff account)
- Cetin: Better UI with views.py for teachers to add nodes & triggers
- Vish: convoscript v1 Django form for creating those yaml files ( or database entries) requires the teacher to name nodes and create intents
- Cetin?: convoscript v2 Django form or the chatbot widget used by teacher to have a pretend conversation and that becomes a graph skeleton for the conversation design

### New features (Cetin)

1. __next__: automatically next for sequences of messages from the bot
2. __default__: e.g. wrong answers to numerical problems or unrecognized intents such as "you're stupid"

```yaml
-
  name: ready
  message:
    - "Let's count by ones, what comes after 2"
  trigger:
    - "yes": ready - state
  button:
    - "Y": ready - state
-
  name: counting
  trigger:
    - "three": correct - answer - state
    - "__default__": wrong - answer - state
  button:
    - "3": correct - answer - state
-
```

```yaml
name: ready
trigger:
  - "yes": ready - state
  - "next": next - state
button:
  - "Y": ready - state
```

Together, the triggers(edges) and states(nodes) these form a graph.

## Rori

Rori uses Turn.io and spreadsheets for their conversation manager.
It is not sustainable.
Turn.IO conversations are not yet version - controllable, do not contain any NLU, and do not have any generative model for having deep, complex conversations.
Rori content is the microlessons they currently store in spreadsheets and the logic they store in Turn.IO stacks

### Templatized dialog (Vlad's experiment)

Vlad is developing a way to parameterize and templatize the content, so that it does not require as much data entry and maintenance work as the spreadsheets or the new CMS planned by Raoul.

#### Sprint Plan
1. Template quiz / lessons(microlessons) endpoint in mathtext - fastapi `num_one`
  - e.g. template = "Let's count by ones, what comes after {start}"
  - flat file `users.json` to store user data(user skill report card and their current state)
2. You and greg can deploy and test your chatbot within a wormhole for Rori on staging. Maybe you can use the trigger "wormhole_num_one" as the user message that triggers the new experimental templatized dialog

After step 2 above we will re - evaluate our approach and decide on next steps. Some ideas include:

#### Longer term roadmap (ideas)

3. Ensure all `num_one` lesson content is stored in data files rather than in string literals:
  - flat file `counting_difficulty.csv` to store question difficulty ratings(interpolated to generate questions based on difficulty or user skill)
  - flat file `content.json` or `content.yaml` to store bot messages(templates) and triggers(intents)
4. Create a conversation manager in nudger/quizbot that uses `mathactive.microlessons.num_one.*` ( or your fastapi `num_one/` endpoint) to execute your conversation from your flat files (`users.json`, `num_one_messages.json`, `counting_difficulty.csv`) - - you only need to test it from the django shell (command line), not connect it to the frontend widget
5. Modify `nudger / quizbot / consumers.py` to detect when the bot_id or name startswith "num_one" so that it uses your conversation manager instead of the baseline one, but only for some chatbots)
    6. Deploy and test your chatbot with the qary.ai(nudger) frontend
    7. Use nudger's quizbot.UserProfile table store user data for the num_one exercise(instead of users.json) - change the functions `get_user_state()` and `get_user_difficulty()` and `update_user_skill()` to use the database or a REST API for the database within nudger at `qary.ai`
    8. Use nudger's `quizbot.Message` table to store templates for num_one(adding a `bot_type = "templatized"` field)
    9. Make sure the templates can be modified / updated / added with the nudger.quizbot admin interface(within the table called Message which should have an `is_template` field and a bot_id that starts with "num_one"
10. Create a conversation manager in nudger / quizbot that uses `mathactive.microlessons.num_one.*` to execute your conversation from the templates in the Message table
11. Create a conversation manager in nudger / quizbot that uses `mathactive.microlessons.num_one.*` to execute your conversation from the templates in the Message table
12. Use nudger's `quizbot.Trigger` table to store the num_one logic(welcome -> question -> hint -> question)
13. Load all of the mathactive content, logic, and user data from the `nudger.quizbot` database(Message, UserProfile, and Trigger tables)
14. What additional features do we need to add to the quizbot.yaml format to allow you to design templatized conversations in quizbot using the yaml file data schema(Message, Trigger, User)
15. Add the `num_one` content to the nudger.quizbot database as a new chatbot type or version(you can use the yaml loader or create a new one yaml_loader for your particular schema)
16. Create a views.py(UI) for teachers to create their own templates and logic for new lessons such as `addition` ("what is 1+1?")

#### Vlad notes

Here is an endpoint called num_one  that will be used as a single entry to my quiz:

```python
@app.post("/num_one")
async def num_one(request: Request):
  """
  Input: 
  {
      "user_id": 1,
      "user_message": "Hello" | "#answer to question",
  }
  """
  data_dict = await request.json()
  # FIXME: should be ... `data_dict.get('message_data', {})` ... (empty dict instead of empty string)
  # FIXME: don't ever use `ast.literal_eval(`; instead use: `message_data = data_dict.get(`...
  message_body = ast.literal_eval(data_dict.get('message_data', '').get('message_body', ''))
  # FIXME: Hobs changed all the lines below:
  user_id = data_dict['user_id']
  message_text = data_dict['user_message']
  bot_message_data = process_user_message(user_id=user_id, message_text=message_text)  # from mathactive repo
  # bot_message_data example: {"user_id": 1, "bot_message": "good job", "state_name": "question", "num_one_skill_score": .05}
  return bot_message_data
```

Here we pass as an input two parameters: user_id and user_message, last one is just a user answer to any question or data unit that can potentially accept user answer(that's like "Hello" or some number matching an answer to "Prolongate a sequence 1,2,3" - in our case 4).
The function that will be called on my site is process_user_message from mathactive repository. Here it is (just a skeleton with definition and functionality described):

```python
def process_user_message(user_id, message_text):
  """
  1. load users
  2. create new users
  2.5. get student state
  3. get student skill
  4. generate 3 value (start, stop, step)
  # FIXME: Hobs added steps 5-9 below
  5. get a message template
  6. if necessary generate the message text using `(start, stop, step)` and the template
  7. adjust the num_one_skill_score if they got a question wrong or right
  8. update the user data dict e.g. users[user_id] = bot_message_data = {'user_id': 1, "bot_message": "...", "state_name": "welcome", "num_one_skill_score": 0.05, "microlesson_name": "num_one"}
  9. dump the updated user data to `users.json`
  10. return the bot message text and bot state name to the fast-api endpoint function
  """
  pass
```

In this function I load a file with users data to fetch tied to user information like current_skill_score and current state user is on(can be "question" or "hint" or "introduction") or create a new user if it's not found, then I generate 3 values (start, stop, step), dump all the new data to users file and return some info from function. The remaining part is to define the constant output schema for my endpoint to return back to Greg's code calling it
