# Sprint 4 (Aug 26 - Sep 02)

> Main goal: complete the frontend
 
* [x] R2-2: Search for some online chatbot examples 
* [x] R1-1: List the new features/improvements that will be implemented in a file
* [x] R1-0.7: Add an error message when the user input doesn't match one of the listed options
* [x] R0.1-0.1: Add styling to the error message
* [x] R0.1-0.1: Change the style of the inout field when the error shows up
* [x] R0.5-0.2: Improve the style of the chat triggers (buttons)
* [x] R0.5-0.2: Resize the chat components to fit the new look of chat triggers
* [x] R0.2-0.1: Make the new triggers changes responsive to small screens (mobile, etc)
* [x] R0.2-0.2: Add a list menu to the chat header 
* [x] R0.1-0.2: Add stying to the list menu
* [x] R1-0.7: Close the menu when you click on an item or outside the menu
* [x] R0.5-0.2: Write a function that gets the "reset chat" item working
* [x] R0.5-0.2: Write a function that gets the "exit chat" item working
* [x] R0.2-0.1: Fix the noHistory div display
* [x] R1-1: Meet Maria to get an overview about the MOIA's monthly report (August, 2022)
* [x] R1-1: Do research on how "exit chat" works in other chat widget examples
* [x] R0.5-0.2: Make a spinner that loads before the conversation started
* [x] R0.1-0.1: Add styling to the spinner
* [x] R0.2-0.5: Fix the height/max-height of the chat room component
* [x] R0.2-0.2: Reset the input field and the error message when the you reset the conversation
* [x] R0.1-0.1: Fix the width of the chat room component
* [x] R1-0.5: Add three new configurations to the settings file and get the working
* [x] R2-1: Make the triggers clickable and maintain the current state
* [x] R1-0.5: Remove the triggers of the current state when one option is selected

## Done: Sprint 3 (Aug 19 - Aug 26)

> Backend tasks (Poly - nudger)

* [x] R1-0.2: Install the dependencies and run locally the latest **main** branch of `nudger`
* [x] R0.5-0.1: Configure the Channel layers settings
* [x] R0.5-0.2: Setup Celery and Celery Beat in Django
* [x] R0.5-0.2: Setup Redis server
* [x] R1-0.7: Add Redis server as a task broker
* [x] R0.5-0.5: Create the needed environment variables to store the Django/Redis credentials
* [x] R1-0.5: Test manually Poly behaviors to find bugs that need to be solved

> Frontend tasks (Chameleon)

* [x] R1-0.5: Convert numerical room id to an alphanumeric hash
* [x] R0.5-0.1: Connect the Room component to [qary.ai](https://www.qary.ai/) or [staging.qary.ai](https://staging.qary.ai/)
* [x] R0.5-0.5: Pop up the welcome message from the Web Socket server
* [x] R1-1: Fix the welcome message display on the Room component
* [x] R0.5-0.2: Add Triggers (options) to the Room component as buttons 
* [x] R1-0.2: Identify Triggers in the Room component
* [x] R1-0.5: Maintain the state by storing/updating the current state
* [x] R0.5-0.2: Have the user responses appear in the chat
* [x] R1-0.5: Normalize the user inputs to handle different formats (lowercase, etc)
* [x] R1-0.5: Submit user inputs by hitting Enter
* [x] R1-0.7: Fix the web socket connection issue when the welcome popup is off
* [x] R0.5-0.5: Prevent the web socket to be called at "Resume the ..." button
* [x] R1-0.7: Prevent the web socket to be called when exit/back to the conversation
* [x] R1-0.5: Edit the source to call the Web Socket within the Door component


