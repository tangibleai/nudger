# Developer Workflow

## Install your tools

First, make sure you have your tools set up:

- Install and configure Python (preinstalled on most OSes)
- Windows: git-bash (a POSIX compliant shell)
- Windows: install and and configure Anaconda
- [Set up your environment](#set-up-your-environment)
- [Set up git](#set-up-git)

- If you plan to help Cetin & hobs with the great migration (from Render to Docker containers), set up your devops tools:
     - Docker
     - Digital Ocean SDK
     - Render SDK
     - OpenTofu (Open Terraform)
     - Ansible
     - Kubernetes

Now you're ready to build some software:

- [Check out an issue](#check-out-an-issue)
- [Implement a new feature](#implement-a-new-feature)
- [Merge request](#merge-request)
- [Deploy to Render](#deploy-to-render)

To support your team, when you are between tasks:

- Study the [Cardinal rules](#cardinal-rules)
- Use the cardinal rules to [Review a merge request](#review-a-merge-request)
- See if we're ready for a [Merge to main](#merge-to-main)

## Set up your environment

The bash script `source scripts/build-start-dev.sh` should accomplish these steps on a linux system.
1. Clone the repository
2. Install redis
3. Create and activate a Python virtual environment
4. Install nudger and all it's dependencies and data files (use --editable)
5. Make sure you can run the celery server (see `build-start-dev.sh`) and leave it running in the background
6. Make sure you can launch the django web server (`gunicorn` or `runserver`) and leave it running in the background
7. Make sure you can browse to localhost:8000 or 127.0.0.1:8000 and interact with the chatbot

## Set up git
1. Make sure you get to see merge conflicts during `pull`s: `git config --global pull.ff only`
2. Use a name that others on the team will recognize: `git config --global user.name "Hobson"`
3. Use the same email you use in GitLab, preferably a TangibleAI.com address: `git config --global user.email "user@tangibleai.com"`
4. Use the same email you use in GitLab, preferably a TangibleAI.com address: `git config --global user.email "user@tangibleai.com"`

## Check out an issue
1. Find an issue or feature on the feature-frontier.md (roadmap) that you want to work on: gitlab.com/tangibleai/community/convohub/-/blob/staging/docs/planning/feature_frontier.md
2. Create an issue
   - Paste the appropriate text from feature-frontier.md in the issue title
   - Put Feature: at the beginning of the issue title
   - Click "Draft" or "Work in Progress" checkbox
   - Think about the implementation details and describe the first steps you plan to accomplish.  You might include:
      - The inputs, outputs, and side effects of a function
      - How the UX will change
      - How to manually test the new feature once you're done
      - If you have a doctest, make sure your module (py file) is listed in `tests/test_doctests.py`
   - Assign yourself to the issue
   - Choose labels such as "Bug", "Features", "Backlog"
3. Click "Create Merge Request" for the new Issue
   - Notice the issue number in "Closes #..." in the description.
   - Add "See Also Issue #..." for any related issues
4. Paste the issue number at the beginning of the feature_frontier.md file
5. Checkout your branch locally
   - `git pull origin main`
   - `git pull origin staging`
   - `git pull {branch-name}`
5. Commit and push only the docs/* (feature_frontier.md) changes to staging
6. Edit the merge request or issue to insert your implementation details with pseudo code where possible.

## Implement a new feature
1. change the code
2. run `black .`
3. Only if you have created a new file should you use `git add path/to/file.py`
   - Rather than add large compiled files (>5 MB) consider creating a downloader to retrieve them from object storage.
   - For large Javascript/Typescript project bundles (`bundle.js`) consider adding them to a free CDN or Digital Ocean object storage.
   - For large datasets (`*.csv.gz`), consider creating a Python object storage downloader to run when your module is imported. See `spacy_language_model.py` for an example.
4. `git commit -am 'short message'`
5. git push -u origin feature-whatever-working-on  # (or `git push` if second time)
6. write a minimal test, e.g. import your module file within a django test (tests/test_*.py)
7. `python manage.py test -vvv`
8. start local server and manually test: `source scripts/build-start-dev.sh`
9. `git pull origin staging`
10. `git push`
11. Add any **new dependencies** (Python packages) to the `pyproject.toml` file and rerun `scripts/build.sh` in your local environment.
12. run your tests locally with `python manage.py test`
13. go back to step 1 to fix tests or improve your feature until you acheive a minimal improvement to the UX

### `.env` (dotenv)

Environment variables should only be used for secrets (credentials, passwords, tokens, security settings).
Never add or modify the `.env` file in your local environment without discussing with Hobson or Greg first.
If we can't think of any other way to architect the system and you need to add a `.env` you **MUST**:

1. add the key (variable) and an example value (NO SECRETS) to the `.env_example` file.
2. add the key (variable) and the secret value to the `.env_example` file.
3. Do a `diff` between your new `.env` file and the one stored in BitWarden (as a "secure note") -- make sure you have only made the minimal changes necessary
4. Overwrite the BitWarden `.env` file with your new file contents
5. Overwrite the .env contents on the Render console for staging
6. Make sure gitlab has the new environment on the staging branch used for CI-CD


## Merge request
1. Create merge request from your branch to staging (Source: feature-whatever, Target: staging) using the alert popup in GitLab or manually here: https://gitlab.com/tangibleai/community/nudger/-/merge_requests/new
2. Create a short title that starts with DRAFT if not ready to merge or FIX if fixes a bug
3. Select a template that matches your needs
4. Ensure a line at top `f"fixes #{{issue_number}}"` (e.g. "fixes #42") for any associated gitlab issues
5. Assign yourself as the "Assignee" who will ultimately be responsible for the merge and deployment to staging
6. Add at least one reviewer.
7. Create another branch off the existing feature so you can keep working while waiting on reviewers: Go to [start a new task](#start-a-new-task)
7. Implement any suggestions/comments by reviewers (adjusting them based on your understanding of how the feature is supposed to work).
8. Wait for at least one Approval by somone besides yourself.
9. Before hitting the merge button check to see that the staging .env files are ready - SEE [Deploy to Render](#deploy-to-render) below
10. Click the blue Merge button!

## Deploy to Render
Once your MR is merged, your code will auto deploy
If you have changed any `os.environ` variables or `.env`/`.env_example` files the changes are also propagated to:
- [ ] `.env_example` # WARNING: Do not put secrets/passwords/tokens here!!!
- [ ] BitWarden: A "Secure note" named `Nudger .env staging` - add a date to the name if you have changed anything
- [ ] Render: `.env` file in [environment group](https://dashboard.render.com/env-groups) named [`staging.qary.ai`](https://dashboard.render.com/env-group/evg-cibc3h95rnuk9q80pfrg) 

## Review a merge request
Support your team with timely code reviews and merge request approvals.

1. Review the files that have changed looking for possible bugs
2. Suggest no more than one or two readability or style improvements focusing on functionality and objective measures of readability (Linter and _Clean Code_ guidelines) rather than your particular style or patterns you perfer
3. Hit the Approve button. Your approval means:
    - [ ] You assume the Assignee will implement your suggestions.
    - [ ] You think the merge request will pass tests once your suggestions are implemented
    - [ ] You think the implementation meets the requirements/documentation specified in the Issue or Feature description or docstring(s)

## Merge to main

If staging has been working well for the entire team for several days, it may be time to merge `staging` into `main`.
Talk to Hobson and Greg about your manual testing of the new features on staging give them confidence that the new code will work on the `main` ([qary.ai](https://qary.ai)).
If they agree, you can create a merge request from `staging` to `main`:

- Follow the [Merge request](#merge-request) instructions above, replacing `staging` with `main` as appropriate.
- Do a `diff` or `meld` between the Render `.env` file on `main` and `staging`. If there are changes, update the `main` `.env` file.
- Update the `.env` for `main` on BitWarden 

## Naming branches
Whenever you create a new branch please follow the rules below.

- Basic Rules
  - Stick to lowercase for branch names and use hyphens to separate words.
  - Use only alphanumeric characters (a-z, 0–9) and hyphens. Avoid punctuation, spaces, underscores, or any non-alphanumeric character.
  - Do not use continuous hyphens. feature--new-login can be confusing and hard to read.
  - Do not end your branch name with a hyphen. For example, feature-new-login- is not a good practice.
  - The name should be descriptive and concise, ideally reflecting the work done on the branch.

- Branch Prefixes
  - Feature Branches: These branches are used for developing new features. Use the prefix `feature`. For instance, `feature-login-system`.
  - Bugfix Branches: These branches are used to fix bugs in the code. Use the prefix `bugfix`. For example, `bugfix-name-styling`.
  - Hotfix Branches: These branches are made directly from the production branch to fix critical bugs in the production environment. Use the prefix `hotfix`. For instance, `hotfix-critical-security-issue`.
  - Release Branches: These branches are used to prepare for a new production release. Use the prefix `release`. For example, `release-v1.0.1`.
  - Documentation Branches: These branches are used to write, update, or fix documentation. Use the prefix `docs`. For instance, `docs-api-endpoints`.

## Conventional commit messages
Whenever you create a new commit please follow the conventions. The commit message should be structured as follows:

```text
<type>[optional scope]: <description>

[optional body]

[optional footer(s)]
```

The commit should contain the following elements:
- build: changes that affect build components like build tool, ci pipeline, dependencies, project version, etc...
- chore: changes that aren't user-facing.
- enh: changes that improve a feature.
- docs: changes that affect the documentation.
- feat: changes that introduce a new feature.
- fix: changes that patch a bug.
- perf: changes which improve performance.
- refactor: changes which neither fix a bug nor add a feature.
- revert: changes that revert a previous commit.
- style: changes that don't affect code logic, such as white-spaces, formatting, missing semi-colons.
- test: changes that add missing tests or correct existing tests.

Examples:

`feat: integration of google sign-in`

`enh(api): send an email after registration`

```text
fix: prevent wrong sequence

Introduce a sequence id on bot messages

Reviewed-by: Z
Refs: #123
```

## Cardinal rules
- Don't `print()` anything in Django apps or tests!
- Anything that affects the code or data for the project should be reviewed and approved
- Use a linter (Anaconda/Black/Flake8) that warns you of common errors such as unused imports, and undefined variables.
- Always use `log = logging.getLogger(__name__)` at beginning of all modules
- Always use `log.warning()/.info()/.debug()` -- this will automatically send messages to Sentry or the console
- No function should have more than 12 significant lines of code (cycolomatic complexity)
- Refactor carefully, using your IDE or search/replace with exact string match e.g. `r'\bfunction_or_variable_name\b', reviewing each change one at a time.

