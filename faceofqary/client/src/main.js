import App from './App.svelte';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-icons/font/bootstrap-icons.css';

// variables we reach here are declared in html template with <script> tag
let props = {
    welcome_popup_on,
    empty_message_allowed,
    name,
    bot_picture,
    ws_url,
    fetch_image_endpoint_url,
    is_s3
};
props.widget_icon = props.bot_picture;

const app = new App({
    target: document.body,
    props: props,
});

export default app;
