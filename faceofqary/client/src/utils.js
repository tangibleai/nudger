function normalize_user_input(text) {
    let options = [];

    for (let i = 0; i < currentState.triggers.length; i++) {
        options.push(currentState.triggers[i].intent_text);
    }
    // returns the first element that passed the comparison (test)
    const index = options.findIndex((e) => {
        // option and user text are converted to lowercase before comparison
        return e.toLowerCase() === text.toLowerCase();
    });

    return options[index];
}

// userInput = normalize_user_input(userInput) within send_user_input() method

async function submit_user_input() {
    let error_invalid_input = check_for_invalid_input(userInput);
    //let error_empty_input = check_for_empty_input(userInput)

    if (config.empty_message_allowed) {
        if (error_invalid_input == false) {
            send_user_input();
            socket.onmessage = function (e) {
                write_bot_response(e);
            };
            // setTimeout(write_bot_response, 1000);
        } else {
            return false;
        }
    } else {
        if (userInput.trim() !== '' && error_invalid_input == false) {
            send_user_input();
            socket.onmessage = function (e) {
                write_bot_response(e);
            };
            // setTimeout(write_bot_response, 1000);
        } else {
            return false;
        }
    }
}

function check_for_invalid_input(data) {
    errorArea.innerHTML = '';

    let options_arr = [];

    for (let i = 0; i < currentState.triggers.length; i++) {
        options_arr.push(currentState.triggers[i].intent_text);
    }

    let error = true;

    const index = options_arr.findIndex((element) => {
        return element.toLowerCase() === data.toLowerCase();
    });

    if (index >= 0 || userInput.trim() === '') {
        error = false;
        textArea.style.border = 'none';
    } else {
        add_error_message('Please select one of the listed options');
        textArea.style.border = '2px solid #EB090D';
    }
    return error;
}

function add_error_message(errorMessage) {
    errorMessage = `
        <i class = "bi bi-info-circle me-1"></i><span class = "error-message"> ${errorMessage} <span>
    `;

    errorArea.insertAdjacentHTML('beforeend', errorMessage);
    userInput = '';
    scroll_to_bottom();
}
