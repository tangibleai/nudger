rm public/build/polyface.css \
   public/build/polyface.js \
   public/build/polyface.js.map
npm install
npm run build
STATIC_DIR=../../static
cp -f public/build/polyface.* "$STATIC_DIR/polyface/"
