from .models import Trigger, TriggerCondition


def trigger_to_dict(trigger: Trigger):
    return {
        "from_state": trigger.from_state,
        "to_state": trigger.to_state,
        "intent_text": trigger.intent_text,
        "update_args": trigger.update_args,
        "update_kwargs": trigger.update_kwargs,
        "lang": trigger.lang,
        "is_button": trigger.is_button,
    }


def trigger_condition_to_dict(trigger_condition: TriggerCondition):
    return {
        "expression": trigger_condition.expression,
        "value": trigger_condition.value,
        "trigger": trigger_condition.trigger,
    }
