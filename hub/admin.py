from django.contrib import admin

from rangefilter.filters import DateRangeFilterBuilder

from .filters.convo import ConvoNameFilter, ConvoDescriptionFilter

from .models import (
    State,
    Message,
    Trigger,
    Convo,
    ConversationTracker,
    Project,
    ProjectCollaborator,
)

admin.site.register(State)
admin.site.register(Message)
admin.site.register(Trigger)
admin.site.register(ConversationTracker)
admin.site.register(Project)
admin.site.register(ProjectCollaborator)


class ConvoAdmin(admin.ModelAdmin):
    search_fields = ["id", "name", "description", "project_id__public_name"]
    list_display = ("id", "name", "description", "project_id", "activated_on")
    list_filter = [
        ConvoNameFilter,
        ConvoDescriptionFilter,
        ("activated_on", DateRangeFilterBuilder("By activated on")),
    ]


admin.site.register(Convo, ConvoAdmin)
