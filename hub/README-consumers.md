# Consumers

Communicate the bot state or conversation session (called `context`) between the frontend chat widget and the backend.
This AsyncWebsocketConsumer does not send directly to the frontend, but rather sends the context object to Celery.
scheduling the context to be sent to the frontend.

### Context

The `context` object sent to Celery contains information that the bot knows about the user and the conversation plan:
This includes:

- Convo plan for the current node (state) in the convograph:
  - **state_name**: The `models.Convo.state.name` str
  - **bot_messages**: list of dicts with the HTML text messages to be sent to the user 
  - **triggers**: `models.Convo.state.triggers.values` list of dicts
  - **convo_id**: `models.Convo.id` int
- Recent text messages from the user or the bot:
- The context may also contain additional info about the history of the conversation:
  - **user_text**: last text message from the user that triggered the transition to this bot state
  - **message_history**: list of dicts containing the text from the user and bot message bubbles displayed in the widget
- User info:
  - **lang**: The currently selected language ISO code from `constants.LANGUAGES`, e.g. "en" or "ua"

The context object sent from the frontend and received by this `AsyncWebsocketConsumer`
echos the context sent from the backend after being updated with new user actions:

- **user_text**: The user's response to the current bot messages.
- **lang**: Any updates to the user's prefered language setting (2-character ISO code)

The `context` object is used in a query to the database to find the next `Convo.state`.
object to switch to for the next dialog turn.
The `context` for this conversation session is then updated with the new `Convo.state` information.
And the updated `context` dict is scheduled for delivery by Celery task which will continue the conversation.

Example context object sent to Celery for a send_message task

```json
{
    "state_name": "q2",
    "bot_messages": [
        {"sequence_number": 0, "bot_text": "<p>Exactly right!</p>"},
        {"sequence_number": 0, "bot_text": "<p>16, 17, 18</p>"},
    ],
    "triggers": [
        {
            '"from_state"': "q2",
            '"to_state"': "q2",
            '"intent_text"': '"OK"',
            '"update_args"': "null",
            '"update_kwargs"': "null",
            '"lang"': '"en"',
            '"is_button"': "true",
        },
        {
            '"from_state"': "q2",
            '"to_state"': "correct-answer-q2",
            '"intent_text"': '"19"',
            '"update_args"': "null",
            '"update_kwargs"': "null",
            '"lang"': '"en"',
            '"is_button"': "false",
        },
        {
            '"from_state"': "q2",
            '"to_state"': "wrong-answer-q2",
            '"intent_text"': '"__default__"',
            '"update_args"': "null",
            '"update_kwargs"': "null",
            '"lang"': '"en"',
            '"is_button"': "false",
        },
    ],
    "lang": "en",
    "convo_id": "8",
    "functions_execution_result": {},
}
```

Example context object sent to the backend front frontend

```json
{
    "state_name": "q1",
    "bot_messages": [
        {
            "sequence_number": 0,
            "bot_text": "<p>Let's start by counting up by 1s....Now you try 😄</p>",
        },
        {"sequence_number": 1, "bot_text": "<p>8, 9, 10</p>"},
    ],
    "triggers": [
        {
            '"from_state"': "q1",
            '"to_state"': "q1",
            '"intent_text"': '"OK"',
            '"update_args"': "null",
            '"update_kwargs"': "null",
            '"lang"': '"en"',
            '"is_button"': "true",
        },
        {
            '"from_state"': "q1",
            '"to_state"': "correct-answer-q1",
            '"intent_text"': '"11"',
            '"update_args"': "null",
            '"update_kwargs"': "null",
            '"lang"': '"en"',
            '"is_button"': "false",
        },
        {
            '"from_state"': "q1",
            '"to_state"': "correct-answer-q1",
            '"intent_text"': '"eleven"',
            '"update_args"': "null",
            '"update_kwargs"': "null",
            '"lang"': '"en"',
            '"is_button"': "false",
        },
        {
            '"from_state"': "q1",
            '"to_state"': "wrong-answer-q1",
            '"intent_text"': '"__default__"',
            '"update_args"': "null",
            '"update_kwargs"': "null",
            '"lang"': '"en"',
            '"is_button"': "false",
        },
    ],
    "lang": "en",
    "convo_id": "8",
    "functions_execution_result": {},
    "user_text": "11"
}
```

## TODO

### Double jsonification

It looks like the keys in the list of triggers (dicts) within `context` are quoted unnecessarily.
These should be plain strings.
Perhaps there is an extra `json.loads` or `json.dumps` call somewhere hidden inside `consumers.AsyncWebsocketConsumer` or the Celery `tasks.py`

### `sequence_number`

Remove the `sequence number` from the `bot_messages` dicts.
It doesn't seem to be incremented but stays constant at 0.

Rochdi added it during his attempts to fix a bug where messages arrive at the frontend out of order.  This complicates the context data structure unnecessarily. And it probably complicates the frontend widget code.
If each message sent to the frontend had only a single bot_text message that may be simpler. But that may require adding a context_seq int to the context dictionary or a message_seq int as a new context variable and also within the list of dicts in the context variable message_history (described in the new docs).