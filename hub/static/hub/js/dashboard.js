import { getConvosFromQuery, populateConvoList } from './utils.js';

document.addEventListener('DOMContentLoaded', async () => {
    let convos = await getConvosFromQuery({ queryString: "is_list_public=false" });
    await populateConvoList({ convos, is_list_public: false });

    const searchField = document.getElementById("search_field");
    const searchButton = document.getElementById("search_button");
    async function searchAndUpdateConvoList() {
        let val = searchField.value;
        let convos;
        if (!val) {
            convos = await getConvosFromQuery({ queryString: "is_list_public=false" });
        } else {
            convos = await getConvosFromQuery({ endpoint: endpoints["search_convos"]["endpoint"], queryString: `search=${val}&is_list_public=false` });
        }
        await populateConvoList({ convos, is_list_public: false });
    }
    searchField.addEventListener("input", searchAndUpdateConvoList);
    searchButton.addEventListener("click", searchAndUpdateConvoList);


    const filterButton = document.getElementById('filter-button');
    const searchFilterConvosDiv = document.getElementById("search-filter-convos");
    const filterContainer = document.getElementById("filter-container");


    filterButton.addEventListener('click', () => {
        if (filterContainer.classList.contains("hidden")) {
            searchFilterConvosDiv.style.height = "330px";
        } else {
            searchFilterConvosDiv.style.height = "50px";
        }
        filterContainer.classList.toggle('hidden');
    });


    const startDatepicker = datepicker('#start-date', {
        id: 1,
        onSelect: instance => {
            document.getElementById("start-date").value = instance.dateSelected.toDateString();
        },
    });
    const endDatepicker = datepicker('#end-date', {
        id: 1, onSelect: instance => {
            document.getElementById("end-date").value = instance.dateSelected.toDateString();
        },
    });


    const mainCheckbox = document.getElementById("main-checkbox");
    const selectEntryCheckboxes = document.querySelectorAll(".select-entry");
    mainCheckbox.addEventListener("change", function () {
        if (mainCheckbox.checked) {
            selectEntryCheckboxes.forEach(function (element, index) {
                element.checked = true;
            });
        } else {
            selectEntryCheckboxes.forEach(function (element, index) {
                element.checked = false;
            });
        }
    });


    const action = document.getElementById("action");
    action.addEventListener("change", async function () {
        let convoIds = [];
        selectEntryCheckboxes.forEach(function (element, index) {
            if (element.checked)
                convoIds.push(element.value);
        });

        let selectedOption = action.options[action.selectedIndex];
        let endpoint = selectedOption.dataset.endpoint;
        let httpMethod = selectedOption.dataset.httpMethod;
        const urlOrigin = new URL(window.location.href).origin;
        let url = `${urlOrigin}${endpoint}`;
        await fetch(url, {
            method: httpMethod,
            body: JSON.stringify({ "convo_ids": convoIds }),
            headers: {
                'Content-Type': 'application/json'
            },
        });
        location.reload();
        action.value = "no_value";
    });

    const applyFilterButtons = document.querySelectorAll(".filter-apply");
    applyFilterButtons.forEach(async (button) => {
        button.addEventListener('click', applyFilter);
    });

    const resetFilterButtons = document.querySelectorAll(".filter-reset");
    resetFilterButtons.forEach(async (button) => {
        button.addEventListener('click', resetFilter);
    });
});


async function applyFilter(event) {
    event.preventDefault();
    var formObject = {};

    const filterFields = document.querySelectorAll('[data-group="filter-field"]');

    filterFields.forEach(elem => {
        if (elem.value) {
            formObject[elem.name] = elem.value;
        }
    });

    let queryString = "";
    for (const [key, val] of Object.entries(formObject)) {
        queryString += `${key}=${val}&`;
    }

    const convos = await getConvosFromQuery({ endpoint: endpoints["filter_convos"]["endpoint"], queryString: queryString + "is_list_public=false" });
    await populateConvoList({ convos, is_list_public: false });
    return;
}


async function resetFilter(event) {
    event.preventDefault();
    let formElem = event.target.closest('form');
    const inputField = formElem.querySelector('[data-group="filter-field"]');
    inputField.value = "";

    var formObject = {};
    const filterFields = document.querySelectorAll('[data-group="filter-field"]');
    filterFields.forEach(elem => {
        if (elem.value) {
            formObject[elem.name] = elem.value;
        }
    });

    if (Object.keys(formObject).length === 0) {
        const convos = await getConvosFromQuery({ queryString: "is_list_public=false" });
        await populateConvoList({ convos, is_list_public: false });
        return;
    }

    let queryString = "";
    for (const [key, val] of Object.entries(formObject)) {
        queryString += `${key}=${val}&`;
    }

    const convos = await getConvosFromQuery({ endpoint: endpoints["filter_convos"]["endpoint"], queryString: queryString + "is_list_public=false" });
    await populateConvoList({ convos, is_list_public: false });
    return;
}
