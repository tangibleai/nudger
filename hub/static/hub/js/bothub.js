import { getConvosFromQuery, populateConvoList } from './utils.js';


document.addEventListener('DOMContentLoaded', async () => {
    let privateConvos = await getConvosFromQuery({ queryString: "is_list_public=false" });
    for (let i = privateConvos.length - 1; i >= 0; i--) {
        if (privateConvos[i].is_public === true) {
            privateConvos.splice(i, 1);
        }
    }
    const publicConvos = await getConvosFromQuery({});
    const convos = privateConvos.concat(publicConvos);
    await populateConvoList({ convos });

    const searchField = document.getElementById("search_field");
    const searchButton = document.getElementById("search_button");
    async function searchAndUpdateConvoList() {
        let val = searchField.value;
        if (!val) {
            const privateConvos = await getConvosFromQuery({ queryString: "is_list_public=false" });
            const publicConvos = await getConvosFromQuery({});
            const convos = privateConvos.concat(publicConvos);
            await populateConvoList({ convos });
            return
        }
        const privateConvos = await getConvosFromQuery({ endpoint: endpoints["search_convos"]["endpoint"], queryString: "is_list_public=false" });
        const publicConvos = await getConvosFromQuery({ endpoint: endpoints["search_convos"]["endpoint"] });
        const convos = privateConvos.concat(publicConvos);
        await populateConvoList({ convos });
    }
    searchField.addEventListener("input", searchAndUpdateConvoList);
    searchButton.addEventListener("click", searchAndUpdateConvoList);


    const filterButton = document.getElementById('filter-button');
    const searchFilterConvosDiv = document.getElementById("search-filter-convos");
    const filterContainer = document.getElementById("filter-container");


    filterButton.addEventListener('click', () => {
        if (filterContainer.classList.contains("hidden")) {
            searchFilterConvosDiv.style.height = "330px";
        } else {
            searchFilterConvosDiv.style.height = "50px";
        }
        filterContainer.classList.toggle('hidden');
    });


    const startDatepicker = datepicker('#start-date', {
        id: 1,
        onSelect: instance => {
            document.getElementById("start-date").value = instance.dateSelected.toDateString();
        },
    });
    const endDatepicker = datepicker('#end-date', {
        id: 1, onSelect: instance => {
            document.getElementById("end-date").value = instance.dateSelected.toDateString();
        },
    });


    const mainCheckbox = document.getElementById("main-checkbox");
    const selectEntryCheckboxes = document.querySelectorAll(".select-entry");
    mainCheckbox.addEventListener("change", function () {
        if (mainCheckbox.checked) {
            selectEntryCheckboxes.forEach(function (element, index) {
                element.checked = true;
            });
        } else {
            selectEntryCheckboxes.forEach(function (element, index) {
                element.checked = false;
            });
        }
    });



    const action = document.getElementById("action");
    action.addEventListener("change", async function () {
        let convoIds = [];
        selectEntryCheckboxes.forEach(function (element, index) {
            convoIds.push(element.value);
        });
        let endpoint = action.endpoint;
        let httpMethod = action.http_method;
        const urlOrigin = new URL(window.location.href).origin;
        let url = `${urlOrigin}${endpoint}`;
        await fetch(url, {
            method: httpMethod,
            body: JSON.stringify({ "convo_ids": convoIds }),
            headers: {
                'Content-Type': 'application/json'
            },
        });
        location.reload();
        action.value = "no_value";
    });
});


async function applyFilter(event) {
    event.preventDefault();
    var formObject = {};

    const filterFields = document.querySelectorAll('[data-group="filter-field"]');

    filterFields.forEach(elem => {
        if (elem.value) {
            formObject[elem.name] = elem.value;
        }
    });

    let queryString = "";
    for (const [key, val] of Object.entries(formObject)) {
        queryString += `${key}=${val}&`;
    }

    const privateConvos = await getConvosFromQuery({ endpoint: endpoints["filter_convos"]["endpoint"], queryString: queryString + "is_list_public=false" });
    const publicConvos = await getConvosFromQuery({ endpoint: endpoints["filter_convos"]["endpoint"], queryString });
    const convos = privateConvos.concat(publicConvos);
    await populateConvoList({ convos });
    return;
}


async function resetFilter(event) {
    event.preventDefault();
    let formElem = event.target.closest('form');
    const inputField = formElem.querySelector('[data-group="filter-field"]');
    inputField.value = "";

    var formObject = {};
    const filterFields = document.querySelectorAll('[data-group="filter-field"]');
    filterFields.forEach(elem => {
        if (elem.value) {
            formObject[elem.name] = elem.value;
        }
    });

    if (Object.keys(formObject).length === 0) {
        await populateConvoList({ queryString: "is_list_public=false" });
        await populateConvoList({});
        return;
    }

    let queryString = "";
    for (const [key, val] of Object.entries(formObject)) {
        queryString += `${key}=${val}&`;
    }

    const privateConvos = await getConvosFromQuery({ endpoint: endpoints["filter_convos"]["endpoint"], queryString: queryString + "is_list_public=false" });
    const publicConvos = await getConvosFromQuery({ endpoint: endpoints["filter_convos"]["endpoint"], queryString });
    const convos = privateConvos.concat(publicConvos);
    await populateConvoList({ convos });
}
