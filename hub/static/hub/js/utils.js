import jsCookie from 'https://cdn.jsdelivr.net/npm/js-cookie@3.0.5/+esm';

async function getConvosFromQuery({ endpoint = endpoints["get_convo_list"]["endpoint"], queryString = "" }) {
    const urlOrigin = new URL(window.location.href).origin;
    let url = `${urlOrigin}${endpoint}?${queryString}`;
    let convos;
    await fetch(url)
        .then(response => response.json())
        .then(data => {
            convos = data.map(jsonString => JSON.parse(jsonString));
        });
    return convos;
}


function createCustomColumn(text, columnClass) {
    const column = document.createElement('div');
    column.classList.add(columnClass);
    const span = document.createElement('span');
    span.textContent = text;
    column.appendChild(span);
    return column;
}


async function getLoggedInUserUsername(urlOrigin) {
    let username;
    await fetch(`${urlOrigin}${endpoints["get_logged_in_user"]["endpoint"]}`)
        .then(response => response.json())
        .then(data => {
            let userData = data;
            username = userData.username;
        });
    return username;
}


async function populateConvoList({ convos, is_list_public = true }) {
    const urlOrigin = new URL(window.location.href).origin;
    const convoList = document.getElementById("convo-list");
    while (convoList.firstChild) {
        convoList.removeChild(convoList.firstChild);
    }

    if (convos.length) {
        convos[0].is_activated = true;

        convos.forEach(async (convo) => {
            const listItem = document.createElement('li');
            listItem.classList.add('list-group-item');

            const hiddenCol = document.createElement('div');
            hiddenCol.style = "display: inline-block;";
            const hiddenIdInput = document.createElement('input');
            hiddenIdInput.type = "hidden";
            hiddenIdInput.name = "id";
            hiddenCol.appendChild(hiddenIdInput);
            listItem.appendChild(hiddenCol);

            // checkbox
            const checkboxCol = document.createElement('div');
            checkboxCol.classList.add('checkbox-col');
            const checkbox = document.createElement('input');
            checkbox.type = 'checkbox';
            checkbox.value = convo.id;
            checkbox.classList.add('select-entry');
            checkboxCol.appendChild(checkbox);
            listItem.appendChild(checkboxCol);

            // name column
            const nameCol = createCustomColumn(convo.name, 'custom-col-2');
            listItem.appendChild(nameCol);

            // description column
            const descriptionCol = createCustomColumn(convo.description, 'custom-col-2');
            listItem.appendChild(descriptionCol);

            // publisher column for private list view
            if (is_list_public) {
                const loggedInUserUsername = await getLoggedInUserUsername(urlOrigin);
                let publisher;
                if (convo.created_by == loggedInUserUsername) {
                    publisher = "You";
                } else {
                    publisher = convo.created_by;
                }
                const publisherCol = createCustomColumn(publisher, 'custom-col-2');
                listItem.appendChild(publisherCol);
            }

            // file path column
            const filePathCol = document.createElement('div');
            filePathCol.classList.add('custom-col-3');

            const anchor = document.createElement('a');
            anchor.textContent = convo.file;
            anchor.href = `${urlOrigin}/quiz/bothub/${convo.id}`;
            anchor.target = "_blank";

            filePathCol.appendChild(anchor);
            listItem.appendChild(filePathCol);

            // activated on column
            const datetime = new Date(convo.activated_on);
            const options = { year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric', hour12: true };
            const formattedDatetime = datetime.toLocaleString('en-US', options);
            const activatedOnCol = createCustomColumn(formattedDatetime, 'custom-col-2');
            listItem.appendChild(activatedOnCol);

            // is public column for private list view
            if (!is_list_public) {
                const isPublicCol = document.createElement('div');
                isPublicCol.classList.add('custom-col-2');
                isPublicCol.style = "text-align: center;";
                const isPublicMark = document.createElement('img');
                isPublicMark.src = convo.is_public ? checkMarkSymbol : crossSymbol;
                isPublicMark.style = "width: 20px;";
                isPublicCol.appendChild(isPublicMark);
                listItem.appendChild(isPublicCol);
            }

            const buttonCol = document.createElement('div');            
            buttonCol.classList.add('custom-col-2');

            if (!is_list_public) {
                if (convo.is_activated) {
                    const form = document.createElement('form');
                    form.className = 'inline';

                    const submitButton = document.createElement('button');
                    submitButton.className = 'btn btn-success';
                    submitButton.textContent = 'Activated';

                    form.appendChild(submitButton);
                    buttonCol.appendChild(form);
                } else {
                    const form = document.createElement('form');
                    form.className = 'inline';
                    form.onsubmit = async (event) => {
                        event.preventDefault();
                        let endpoint = endpoints["activate_convo"]["endpoint"];
                        let httpMethod = endpoints["activate_convo"]["http_method"];
                        await fetch(`${urlOrigin}${endpoint}`, {
                            method: httpMethod,
                            body: JSON.stringify({ "convo_id": convo.id }),
                            headers: {
                                'Content-Type': 'application/json'
                            },
                        });
                        location.reload();
                    }
                    const submitButton = document.createElement('button');
                    submitButton.type = 'submit';
                    submitButton.className = 'btn btn-outline-primary';
                    submitButton.textContent = 'Activate';

                    form.appendChild(submitButton);
                    buttonCol.appendChild(form);
                }
            }
            listItem.appendChild(buttonCol);

            let user_owns_convo;
            let endpoint = endpoints["does_user_own_convo"]["endpoint"]
            let url = `${urlOrigin}${endpoint}?visitor_id=${visitor_id}&convo_id=${convo.id}`;
            await fetch(url)
                .then(response => response.json())
                .then(data => {
                    user_owns_convo = data;
                });
            if (is_list_public && convo.is_public && visitor_id && (!(user_owns_convo))) {
                const cloneButtonCol = document.createElement('div');
                cloneButtonCol.classList.add('custom-col-2');
                const button = document.createElement('button');
                button.classList.add('btn', 'btn-outline-info');
                button.textContent = "Clone";

                button.addEventListener("click", async function () {
                    let endpoint = endpoints["clone_convo"]["endpoint"];
                    let httpMethod = endpoints["clone_convo"]["http_method"];
                    await fetch(`${urlOrigin}${endpoint}`, {
                        method: httpMethod,
                        body: JSON.stringify({ "visitor_id": visitor_id, "convo_id": convo.id }),
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    });
                    let flash_message = `?flash_message=Convo has been successfully cloned. You can see it on the dashboard page.`;
                    window.location.replace(new URL(window.location.href) + flash_message);
                });
                cloneButtonCol.appendChild(button);
                listItem.appendChild(cloneButtonCol);
            }

            convoList.appendChild(listItem);
        });
    }
}


export { getConvosFromQuery, populateConvoList };