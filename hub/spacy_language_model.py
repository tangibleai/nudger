""" Ensure that a SpaCy language model has been downloaded and is a Monad (Singleton) """

import spacy
from spacy.cli import download


from convohub.constants import SPACY_MODEL


def load(spacy_model=SPACY_MODEL):
    spacy_model = spacy_model or SPACY_MODEL or "en_core_web_md"
    try:
        return spacy.load(spacy_model)
    except OSError:
        download(spacy_model)
        return spacy.load(spacy_model)


nlp = load()
