from django import forms
from django.forms import ModelForm
from django.forms.widgets import Widget
from django.utils.html import format_html

from hub.models import Convo


class FileUploadWidget(Widget):
    def render(self, name, value, attrs=None, renderer=None):
        checkbox_html = """
            <div class="input-group mb-3">
                <input type="file" name="file" class="form-control" id="inputGroupFile02" onchange="showCheckbox()">
                <button class="input-group-text" type="submit" style="display: inline-block;">Upload</button>
            </div>
            <div id="checkboxContainer" style="display: none;">
                <input type="checkbox" id="checkbox" name="checkbox">
                <label for="checkbox" style="display: inline-block;">Make private</label>
            </div>
        """
        return format_html(checkbox_html)


class ConvoForm(ModelForm):
    file = forms.FileField(widget=FileUploadWidget(), required=False, label="")

    class Meta:
        model = Convo
        fields = ["file"]


class ConvoSpreadsheetForm(forms.Form):
    convo_name = forms.CharField(label="Conversation Name", required=True)
    convo_description = forms.CharField(label="Conversation Description", required=True)
    sheet_id = forms.CharField(label="Google Sheet ID", required=False)
    file = forms.FileField(required=False)
    is_public = forms.BooleanField(initial=True)

    def clean(self):
        cleaned_data = super().clean()
        uploaded_file = cleaned_data.get("file")
        sheet_id = cleaned_data.get("sheet_id")
        is_public = cleaned_data.get("is_public")

        if not uploaded_file and not sheet_id:
            raise forms.ValidationError(
                "You must upload either a file or a Google Sheet id"
            )

        return cleaned_data
