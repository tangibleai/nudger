"""
Current output to frontend

{
    "type": "chat_message_event",
    "state_assets": {
        "state_name": "q2",
        "bot_messages": [
            {"sequence_number": 0, "bot_text": "<p>Exactly right!</p>"},
            {"sequence_number": 1, "bot_text": "<p>16, 17, 18</p>"},
        ],
        "triggers": [
            {
                '"from_state"': "q2",
                '"to_state"': "q2",
                '"intent_text"': '"OK"',
                '"update_args"': "null",
                '"update_kwargs"': "null",
                '"lang"': '"en"',
                '"is_button"': "true",
            },
            {
                '"from_state"': "q2",
                '"to_state"': "correct-answer-q2",
                '"intent_text"': '"19"',
                '"update_args"': "null",
                '"update_kwargs"': "null",
                '"lang"': '"en"',
                '"is_button"': "false",
            },
            {
                '"from_state"': "q2",
                '"to_state"': "wrong-answer-q2",
                '"intent_text"': '"__default__"',
                '"update_args"': "null",
                '"update_kwargs"': "null",
                '"lang"': '"en"',
                '"is_button"': "false",
            },
        ],
        "lang": "en",
        "convo_id": "8",
        "functions_execution_result": {},
    },
}


"""

import logging
import os

from asgiref.sync import async_to_sync
from celery import shared_task
from channels.layers import get_channel_layer
from django.contrib.auth.models import User
from django.utils import timezone

from users.models import Profile

log = logging.getLogger(__name__)

os.environ["DJANGO_SETTINGS_MODULE"] = "convohub.settings"


@shared_task(name="schedule_sending_chat_context")
def schedule_sending_chat_context(room_group_name, context={}):
    """Sends out an individual BOT(??) message to the channel group

    TODO: find out if this is a message from the Bot or from the user
    """
    channel_layer = get_channel_layer()

    log.info(
        f"in schedule_sending_chat_context(room_group_name={room_group_name}, context={context}):"
    )

    async_to_sync(channel_layer.group_send)(
        room_group_name, {"type": "chat_message_event", "context": context}
    )


@shared_task(name="remove_old_unactive_users")
def remove_old_unactive_users(unactivity_time=1800):
    for user in User.objects.filter(is_active=False):
        current_time = timezone.now()
        elapsed_seconds = (current_time - user.date_joined).total_seconds()
        if elapsed_seconds > unactivity_time:
            Profile.objects.create(user=user)
            user.delete()
    return
