from django.apps import AppConfig
from django.db.models.signals import post_save

from allauth.socialaccount.signals import pre_social_login


class HubConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "hub"

    def ready(self):
        from .signals import delete_expired_sessions_and_relations

        post_save.connect(delete_expired_sessions_and_relations)
        pre_social_login.connect(pre_social_login_handler)


def pre_social_login_handler(request, sociallogin, **kwargs):
    username = sociallogin.user.username
    email = sociallogin.user.email
    provider = sociallogin.account.provider

    from users.models import ExternalAccount

    ExternalAccount.objects.get_or_create(
        username=username, email=email, provider=provider
    )
