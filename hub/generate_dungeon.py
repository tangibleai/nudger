# generate_dungeon.py
""" Programmatically generate a dungeon text adventure in YAML ConvoGraph format

Grid coordinate system is x, y in a numpy array:
  - x goes positive to the right (East)
  - y goes positive up (North)
"""
import random
import yaml


from convohub.constants import DATA_DIR


path = DATA_DIR / "v3" / "demo_dungeon.yml"
assert path.is_file()
convo = yaml.safe_load(path.open())
# g[2]['triggers'][0]['target'] = g[2]['name']


def print_state(state):
    """Pretty-print the state text and triggers

    >>> print(yaml.dump(convo[2]))
    actions:
    - send_message:
        bot_text: You find a secret door in the North wall.
    name: 0,1
    triggers:
    - target: 0,1
      user_text: __default__
    """
    print(yaml.dump(state))


# 40(NtoS) x 100(WtoE) grid coorinates are positions in a string so
#    x=0,y=0 is upper left corner at grid[0][0]
#    x=99,y=39 is lower right corner at grid[39][99]


class Path:
    """represents a path in Decart coordinates system where y axis is reversed, for instance

    -5 .>>>v
    -4 .^..x
    -3 .^<..
    -2 .....
    -1 .....
       01234

    start at (2;2), end at (4;3)"""

    arrows = dict(
        N="^", E=">", S="v", W="<"
    )  # https://symbl.cc/en/collections/arrow-symbols/
    directions = dict(N=[0, -1], E=[1, 0], S=[0, 1], W=[-1, 0])

    def __init__(self):
        self.rows_count = 11
        self.columns_count = 11
        self.row = [" "] * self.columns_count
        self.grid = [
            self.row.copy() for _ in range(self.rows_count)
        ]  # mutable list of lists of chars
        self.doors_count = 10
        self.x = self.rows_count // 2  # x coordinate of a room
        self.y = -self.columns_count // 2  # y coordinate of a room
        self.path = []  # for e.g. ["N", "W", "N", "E", "E", "S"]
        self.previous_room_direction = None

    def generate(self):
        """Return a list of position deltas and an asciiart grid"""

        for room_num in range(self.doors_count):
            rooms = self.get_next_free_rooms_at_valid_positions()
            if not rooms:
                raise ValueError
            next_room = random.choice(
                rooms
            )  # can raise ValueError since there can be no availbale direction to move forward
            self.path.append(next_room)
            self.paint_current_grid_position(next_room, room_num)

            self.x, self.y = self.get_next_room_position(next_room)
            self.grid[self.y][self.x] = "X"
            self.previous_room_direction = next_room
        return self.path

    def paint_current_grid_position(self, next_room, room_num):
        self.grid[self.y][self.x] = self.arrows[next_room]
        if room_num == 0:
            self.grid[self.y][self.x] = next_room
        return

    def get_next_free_rooms_at_valid_positions(self):
        rooms = list("NESW")
        for next_room in rooms.copy():
            previous_room_direction = None
            if len(self.path) > 1:
                previous_room_direction = self.path[-2]
            elif len(self.path) > 0:
                previous_room_direction = self.path[-1]
            if not (
                self.next_room_position_valid(next_room)
                and self.next_room_is_free(next_room)
                and next_room != previous_room_direction
            ):
                rooms.remove(next_room)
        return rooms

    def next_room_position_valid(self, next_room):
        match next_room:
            case "N":
                return abs(self.y) < len(self.grid)
            case "E":
                return self.x < len(self.grid[0]) - 1
            case "S":
                return self.y < -1
            case "W":
                return self.x > 0

    def next_room_is_free(self, next_room):
        next_x, next_y = self.get_next_room_position(next_room)
        is_free = not self.grid[next_y][next_x].strip()
        return is_free

    def get_next_room_position(self, next_room):
        x, y = self.x, self.y
        match next_room:
            case "N":
                y -= 1
            case "E":
                x += 1
            case "S":
                y += 1
            case "W":
                x -= 1
        return [x, y]

    def print_grid(self):
        print("\n".join(["".join(row) + "|" for row in self.grid]))
        print("-" * self.columns_count)
        return self.grid


class PathWithDeadEnds(Path):
    def __init__(self):
        super().__init__()
        self.dead_end_pathes = {}  # for e.g. {0: ["N", "E", "W"], 4: ["E", "W"]}

    def generate(self):
        super().generate()
        dead_ends_count = random.randint(2, 3)

        self.x = self.rows_count // 2  # x coordinate of a room
        self.y = -self.columns_count // 2  # y coordinate of a room

        numbers = random.sample(range(10), dead_ends_count)
        numbers.sort()
        for i in range(len(self.path)):
            if i in numbers:
                path = self.path
                x, y = self.x, self.y
                # next_x, next_y = self.get_next_room_position(self.path[i])
                rooms_count = random.randint(2, 3)
                dead_end_path = self.generate_dead_end_path(rooms_count)
                tries = 10
                for _ in range(tries):
                    if len(dead_end_path) == rooms_count:
                        break
                    dead_end_path = self.generate_dead_end_path(rooms_count)
                if not dead_end_path:
                    raise ValueError
                self.dead_end_pathes[(x, y)] = dead_end_path
                # self.x, self.y = x, y
                self.path = path
            self.x, self.y = self.get_next_room_position(self.path[i])

    def generate_dead_end_path(self, rooms_count):
        self.path = []
        for _ in range(rooms_count):
            rooms = self.get_next_free_rooms_at_valid_positions()
            if not rooms:
                raise ValueError
            next_room_direction = random.choice(
                rooms
            )  # can raise ValueError since there can be no availbale direction to move forward
            self.x, self.y = self.get_next_room_position(next_room_direction)
            self.path.append(next_room_direction)
            self.paint_current_grid_position_2(next_room_direction)
        return self.path

    def paint_current_grid_position_2(self, next_room):
        self.grid[self.y][self.x] = next_room
        return


class PathToYml:
    """Accepts a path for e.g. ['S', 'S', 'W', 'W', 'S', 'W', 'N', 'S', 'S', 'S'] and creates a conversation data"""

    def __init__(self, path, rows_count, columns_count):
        self.conversation_data = []
        self.path = path
        self.rows_count = 11
        self.columns_count = 11
        self.x = self.rows_count // 2
        self.y = -self.columns_count // 2
        self.add_required_states()

        self.previous_room_direction = None
        self.next_room_direction = None
        self.previous_x = None
        self.previous_y = None
        self.next_x = None
        self.next_y = None

    def add_required_states(self):
        """Adds start and stop states to conversation"""
        self.conversation_data.append(
            dict(
                name="start",
                version=3.1,
                convo_name="dungeon",
                convo_description=self.path,
                nlp="match",
                lang="en",
                triggers=[
                    dict(
                        target="first_step",
                        user_text="__next__",
                    ),
                ],
            )
        )
        self.conversation_data.append(
            dict(
                name="first_step",
                actions=[
                    dict(
                        send_message=dict(
                            bot_text="Please, enter the first step: N, E, S or W."
                        )
                    )
                ],
                triggers=[
                    dict(
                        target="{x},{y}".format(x=self.x, y=self.y),
                        user_text=self.path[0],
                    ),
                    dict(
                        target="first_step_wrong",
                        user_text="__default__",
                    ),
                ],
            )
        )
        self.conversation_data.append(
            dict(
                name="first_step_wrong",
                actions=[
                    dict(send_message=dict(bot_text="Oops! You should try again."))
                ],
                triggers=[
                    dict(
                        target="{x},{y}".format(x=self.x, y=self.y),
                        user_text=self.path[0],
                    ),
                    dict(
                        target="first_step_wrong",
                        user_text="__default__",
                    ),
                ],
            )
        )
        self.conversation_data.append(
            dict(
                name="end",
                actions=[
                    dict(
                        send_message=dict(
                            bot_text="Congratulations! You escaped from dungeon."
                        )
                    )
                ],
                triggers=[
                    dict(
                        target="stop", button_text="Play again", user_text="__default__"
                    ),
                ],
            )
        )
        self.conversation_data.append(
            dict(
                name="stop",
                triggers=[
                    dict(
                        target="start",
                        user_text="__default__",
                    ),
                ],
            )
        )
        return

    def generate(self):
        self.add_first_room()
        self.add_middle_rooms()
        self.add_last_room()
        return self.conversation_data

    def add_first_room(self):
        self.direction = self.path[0]
        self.previous_room_direction = self.direction
        self.previous_x = self.x
        self.previous_y = self.y
        self.next_room_direction = self.path[1]
        self.next_x, self.next_y = self.get_next_room_position()
        self.conversation_data.append(
            dict(
                name="{x},{y}".format(x=self.x, y=self.y),
                actions=[
                    dict(
                        send_message=dict(
                            bot_text="You find a secret door in the {} wall.".format(
                                self.direction
                            )
                        )
                    )
                ],
                triggers=[
                    dict(
                        target="{x},{y}".format(x=self.next_x, y=self.next_y),
                        user_text=self.next_room_direction,
                    ),
                    dict(
                        target="{x},{y}_wrong".format(x=self.x, y=self.y),
                        user_text="__default__",
                    ),
                ],
            )
        )
        self.conversation_data.append(
            dict(
                name="{x},{y}_wrong".format(x=self.x, y=self.y),
                actions=[
                    dict(send_message=dict(bot_text="Oops! You should try again."))
                ],
                triggers=[
                    dict(
                        target="{x},{y}_wrong".format(x=self.x, y=self.y),
                        user_text="__default__",
                    ),
                    dict(
                        target="{x},{y}".format(x=self.next_x, y=self.next_y),
                        user_text=self.next_room_direction,
                    ),
                ],
            )
        )
        self.previous_x, self.previous_y = self.x, self.y
        self.x = self.next_x
        self.y = self.next_y
        return

    def add_middle_rooms(self):
        for i, direction in enumerate(self.path[1:-1], 1):
            self.direction = direction
            self.next_room_direction = self.path[i + 1]
            self.next_x, self.next_y = self.get_next_room_position()
            self.add_middle_room()
            self.previous_room_direction = self.direction
            self.previous_x = self.x
            self.previous_y = self.y
            self.x = self.next_x
            self.y = self.next_y
        return

    def get_next_room_position(self):
        x = self.x
        y = self.y

        match self.direction:
            case "N":
                y -= 1
            case "E":
                x += 1
            case "S":
                y += 1
            case "W":
                x -= 1
        position = [x, y]
        return position

    def add_middle_room(self):
        self.conversation_data.append(
            dict(
                name="{x},{y}".format(x=self.x, y=self.y),
                actions=[
                    dict(
                        send_message=dict(
                            bot_text="You find a secret door in the {} wall.".format(
                                self.direction
                            )
                        )
                    )
                ],
                triggers=[
                    dict(
                        target="{x},{y}".format(x=self.next_x, y=self.next_y),
                        user_text=self.next_room_direction,
                    ),
                    dict(
                        target="{x},{y}_back".format(x=self.x, y=self.y),
                        user_text=self.previous_room_direction,
                    ),
                    dict(
                        target="{x},{y}_wrong".format(x=self.x, y=self.y),
                        user_text="__default__",
                    ),
                ],
            )
        )
        self.conversation_data.append(
            dict(
                name="{x},{y}_back".format(x=self.x, y=self.y),
                actions=[
                    dict(send_message=dict(bot_text="You returned to previous room."))
                ],
                triggers=[
                    dict(
                        target="{x},{y}".format(x=self.previous_x, y=self.previous_y),
                        user_text="__next__",
                    ),
                ],
            )
        )
        self.conversation_data.append(
            dict(
                name="{x},{y}_wrong".format(x=self.x, y=self.y),
                actions=[
                    dict(
                        send_message=dict(
                            bot_text="Oops! You should move back or try again."
                        )
                    )
                ],
                triggers=[
                    dict(
                        target="{x},{y}_wrong".format(x=self.x, y=self.y),
                        user_text="__default__",
                    ),
                    dict(
                        target="{x},{y}".format(x=self.next_x, y=self.next_y),
                        user_text=self.next_room_direction,
                    ),
                ],
            )
        )
        return

    def add_last_room(self):
        self.direction = self.path[-1]
        self.conversation_data.append(
            dict(
                name="{x},{y}".format(x=self.x, y=self.y),
                actions=[
                    dict(
                        send_message=dict(
                            bot_text="You find a secret door in the {} wall.".format(
                                self.direction
                            )
                        )
                    )
                ],
                triggers=[
                    dict(target="end", user_text="__next__"),
                ],
            )
        )
        return


class PathToYamlWithDeadEnds(PathToYml):
    def __init__(self, path, rows_count, columns_count, dead_end_paths):
        super().__init__(path, rows_count, columns_count)
        self.dead_end_paths = dead_end_paths

    def generate(self):
        super().generate()
        dead_ends_count = random.randint(2, 3)

        self.x = self.rows_count // 2  # x coordinate of a room
        self.y = -self.columns_count // 2  # y coordinate of a room

        numbers = random.sample(range(10), dead_ends_count)
        numbers.sort()
        for i in range(len(self.path)):
            dead_end_path = self.dead_end_paths.get((self.x, self.y), None)
            if not dead_end_path:
                continue
            self.add_dead_end_path()
            self.direction = self.path[i]
            self.x, self.y = self.get_next_room_position()

    def add_dead_end_path(self):
        dead_end_path = self.dead_end_paths.get((self.x, self.y))
        x_current, y_current = self.x, self.y
        beginning_state = self.get_state_data_by_coordinates(x_current, y_current)
        triggers = beginning_state["triggers"]
        self.direction = dead_end_path[0]
        x_next, y_next = self.get_next_room_position()
        triggers.append(
            dict(
                target="{x},{y}".format(x=x_next, y=y_next),
                user_text=dead_end_path[0],
            ),
        )
        x_prev, y_prev = x_current, y_current
        x_current, y_current = x_next, y_next

        for i in range(len(dead_end_path[:-1])):
            self.direction = dead_end_path[i]
            opposite_direction = self.get_opposite_direction(self.direction)
            x_next, y_next = self.get_next_room_position()
            self.conversation_data.append(
                dict(
                    name="{x},{y}".format(x=x_current, y=y_current),
                    actions=[
                        dict(
                            send_message=dict(
                                bot_text="You find a secret door in the {} wall.".format(
                                    self.direction
                                )
                            )
                        )
                    ],
                    triggers=[
                        dict(
                            target="{x},{y}".format(x=x_prev, y=y_prev),
                            user_text=opposite_direction,
                        ),
                        dict(
                            target="{x},{y}".format(x=x_next, y=y_next),
                            user_text=dead_end_path[i + 1],
                        ),
                        dict(
                            target="{x},{y}_wrong".format(x=x_current, y=y_current),
                            user_text="__default__",
                        ),
                    ],
                )
            )
            self.conversation_data.append(
                dict(
                    name="{x},{y}_wrong".format(x=x_current, y=y_current),
                    actions=[
                        dict(send_message=dict(bot_text="Oops! You should try again."))
                    ],
                    triggers=[
                        dict(
                            target="{x},{y}_wrong".format(x=x_current, y=y_current),
                            user_text="__default__",
                        ),
                        dict(
                            target="{x},{y}".format(x=x_prev, y=y_prev),
                            user_text=opposite_direction,
                        ),
                        dict(
                            target="{x},{y}".format(x=x_next, y=y_next),
                            user_text=dead_end_path[i + 1],
                        ),
                    ],
                )
            )
            x_prev, y_prev = x_current, y_current
            x_current, y_current = x_next, y_next
        opposite_direction = self.get_opposite_direction(dead_end_path[-1])
        self.conversation_data.append(
            dict(
                name="{x},{y}".format(x=x_current, y=y_current),
                actions=[
                    dict(
                        send_message=dict(
                            bot_text="You find a secret door in the {} wall.".format(
                                dead_end_path[-1]
                            )
                        )
                    )
                ],
                triggers=[
                    dict(
                        target="{x},{y}".format(x=x_prev, y=y_prev),
                        user_text=opposite_direction,
                    ),
                    dict(
                        target="{x},{y}_wrong".format(x=x_current, y=y_current),
                        user_text="__default__",
                    ),
                ],
            )
        )
        self.conversation_data.append(
            dict(
                name="{x},{y}_wrong".format(x=x_current, y=y_current),
                actions=[
                    dict(send_message=dict(bot_text="Oops! You should try again."))
                ],
                triggers=[
                    dict(
                        target="{x},{y}_wrong".format(x=x_current, y=y_current),
                        user_text="__default__",
                    ),
                    dict(
                        target="{x},{y}".format(x=x_prev, y=y_prev),
                        user_text=opposite_direction,
                    ),
                ],
            )
        )

    def get_state_data_by_coordinates(self, x, y):
        for state_data in self.conversation_data:
            state_name = state_data["name"]
            if state_name == f"{x},{y}":
                return state_data

    def get_opposite_direction(self, direction):
        match direction:
            case "N":
                return "S"
            case "E":
                return "W"
            case "S":
                return "N"
            case "W":
                return "E"


if __name__ == "__main__":
    while True:
        try:
            path = PathWithDeadEnds()
            path.generate()
            break
        except ValueError:
            pass
    path.print_grid()
    print(path.path)
    dead_end_paths = path.dead_end_pathes
    conversation = PathToYamlWithDeadEnds(path.path, 11, 11, dead_end_paths)
    conversation.generate()
    print(conversation.conversation_data)
