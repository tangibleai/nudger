import random
import string
import uuid

from django.db import models, IntegrityError
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from django.utils.text import slugify

from convohub.constants import (
    DEFAULT_BOT_WIDGET_IMAGE_PATH,
    LANGUAGE_CHOICES,
)


class ConversationTracker(models.Model):
    student_id = models.CharField(
        null=False,
        max_length=100,
    )
    state_name = models.CharField(
        max_length=100,
        blank=True,
        null=True,
    )
    updated_on = models.DateTimeField(auto_now=True)


class Convo(models.Model):
    """A design or plan for a conversational experience"""

    project = models.ForeignKey(
        "Project",  # Use string around this because Project is defined below
        on_delete=models.CASCADE,
        default=None,
        null=True,
        blank=True,
        related_name="project",
    )
    file = models.FileField(
        upload_to="documents/",
        null=True,
        blank=False,
        default=None,
    )
    name = models.CharField(
        max_length=100,
        blank=False,
        null=False,
        default="default",
    )
    description = models.CharField(
        max_length=100,
        blank=False,
        null=False,
        default="default",
    )
    version = models.FloatField(blank=False, null=False, default=3)
    activated_on = models.DateTimeField(
        auto_now_add=True,
        editable=False,
    )
    is_public = models.BooleanField(
        blank=False,
        null=False,
        default=False,
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=[
                    "project",
                    "name",
                ],
                name="composite_key",
            )
        ]

    def __str__(self):
        return (
            f"ID: {self.id}; PROJECT: {self.project}; DESCRIPTION: {self.description}"
        )


class State(models.Model):
    # Foreign key to itself
    convo = models.ForeignKey(
        Convo,
        on_delete=models.CASCADE,
    )
    next_states = models.ManyToManyField(
        "self",
        through="Trigger",
        through_fields=(
            "from_state",
            "to_state",
        ),
    )
    state_name = models.CharField(
        null=False,
        max_length=255,
        help_text="The title the system uses to recognize the state",
    )
    level = models.IntegerField(
        null=True,
        help_text="An optional field that tells the depth of the action",
    )
    nlp_algo = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        help_text="NLP algorithm used to compare trigger text with user text",
    )

    # HL: should be in the Trigger table because they are defined by what the user has said?
    update_args = models.JSONField(null=True)
    update_kwargs = models.JSONField(null=True)
    update_context = models.JSONField(
        null=True,
        help_text="May hold information such as gender, location, or history of previous exchanges",
    )

    # HL: What creates the association between what the bot says and a state name/id ?

    def __str__(self):
        return self.state_name


class Action(models.Model):
    state = models.ForeignKey(
        State,
        null=False,
        blank=False,
        on_delete=models.CASCADE,
    )
    name = models.CharField(
        blank=False,
        null=False,
        max_length=100,
    )
    kwargs = models.JSONField(blank=True, null=True)


class Message(models.Model):
    """A table for messages the bot can send.
    Each row contains one message written in one language.
    """

    sequence_number = models.IntegerField(
        blank=False,
        null=False,
    )
    state = models.ForeignKey(
        State,
        null=False,
        blank=False,
        on_delete=models.CASCADE,
    )
    bot_text = models.TextField(
        null=False,
        help_text="The message of the state written in English",
    )
    lang = models.CharField(
        choices=LANGUAGE_CHOICES,
        default="en",
        max_length=100,
    )


class Trigger(models.Model):
    """A table of supported user inputs that connect two states.
    Users can enter the intent_text by button click or text input.
    Each row is one trigger option written in one language.
    """

    from_state = models.ForeignKey(
        State,
        related_name="rel_from_state",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    to_state = models.ForeignKey(
        State,
        related_name="rel_to_state",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    intent_text = models.CharField(
        null=False,
        max_length=1024,
        help_text="The user utterance that triggers a transition to a new bot state.",
    )
    update_args = models.JSONField(null=True)
    update_kwargs = models.JSONField(null=True)
    # FIXME: We need an nlp = CharField(null=True) to define the NLU algorithm
    #        used to recognize user intents (Trigger messages)
    lang = models.CharField(
        choices=LANGUAGE_CHOICES,
        default="en",
        max_length=100,
    )
    is_button = models.BooleanField(default=False)
    button_text = models.CharField(
        blank=True,
        null=True,
        max_length=1024,
        help_text="Text to be displayed as a button in case trigger is intended to be a button.",
    )

    def trigger_to_dict(self):
        return {
            "from_state": self.from_state.state_name,
            "to_state": self.to_state.state_name,
            "intent_text": self.intent_text,
            "update_args": self.update_args,
            "update_kwargs": self.update_kwargs,
            "lang": self.lang,
            "is_button": self.is_button,
        }

    @staticmethod
    def stringify(trig_as_dict: dict) -> dict:
        for key, value in trig_as_dict.copy().items():
            trig_as_dict[key] = str(value)
        return trig_as_dict


class TriggerCondition(models.Model):
    expression = models.CharField(
        blank=False,
        null=False,
        max_length=1024,
    )
    value = models.JSONField(blank=False, null=False)
    trigger = models.ForeignKey(
        Trigger,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
    )


class UserSession(models.Model):
    """Track sessions tied to User object and delete previous sessions for the same User"""

    id = models.CharField(max_length=10, primary_key=True, default=None)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    session = models.ForeignKey(Session, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        if not self.id:
            while True:
                try:
                    self.id = self.generate_random_key()
                    break
                except IntegrityError:
                    pass
        super().save(*args, **kwargs)

    @staticmethod
    def generate_random_key():
        """Generate a random alphanumeric key of length 10."""
        characters = string.ascii_letters + string.digits
        return "".join(random.choice(characters) for _ in range(10))


class Project(models.Model):
    """Model to act as an interface between User and Convo models.
    Project instances are an operational, running chatbot instance, including
    settings and associated designs (Convos).

    Convo instances describe specific versions of a chatbot's content and design.

    Multiple Convo instances can be associated with a Project.
    Only one Convo is allowed to be activated for a Project.
    """

    LICENSE_CHOICES = (
        [  # NOTE: These are examples.  There are other licenses we might include.
            ("apache2", "Apache License, Version 2.0"),
            ("gnu2", "General Public License Version 2"),
            ("gnu3", "General Public License Version 3"),
            ("mit", "MIT"),
        ]
    )
    public_name = models.CharField(
        blank=True,
        null=True,
        max_length=100,
        help_text="A name to describe the chatbot project",
    )
    slug = models.SlugField(
        blank=True, null=True, default="", max_length=100, unique=True
    )
    public_description = models.TextField(
        blank=True,
        null=True,
        help_text="A concise summary of the chatbot project's purpose",
    )
    page_content = models.TextField(
        blank=True,
        null=True,
        help_text="Markdown-formatted content for display on the public page with the chatbot widget",
    )
    activated_convo = models.OneToOneField(
        Convo,
        on_delete=models.CASCADE,
        related_name="activated_convo",
        blank=True,
        null=True,
    )
    project_license = models.CharField(
        choices=LICENSE_CHOICES,
        default="gnu3",
        max_length=100,
        help_text="A license that governs how others could use the bot for their own chatbot projects",
    )
    bot_widget_icon = models.ImageField(  # FIXME: Remove from Profile model
        upload_to="bot_widget_pics",
        blank=True,
        null=True,
        default=DEFAULT_BOT_WIDGET_IMAGE_PATH,
        help_text="A small image that appears on the chatbot widget",
    )
    share_settings = models.JSONField(
        default=dict,
        blank=True,
        null=True,
        help_text="Describes how others can view, use, and share the chatbot conversations",
    )
    created_on = models.DateTimeField(auto_now=True)
    # FIXME: Convo model is connected to a Project as a ForeignKey instead of User

    def save(self, *args, **kwargs):
        value = self.public_name
        slug = slugify(value, allow_unicode=True)
        while Project.objects.filter(slug=slug):
            unique_id = str(uuid.uuid4())[:8]
            slug = f"{slug}-{unique_id}"
        self.slug = slug
        super().save(*args, **kwargs)


class ProjectCollaborator(models.Model):
    """A model that records access permissions for users to Project instances"""

    PROJECT_ROLE_CHOICES = [("owner", "owner"), ("editor", "editor")]
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, help_text="A registered Qary user"
    )
    project = models.ForeignKey(
        Project,
        on_delete=models.CASCADE,
        null=True,
        help_text="A chatbot project in Qary",
    )
    role = models.CharField(
        choices=PROJECT_ROLE_CHOICES,
        default="editor",
        max_length=100,
        help_text="The level of control a user has over the project",
    )


# IDEA: Might be useful to tie UserImage to Project (ProjectImage) so all users associated with a project could view and work with images associated with a project
# class ProjectImage(models.Model):
#     project_id = models.ForeignKey(Project, on_delete=models.CASCADE, null=True)
#     image = models.ImageField(
#         upload_to= os.path.join("uploaded_images", project_id, filename), # FIXME: Pseudocode, not actual code
#         blank=False,
#         null=False
#     )
