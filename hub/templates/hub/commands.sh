git clone git@gitlab.com:tangibleai/convohub.git
cd convohub
python -m virtualenv .venv
source .venv/bin/activate
pip install -e .
python manage.py migrate
python manage.py createsuperuser
python manage.py runserver
(http://127.0.0.1:8000/admin)
./scripts/start-celery.sh
