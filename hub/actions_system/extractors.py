import re
from urlextract import URLExtract

from mathtext.constants import TOKENS2INT_ERROR_INT
from mathtext.v2_text_processing import format_int_or_float_answer

from convohub.constants import LANG_MAPPINGS
from hub.spacy_language_model import nlp


def extract_proper_nouns(context, key="user_text", pos="PROPN", ent_type=None):
    """Extract the names of persons, places, and things as a list of strs

    >>> text = "My name is Cetin, HIS NAME IS Ruslan Borislov."
    >>> extract_proper_nouns(context=dict(user_text=text))
    {'proper_nouns': ['Cetin', 'Ruslan Borislov']}
    """
    text = context.get(key)
    if not text:
        return None
    doc = nlp(text)
    names = []
    i = 0
    while i < len(doc):
        # if not ((tok.pos_ != pos) or (tok.ent_type_ != ent_type)):
        tok = doc[i]
        if (pos is None or tok.pos_ == pos) and (
            ent_type is None or tok.ent_type_ != ent_type
        ):
            person = [tok.text]
            i += 1
            while i < len(doc):
                t = doc[i]
                i += 1
                if not (
                    (pos is None or t.pos_ == pos)
                    and (ent_type is None or t.ent_type_ != ent_type)
                ):
                    break
                person.append(t.text)
            names.append(" ".join(person))
        else:
            i += 1
    context_diff = dict(proper_nouns=names)
    return context_diff


def extract_lang(context, key="user_text"):
    """Return the 2-char ISO name of a spoken language (e.g. English) extracted from a str

    >>> extract_lang(context=dict(user_text='I speak english'))
    {'lang': 'en'}
    >>> extract_lang(context=dict(user_text='I misspell Ukrainean'))

    >>> extract_lang(context=dict(user_text='I misspell Ukrainian'))
    {'lang': 'uk'}
    """
    text = context.get(key)
    languages = list(LANG_MAPPINGS)
    if text:
        pattern = "\\b(" + "|".join(languages) + ")\\b"
        matches = re.findall(pattern, text.lower())
        if matches:
            return dict(lang=LANG_MAPPINGS.get(matches[0]))


def extract_user_text(context, key_to_extract="user_text"):
    user_text = context.get(key_to_extract, "")
    key_to_update = context.get("key_to_update", "")
    try:
        user_text = int(user_text)
    except ValueError:
        pass
    return {key_to_update: user_text}


def extract_number_from_user_text(context):
    """Extracts a single int or float from a user's message
    Will try to combine multiple numbers into one
    but if multiple distinct numbers, will return 32202 (mathtext error token)

    >>> extract_number_from_user_text(context={'user_text': 'I am 22'})
    22
    >>> extract_number_from_user_text(context={'user_text': 'I am twenty-two'})
    22
    >>> extract_number_from_user_text(context={'user_text': 'I am 22.5'})
    22.5
    >>> extract_number_from_user_text(context={'user_text': 'I am either 22 or 23'})
    32202
    >>> extract_number_from_user_text(context={'user_text': 'I am 2 2'})
    4
    """
    user_text = context.get("user_text", "")
    result = format_int_or_float_answer(user_text)
    if result != TOKENS2INT_ERROR_INT and result:
        return result
    return TOKENS2INT_ERROR_INT


def extract_urls(context, key_to_extract="user_text"):
    user_text = context.get(key_to_extract, "")
    key_to_update = context.get("key_to_update", "")
    extractor = URLExtract()
    urls = extractor.find_urls(user_text)
    return {key_to_update: urls}
