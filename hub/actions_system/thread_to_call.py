import builtins
import sys
import json

import resource

# Set maximum allowed virtual memory to 100 MB
resource.setrlimit(resource.RLIMIT_AS, (100 * 1024 * 1024, resource.RLIM_INFINITY))


def execute_code(user_code):
    """Action to execute user code with time limit prevention"""

    restricted_globals = {"__builtins__": get_safe_builtins()}
    init_data = restricted_globals.copy()
    try:
        exec(user_code, restricted_globals)
        res_data = restricted_globals
        execution_context = {k: v for k, v in res_data.items() if k not in init_data}
    except MemoryError:
        execution_context = {"error": "You code tries to consume too much memory."}

    return execution_context


def get_safe_builtins():
    unsafe_builtins = [
        "open",
        "exec",
        "eval",
        "__import__",
        "compile",
        "input",
    ]

    safe_builtins = {
        name: getattr(builtins, name)
        for name in dir(builtins)
        if name not in unsafe_builtins
    }

    return safe_builtins


if __name__ == "__main__":
    user_code = sys.argv[1]
    res = execute_code(user_code)
    res = json.dumps(res)
    print(res)
