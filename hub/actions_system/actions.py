""" Functions that the conversation designer can call within a v3 Convograph

TODO: Secure server against arbitrary code execution
  - cryptographic signing of python functions (checksum + encryption)
    - decorator to add signatures as function attributes
    - extract_proper_nouns.__auth__ == ENV.get("__auth__", "")
  - gRPC = remote procedure calls
  - custom REST API endpoints for all actions
  - cloud/lambda functions on (Digital Ocean/cloudflare/AWS) (can they handle large RAM ML models?)
  - ephemeral Docker containers managed with Kubernetes or Terraform
  - javascript or WASM (web assembly) in the user's browser - only works for faceofqary chat widget, not SMS
"""
import datetime
import time
import logging
import re
import requests
import json
import subprocess

from jinja2 import Template
from dateparser.search import search_dates

from convohub.constants import USER_TEXT_KEYS
from hub.tasks import schedule_sending_chat_context
from hub.models import Convo, ProjectCollaborator
from users.models import UserImage

from django.conf import settings


log = logging.getLogger(__name__)


def find_user_text(context, keys=USER_TEXT_KEYS):
    """Try to find the key in the context where the user_text is stored"""
    for k in keys:
        if k in context:
            return context[k]
    raise ValueError(
        f"Unable to find a user_text (user_utterance) in context={context}"
    )


def extract_lang(context, languages=list(dict(settings.LANGUAGE_CHOICES))):
    """Return the 2-char ISO name of a spoken language (e.g. English) extracted from a str
    >>> extract_lang(context=dict(user_text='I speak english'))
    'en'
    >>> extract_lang(context=dict(user_text='I like Ukrainian'))
    'ua'
    """
    context = context.get("context", context)
    user_text = find_user_text(context=context)
    languages = context.get("languages", languages)
    if user_text is None:
        return None
    matches = re.findall("\\b(" + "|".join(languages) + ")\\b", user_text.lower())
    if matches:
        return matches[0]
    return None


def send_message(context):
    """Prepares and sends a message via consumers

    Extracts value tied to `bot_text` key from context, fills all the variables in it
    if present using Jinja2, schedules celery task to send a message and waits for it
    to finish. Returns empty dictionary since no context modifications are applied
    """
    resolve_parametrized_string(context)
    messages = context.get("messages", [])
    state_name = context["state_name"]
    message_data = {
        "bot_text": context["bot_text"],
        "state_name": state_name,
    }
    messages.append(message_data)
    context["messages"] = messages
    context_diff = send_context(context)
    context_diff["bot_text"] = ""
    return context_diff


def resolve_parametrized_string(context):
    """Extract string being a value of `bot_text` key in context and fills it with variables using Jinja2"""
    template_string = context.get("bot_text", "")
    template = Template(template_string)

    try:
        rendered_string = template.render(context)
    except TypeError:
        rendered_string = "Something went wrong! Expected another answer"
    context["bot_text"] = rendered_string
    return context


def send_triggers(context):
    """Schedule celery task to send context containing triggers via consumers and wait for response"""
    context_diff = send_context(context)
    context_diff["triggers"] = []
    return context_diff


def send_image(context):
    """Extract image url, bind it to `img` tag, send it to consumer?...

    Extract image url, bind it to `img` tag, schedule celery task to send
    a context via consumers, waits for it to finish and removes temporary
    modifications to context needed to run this action.
    """

    image_url = get_url_of_image_to_send(context)
    if image_url is None or not url_is_correct(image_url):
        media_url = settings.MEDIA_URL
        media_url = media_url[:-1] if media_url.endswith("/") else media_url
        image_url = f"{media_url}/default.jpg"

    context["image_as_html"] = (
        f'<a target="_blank" href="{image_url}">'
        f'<img src="{image_url}" style="max-width: 100%; height: auto;"'
        f'     alt="User Image" style="object-fit: cover;"></a>'
    )
    context_diff = send_context(context)
    context_diff["image_as_html"] = ""
    return context_diff


def url_is_correct(url):
    try:
        response = requests.get(url, timeout=5)
        response.raise_for_status()  # Raises an HTTPError if the response was an HTTP error
        return True
    except Exception:
        return False


def get_url_of_image_to_send(context):
    """Extracts user images, compare each's url ending with `image_name` variable from context and returns the first one matching"""
    convo_id = context["convo_id"]
    convo = Convo.objects.get(id=convo_id)
    project_collaborator = ProjectCollaborator.objects.filter(
        project=convo.project
    ).first()
    user = project_collaborator.user
    image_name = context.get("image_name", "")
    user_images = UserImage.objects.filter(user=user)
    image_url = None
    for img in user_images:
        if img.image.name.endswith(image_name):
            image_url = img.image.url
            break
    return image_url


def send_context(context):
    """Schedules celery task to send a context via consumers and waits for it to finish"""
    room_group_name = context["room_group_name"]
    task_result = schedule_sending_chat_context.delay(
        room_group_name=room_group_name, context=context
    )
    wait_for_task_to_finish(task_result)

    context_diff = {}
    return context_diff


def wait_for_task_to_finish(task_status):
    start_time = datetime.datetime.now()
    current_time = start_time
    while not task_status.ready() and (current_time - start_time).seconds < 10:
        time.sleep(0.01)
        current_time = datetime.datetime.now()
    return


def increment_number_of_turns(context):
    """Extracts the number of turns (number of user answers in current conversation) and increases it by one"""
    context_diff = {"num_turns": context.get("num_turns", 0) + 1}
    return context_diff


def execute_code(context):
    user_code = context.get("code_to_execute", "")
    user_code = f"""
context = {context}
{user_code}
"""
    try:
        # Execute the code in a subprocess
        result = subprocess.run(
            [
                "python",
                f"{settings.BASE_DIR}/hub/actions_system/thread_to_call.py",
                user_code,
            ],
            capture_output=True,
            text=True,
            timeout=3,
        )
        if result.stderr:
            context_diff = {"error": "Syntax error detected. Please, check you code."}
        else:
            output = result.stdout.strip().split("\n")
            output = output[-1] if output else None
            context_diff = json.loads(output)
            if "context" in context_diff:
                del context_diff["context"]
    except subprocess.TimeoutExpired:
        context_diff = {"error": "Code takes too much time to execute"}
    return context_diff


def extract_dates(context):
    text_with_dates = context.get("text_with_dates", "")
    res = search_dates(text_with_dates)
    context_diff = {"extracted_dates": res}
    return context_diff
