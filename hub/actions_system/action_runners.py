from logging import getLogger

from convohub.constants import ALLOWED_ACTIONS
from . import actions
from . import extractors

log = getLogger(__name__)


def update_context(context, key, value):
    """Add a key value pair to a context dict"""
    if context is None:
        context = dict()
    context = dict(context)
    key = str(key)
    context[key] = value
    return context


def is_allowed(name: str):
    """Check the action name (str) to see if it is among the list of actions allowed"""
    return (
        name in ALLOWED_ACTIONS
        # and name in globals()
        and not name.startswith("_")
        and not name.endswith("_")
    )


def run(action_name, context):
    """Run the requested action (function) and return the updated context

    # FIXME: The following test results in
    # {'user_text': 'My name is Cetin, HIS NAME IS rUSLAN  Borislov.', 'proper_nouns': ['Cetin', 'Borislov']}
    text = "My name is Cetin, HIS NAME IS rUSLAN  Borislov."
    run(action='extract_proper_nouns', text=text)
    {'extract_proper_nouns': ['Cetin', 'rUSLAN Borisov']}
    """
    callable_action = get_action_to_run(action_name)
    context_diff = execute_callable_action(callable_action, context)
    return context_diff


def execute_callable_action(callable_action, context):
    context_diff = {}
    try:
        context_diff = callable_action(context)
    except Exception as e:
        action_name = callable_action.__name__
        log.error(f"Unable to run the action {action_name}({context})...\n{e}")
    return context_diff


def get_action_to_run(action_name):
    callable_action = None
    modules_list = [actions, extractors]
    for module in modules_list:
        callable_action = get_action_from_module_or_None(module, action_name)
        if callable_action is not None:
            break
    return callable_action


def get_action_from_module_or_None(module, action_name):
    callable_action = None
    try:
        callable_action = getattr(module, action_name)
    except AttributeError:
        pass
    return callable_action
