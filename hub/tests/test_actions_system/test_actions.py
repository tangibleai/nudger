import datetime
import time
import threading

from django.contrib.auth.models import User
from django.test import TestCase

from hub.actions_system.actions import (
    get_url_of_image_to_send,
    wait_for_task_to_finish,
)
from hub.models import Convo, ProjectCollaborator
from scripts.create_default_project import get_or_create_default_project
from users.models import UserImage


class TestGetUrlOfImageToSendHelper(TestCase):
    user = None
    convo = None
    user_images = []

    def setUp(self):
        self.user = User.objects.create(
            username="test_username_for_setUp",
            password="Abcdefgh!",
        )

        # Create a project
        self.default_project = get_or_create_default_project()

        # Create a connection between default user and project
        self.project_collaborator, created = ProjectCollaborator.objects.get_or_create(
            user=self.user, project=self.default_project
        )

        self.convo = Convo.objects.create(
            project=self.project_collaborator.project,
            name="test_convo_name_for_test_username_for_setUp",
            description="test_description_test_username_for_setUp",
        )
        self.create_UserImage_instances()
        return

    def create_UserImage_instances(self):
        images_names = [
            f"media/{self.user.username}/default.jpg",
            f"media/{self.user.username}/default1.jpg",
            f"media/{self.user.username}/default2.jpg",
        ]
        for img_name in images_names:
            user_image = UserImage.objects.create(user=self.user, image=img_name)
            self.user_images.append(user_image)
        return

    def test(self):
        expected_url = f"media/{self.user.username}/default1.jpg"
        actual_url = get_url_of_image_to_send(
            context={"convo_id": self.convo.id, "image_name": "default1.jpg"}
        )
        self.assertTrue(actual_url.endswith(expected_url))
        return

    def tearDown(self):
        self.convo.delete()
        for img in self.user_images:
            img.delete()
        self.user.delete()
        return


class TestGetUrlOfImageToSendHelper(TestCase):
    user = None
    convo = None
    user_images = []

    def setUp(self):
        self.user = User.objects.create(
            username="test_username_for_setUp",
            password="Abcdefgh!",
        )
        # Create a project
        self.default_project = get_or_create_default_project()

        # Create a connection between default user and project
        self.project_collaborator, created = ProjectCollaborator.objects.get_or_create(
            user=self.user, project=self.default_project
        )
        self.convo = Convo.objects.create(
            project=self.project_collaborator.project,
            name="test_convo_name_for_test_username_for_setUp",
            description="test_description_test_username_for_setUp",
        )
        self.create_UserImage_instances()
        return

    def test(self):
        expected_url = f"media/{self.user.username}/default1.jpg"
        actual_url = get_url_of_image_to_send(
            context={"convo_id": self.convo.id, "image_name": "default1.jpg"}
        )
        self.assertTrue(actual_url.endswith(expected_url))
        return

    def create_UserImage_instances(self):
        images_names = [
            f"media/{self.user.username}/default.jpg",
            f"media/{self.user.username}/default1.jpg",
            f"media/{self.user.username}/default2.jpg",
        ]
        for img_name in images_names:
            user_image = UserImage.objects.create(user=self.user, image=img_name)
            self.user_images.append(user_image)
        return

    def tearDown(self):
        self.convo.delete()
        for img in self.user_images:
            img.delete()
        self.user.delete()
        return


class TestWaitForTaskToFinishhelper(TestCase):
    class MockTaskStatus:
        is_ready = False

        def ready(self):
            return self.is_ready == True

    def setUp(self):
        self.mock_task_status = self.MockTaskStatus()
        return

    def test_usual_scenario(self):
        execute_in_a_thread = threading.Thread(
            target=self.set_task_as_ready_after_3_seconds
        )
        start_time = datetime.datetime.now()
        execute_in_a_thread.start()
        wait_for_task_to_finish(self.mock_task_status)
        elapsed_time_in_seconds = (datetime.datetime.now() - start_time).seconds
        self.assertTrue(elapsed_time_in_seconds <= 10)

    def set_task_as_ready_after_3_seconds(self):
        time.sleep(3)
        self.mock_task_status.is_ready = True
        return

    def test_bad_scenario(self):
        start_time = datetime.datetime.now()
        wait_for_task_to_finish(self.mock_task_status)
        elapsed_time_in_seconds = (datetime.datetime.now() - start_time).seconds
        self.assertTrue(elapsed_time_in_seconds <= 11)
