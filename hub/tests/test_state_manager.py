"""Tests user creation, convo creation, state manager"""

from django.contrib.auth import get_user_model
from django.test import TestCase

from hub.conversation_manager.v3.conversation_manager import ConversationRunner
from hub.models import Convo, Project, ProjectCollaborator
from scripts.create_default_convo_on_db import create_convo

from convohub.constants import DEFAULT_CONVOGRAPH_YAML_PATH, DEFAULT_PROJECT


class HubConvoTest(TestCase):
    """Tests convo creation and state manager"""

    user_dict = {
        "username": "normal_user",
        "password": "NORMAL_USER_PASSWORD",
        "email": "first.last@mail.qary.ai",
    }

    default_yaml_path = DEFAULT_CONVOGRAPH_YAML_PATH

    def setUp(self):
        self.user = get_user_model().objects.create_user(**self.user_dict)
        self.project = Project.objects.create(**DEFAULT_PROJECT)
        self.project_collaborator = ProjectCollaborator.objects.create(
            user=self.user, project=self.project
        )
        self.convo = create_convo(self.project, self.default_yaml_path)

    def tearDown(self):
        self.user.delete()
        self.project.delete()
        self.convo.delete()

    def test_convo_creation(self):
        """Test if convo is a Convo object"""
        self.assertTrue(isinstance(self.convo, Convo))

    def test_retrive_users_convos(self):
        """Test if we can retrieve convo for the user"""
        convo_qs = Convo.objects.filter(project=self.project)
        self.assertEqual(convo_qs.count(), 1)

    def test_state_data_package_1(self):
        """Tests data package 1 with the correct response"""
        context = {
            "state_name": "greeting",
            "user_text": "__next__",
            "convo_id": self.convo.id,
            "room_group_name": "0990",
        }
        actual_context = ConversationRunner(context).fill_context_with_next_state()
        expected_context = {
            "state_name": "ask-age",
            "user_text": "__next__",
            "convo_id": 1,
            "room_group_name": "0990",
            "conversation_version": 3.1,
            "bot_text": "",
            "messages": [{"bot_text": "How old are you?", "state_name": "greeting"}],
            "triggers": [{'"intent_text"': '"__default__"', '"target"': '"age"'}],
        }
        self.assertEquals(actual_context, expected_context)

    def test_state_data_package_2(self):
        """Tests data package 2 with a wrong response"""
        context = {
            "state_name": "ask-name",
            "user_text": "vlad",
            "convo_id": self.convo.id,
            "room_group_name": "0990",
        }
        actual_context = ConversationRunner(context).fill_context_with_next_state()
        expected_context = {
            "state_name": "ask-age",
            "user_text": "__next__",
            "convo_id": 1,
            "room_group_name": "0990",
            "conversation_version": 3.1,
            "key_to_update": "name",
            "name": "vlad",
            "bot_text": "",
            "messages": [
                {"bot_text": "Hello vlad!", "state_name": "ask-name"},
                {"bot_text": "How old are you?", "state_name": "ask-name"},
            ],
            "triggers": [{'"intent_text"': '"__default__"', '"target"': '"age"'}],
        }
        self.assertEquals(actual_context, expected_context)
