""" Find and run doctests """
import doctest
import unittest

from users import utils
from hub.actions_system import extractors
from hub import consumers
from hub.conversation_manager.v2 import (
    conversation_manager as v2_conversation_manager,
)
from hub.conversation_manager.v3 import (
    conversation_manager as v3_conversation_manager,
)

# FIXME: does not discover tests in users or convohub, only hub
#        entire package convohub/convohub should be importable and installed with pyproject.toml
testsuite = unittest.TestLoader().discover("..")


def load_tests(*args, **kwargs):
    test_all_doctests = unittest.TestSuite()
    for module in (
        extractors,
        consumers,
        v2_conversation_manager,
        v3_conversation_manager,
        utils,
    ):
        test_all_doctests.addTest(
            doctest.DocTestSuite(
                module, optionflags=(doctest.ELLIPSIS | doctest.NORMALIZE_WHITESPACE)
            )
        )
    return test_all_doctests
