import ast
import json
import os

from django.contrib.auth import get_user_model
from django.test import TestCase

from hub.models import Convo, Project, ProjectCollaborator

from convohub.constants import DEFAULT_CONVOGRAPH_YAML_PATH, DEFAULT_PROJECT


class UploadTest(TestCase):
    """Tests convo creation and state manager"""

    user_data = {
        "username": "test_upload_user",
        "password": "TEST_UPLOAD_USER_PASSWORD",
        "email": "test.upload@mail.qary.ai",
    }
    filenames = ["dungeon_1.yml", "dungeon_2.yml", "dungeon_3.yml"]

    default_yaml_path = DEFAULT_CONVOGRAPH_YAML_PATH
    API_ENDPOINT = "http://localhost:8000/api/convo/post_file"
    convos = []

    def setUp(self):
        self.user = get_user_model().objects.create_user(**self.user_data)
        self.project = Project.objects.create(**DEFAULT_PROJECT)
        self.project_collaborator = ProjectCollaborator.objects.create(
            user=self.user, project=self.project
        )

    def tearDown(self):
        self.project_collaborator.delete()
        Convo.objects.filter(project=self.project).delete()
        Project.objects.get(id=self.project.id).delete()
        self.project.delete()
        self.user.delete()

    def test_upload(self):
        for filename in self.filenames:
            file_content = self.read_file_content(filename)
            data_to_send = {
                "file": file_content,
                "username": self.user.username,
                "is_private": True,
                "filename": filename,
            }
            self.client.post(
                "/api/convo/post_file",
                json.dumps(data_to_send),
                content_type="application/json",
            )

        self.convos = Convo.objects.filter(project=self.project)
        self.assertEquals(len(self.convos), len(self.filenames))

    def read_file_content(self, filename):
        module_dir = os.path.dirname(__file__)

        file_path = os.path.join(module_dir, filename)

        with open(file_path, "r") as file:
            data = file.read()
            return data

    def test_dungeon_conversation(self):
        for filename in self.filenames:
            convo_id = self.setUp_for_test_dungeon_conversation(filename)
            convo = Convo.objects.get(id=convo_id)

            solution = ast.literal_eval(convo.description)
            response = self.client.get(
                "/api/chat",
                {
                    "context": json.dumps(
                        {
                            "username": self.user_data["username"],
                            "state_name": "start",
                            "user_text": "",
                        }
                    )
                },
            )
            for direction in solution:
                ordered_girections = self.get_ordered_directions(direction)
                for d in ordered_girections:
                    context = response.json()
                    context["context"]["user_text"] = d
                    response = self.client.get(
                        "/api/chat",
                        {"context": json.dumps(context["context"])},
                    )
                    response_json = response.json()
                    bot_text = response_json["context"]["bot_text"]
                    if d != direction:
                        self.assertTrue(bot_text.startswith("Oops!"))
                    else:
                        self.assertTrue(
                            bot_text.startswith("You find a secret door")
                            or bot_text.startswith(
                                "Congratulations! You escaped from dungeon."
                            )
                        )

    def setUp_for_test_dungeon_conversation(self, filename):
        file_content = self.read_file_content(filename)
        data_to_send = {
            "file": file_content,
            "username": self.user.username,
            "is_private": True,
            "filename": filename,
        }
        response = self.client.post(
            "/api/convo/post_file",
            json.dumps(data_to_send),
            content_type="application/json",
        )
        convo_id = response.json()
        return convo_id

    def get_ordered_directions(self, right_direction):
        wrong_directions = [d for d in "NSWE" if d != right_direction]
        wrong_directions.append(right_direction)
        return wrong_directions
