import re
from pathlib import Path

import convomeld
from convomeld.parsers import SimpleScriptParser
from django.conf import settings
from django.contrib.auth.models import User
from django.core.files import File
from django.test import TestCase

from convohub.constants import DEFAULT_CONVOGRAPH_YAML_PATH
from hub.conversation_manager.v2.conversation_manager import ConversationRunner
from hub.models import Convo, ProjectCollaborator
from hub.models_convomeld import (
    convograph_to_convomodel,
    convomodel_to_convograph,
    merge_convos,
)
from scripts.create_default_convo_on_db import create_convo
from scripts.create_default_project import get_or_create_default_project

SCRIPTS_DIR = settings.DATA_DIR / "scripts" / "test"
GRAPHS_DIR = settings.DATA_DIR / "v2" / "test"


class TestConvographToConvomodel(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username="testuser", password="testpass")

        # Create a project
        self.default_project = get_or_create_default_project()

        # Create a connection between default user and project
        col, created = ProjectCollaborator.objects.get_or_create(
            user=self.user, project=self.default_project
        )

        # Create a convo associated with project
        self.default_user_convo = create_convo(
            project=self.default_project,
            file_path=Path(DEFAULT_CONVOGRAPH_YAML_PATH),
            convo_name="default_user_convo_name",
            convo_description="default_user_convo_description",
        )

        self.project_collaborator = ProjectCollaborator.objects.filter(
            user=self.user
        ).first()
        self.project = self.project_collaborator.project

        self.delete_on_teardown = []

        self.src_convoyaml_path = GRAPHS_DIR / "count_by_one_2_1+2_2+2_3.yml"
        self.src_convograph = convomeld.file_to_graph(
            self.src_convoyaml_path.as_posix()
        )

    def tearDown(self) -> None:
        for obj in self.delete_on_teardown:
            obj.delete()

    def test_type_convograph(self):
        convomodel = convograph_to_convomodel(self.src_convograph, self.project)
        res_convograph = convomodel_to_convograph(convomodel)
        self.delete_on_teardown.append(convomodel.file)
        self.assertTrue(convomeld.compare_graphs(self.src_convograph, res_convograph))

    def test_type_file(self):
        src_convoyaml_file = File(
            self.src_convoyaml_path.open("rb"), name=self.src_convoyaml_path.name
        )
        convomodel = convograph_to_convomodel(src_convoyaml_file, self.project)
        res_convograph = convomodel_to_convograph(convomodel)
        self.delete_on_teardown.append(convomodel.file)
        self.assertTrue(convomeld.compare_graphs(self.src_convograph, res_convograph))

    def test_type_pathstr(self):
        src_convoyaml_path_str = self.src_convoyaml_path.as_posix()
        convomodel = convograph_to_convomodel(src_convoyaml_path_str, self.project)
        res_convograph = convomodel_to_convograph(convomodel)
        self.delete_on_teardown.append(convomodel.file)
        self.assertTrue(convomeld.compare_graphs(self.src_convograph, res_convograph))

    def test_type_path(self):
        convomodel = convograph_to_convomodel(self.src_convoyaml_path, self.project)
        res_convograph = convomodel_to_convograph(convomodel)
        self.delete_on_teardown.append(convomodel.file)
        self.assertTrue(convomeld.compare_graphs(self.src_convograph, res_convograph))

    def test_type_stream(self):
        src_convograph_stream = self.src_convoyaml_path.open()
        convomodel = convograph_to_convomodel(src_convograph_stream, self.project)
        res_convograph = convomodel_to_convograph(convomodel)
        self.delete_on_teardown.append(convomodel.file)
        self.assertTrue(convomeld.compare_graphs(self.src_convograph, res_convograph))


class TestConvomeldParseMergeCoherence(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username="testuser", password="testpass")
        self.base_author = "teacher"
        self.delete_on_teardown = []

    def tearDown(self):
        for obj in self.delete_on_teardown:
            obj.delete()

    def create_convo_from_merged_scripts(
        self, script_paths, script_as_convo=False
    ) -> Convo:
        script_graphs = [
            convomeld.file_to_graph(script_path, base_author=self.base_author)
            for script_path in script_paths
        ]

        if script_as_convo:
            script_graphs = [
                convograph_to_convomodel(script_graph, self.user)
                for script_graph in script_graphs
            ]

        merged_convo = merge_convos(self.user, "test", "", *script_graphs)

        if script_as_convo:
            self.delete_on_teardown += [
                script_convo.file for script_convo in script_graphs
            ]

        return merged_convo

    def read_script(self, script_path):
        with open(script_path, encoding="utf-8") as f:
            return SimpleScriptParser().parse_lines(f.readlines())

    def check_coherence(self, script_lines, convo):
        state_name = "start"
        user_text = ""
        lang = "en"
        res = ConversationRunner(
            {
                "state_name": state_name,
                "user_text": user_text,
                "lang": lang,
                "convo_id": convo.id,
            }
        ).fill_context_with_next_state()

        bot_manager_lines = [
            re.search(r"<p>(.+)</p>", message["bot_text"]).group(1)
            for message in res["bot_messages"]
        ]
        bot_script_lines = []

        for line in script_lines:
            if line.author == self.base_author:
                bot_script_lines.append(line.text)
                continue

            self.assertEquals(bot_manager_lines, bot_script_lines)

            state_name = res["state_name"]
            user_text = line.text
            res = ConversationRunner(
                {
                    "state_name": state_name,
                    "user_text": user_text,
                    "lang": lang,
                    "convo_id": convo.id,
                }
            ).fill_context_with_next_state()
            bot_manager_lines = [
                re.search(r"<p>(.+)</p>", message["bot_text"]).group(1)
                for message in res["bot_messages"]
            ]
            bot_script_lines = []

        self.assertEquals(bot_manager_lines, bot_script_lines)

    # def test_parsed_graphs_coherence(self):
    #     script_paths = [
    #         SCRIPTS_DIR / "count_by_one_1_1.txt",
    #         SCRIPTS_DIR / "count_by_one_1_2.txt",
    #         SCRIPTS_DIR / "count_by_one_1_3.txt",
    #         SCRIPTS_DIR / "count_by_one_1_1_teacher_stop_early.txt",
    #         SCRIPTS_DIR / "count_by_one_1_1_student_stop_early.txt",
    #         SCRIPTS_DIR / "count_by_one_2_1.txt",
    #         SCRIPTS_DIR / "count_by_one_2_2.txt",
    #         SCRIPTS_DIR / "count_by_one_2_3.txt",
    #         SCRIPTS_DIR / "count_by_one_2_student_stop_early_1.txt",
    #         SCRIPTS_DIR / "count_by_one_2_student_stop_early_2.txt",
    #         SCRIPTS_DIR / "count_by_one_2_student_stop_early_3.txt",
    #     ]

    #     for script_path in script_paths:
    #         script_path = script_path.as_posix()
    #         convo = self.create_convo_from_merged_scripts([script_path])
    #         script_lines = self.read_script(script_path)
    #         self.check_coherence(script_lines, convo)
    #         self.delete_on_teardown.append(convo.file)

    # def test_merged_graphs_coherence(self):
    #     script_combinations_paths = [
    #         (
    #             SCRIPTS_DIR / "count_by_one_1_1.txt",
    #             SCRIPTS_DIR / "count_by_one_1_2.txt",
    #             SCRIPTS_DIR / "count_by_one_1_3.txt",
    #             SCRIPTS_DIR / "count_by_one_1_1_student_stop_early.txt",
    #         ),
    #         (
    #             SCRIPTS_DIR / "count_by_one_2_1.txt",
    #             SCRIPTS_DIR / "count_by_one_2_2.txt",
    #             SCRIPTS_DIR / "count_by_one_2_3.txt",
    #             SCRIPTS_DIR / "count_by_one_2_student_stop_early_1.txt",
    #             SCRIPTS_DIR / "count_by_one_2_student_stop_early_2.txt",
    #             SCRIPTS_DIR / "count_by_one_2_student_stop_early_3.txt",
    #         ),
    #     ]

    #     for script_paths in script_combinations_paths:
    #         script_paths = [script_path.as_posix() for script_path in script_paths]
    #         convo = self.create_convo_from_merged_scripts(script_paths)

    #         for script_path in script_paths:
    #             script_lines = self.read_script(script_path)
    #             self.check_coherence(script_lines, convo)

    #         self.delete_on_teardown.append(convo.file)
