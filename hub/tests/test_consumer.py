import json
from pathlib import Path

from channels.auth import AuthMiddlewareStack
from channels.routing import URLRouter
from channels.testing import WebsocketCommunicator
from django.conf import settings
from django.contrib.auth import get_user_model
from django.test import SimpleTestCase

from convohub.constants import ENV, DEFAULT_CONVOGRAPH_YAML_PATH
from hub.models import ProjectCollaborator
from hub.routing import websocket_urlpatterns
from scripts.create_default_convo_on_db import create_convo
from scripts.create_default_project import get_or_create_default_project


class ConsumerTest(SimpleTestCase):
    """
    Simple test case with databases definition to use database along all tests
    Otherwise use `from channels.db import database_sync_to_async`
    Testing consumer connection and response object
    """

    databases = "__all__"

    username = ENV.get("DJANGO_DEFAULT_USERNAME")
    password = ENV.get("DJANGO_DEFAULT_PASSWORD")

    # This is a simplified version of the application as in the `convohub.asgi.py`
    application = AuthMiddlewareStack(
        URLRouter(
            [
                *websocket_urlpatterns,
            ]
        ),
    )

    def setUp(self):
        self.user = get_user_model().objects.create_user(
            username=self.username,
            password=self.password,
        )

        # Create a project
        self.default_project = get_or_create_default_project()

        # Create a connection between default user and project
        project_collaborator, created = ProjectCollaborator.objects.get_or_create(
            user=self.user, project=self.default_project
        )

        self.convo = create_convo(
            file_path=Path(DEFAULT_CONVOGRAPH_YAML_PATH),
            project=project_collaborator.project,
            convo_name="default_user_convo_name",
            convo_description="default_user_convo_description",
        )

    async def test_websocket_application(self):
        """
        Tests that the WebSocket communicator class works with the
        URLRoute application.
        """

        communicator = WebsocketCommunicator(
            self.application, f"/ws/quiz/{self.username}/1/room0990/"
        )
        connected, subprotocol = await communicator.connect(timeout=1)

        # Testing connection
        self.assertEqual(connected, True)

        text_data = json.dumps(
            {
                "state_name": "q1",
                "bot_text": "<p>Type 11</p>",
                "message_sequence_number": 0,
                "is_message_last": True,
                "triggers": [
                    {
                        "from_state": "q1",
                        "to_state": "q1",
                        "intent_text": "OK",
                        "update_args": "null",
                        "update_kwargs": "null",
                        "lang": "en",
                        "is_button": "true",
                    },
                    {
                        "from_state": "q1",
                        "to_state": "correct-answer-q1",
                        "intent_text": "11",
                        "update_args": "null",
                        "update_kwargs": "null",
                        "lang": "en",
                        "is_button": "false",
                    },
                    {
                        "from_state": "q1",
                        "to_state": "correct-answer-q1",
                        "intent_text": "eleven",
                        "update_args": "null",
                        "update_kwargs": "null",
                        "lang": "en",
                        "is_button": "false",
                    },
                    {
                        "from_state": "q1",
                        "to_state": "wrong-answer-q1",
                        "intent_text": "__default__",
                        "update_args": "null",
                        "update_kwargs": "null",
                        "lang": "en",
                        "is_button": "false",
                    },
                ],
                "lang": "en",
                "convo_id": "1",
                "functions_execution_result": {},
                "user_text": "11",
            }
        )

        await communicator.send_to(text_data=text_data)

        response = json.loads(await communicator.receive_from())

        # Testing if a response is sent and content of the response object
        self.assertTrue(bool(response))
        self.assertTrue("state_name" in response.keys())
        self.assertTrue("lang" in response.keys())
        self.assertTrue("user_text" in response.keys())
        self.assertTrue("room_group_name" in response.keys())
        self.assertTrue("bot_text" in response.keys())
