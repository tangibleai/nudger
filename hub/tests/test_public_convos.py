"""Tests user creation, convo creation, toggling public and private"""

from pathlib import Path
import logging

from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.test import TestCase, Client

from api.views.convo_views import toggle_convos_public_affiliation
from hub.models import Convo
from scripts.create_default_convo_on_db import create_convo

from convohub.constants import DEFAULT_CONVOGRAPH_YAML_PATH

log = logging.getLogger(__name__)


from pathlib import Path
from scripts.create_default_convo_on_db import create_convo
from scripts.create_default_project import get_or_create_default_project
from convohub.constants import DEFAULT_CONVOGRAPH_YAML_PATH, DEFAULT_PROJECT
from hub.models import ProjectCollaborator, Project


def create_project_for_tests(user):
    project = Project.objects.create(**DEFAULT_PROJECT)

    # Create a connection between default user and project
    project_collaborator, created = ProjectCollaborator.objects.get_or_create(
        user=user, project=project
    )
    return project_collaborator.project


class HubConvoTest(TestCase):
    """Tests convo creation and state manager"""

    user1_dict = {
        "username": "normal_user1",
        "password": "NORMAL_USER_PASSWORD1",
        "email": "user.one@mail.qary.ai",
    }

    user2_dict = {
        "username": "normal_user2",
        "password": "NORMAL_USER_PASSWORD2",
        "email": "user.two@mail.qary.ai",
    }

    convo1_name = "convo1"
    convo2_name = "convo2"
    convo1_description = "convo1 description"
    convo2_description = "convo2 description"

    file_path = Path(DEFAULT_CONVOGRAPH_YAML_PATH)

    def setUp(self):
        self.client = Client()
        self.user1 = get_user_model().objects.create_user(**self.user1_dict)
        self.user2 = get_user_model().objects.create_user(**self.user2_dict)

        self.user1_proj = create_project_for_tests(self.user1)
        self.user2_proj = create_project_for_tests(self.user2)

        self.convo1 = create_convo(
            project=self.user1_proj,
            file_path=self.file_path,
            convo_name=self.convo1_name,
            convo_description=self.convo1_description,
        )
        self.convo2 = create_convo(
            project=self.user2_proj,
            file_path=self.file_path,
            convo_name=self.convo2_name,
            convo_description=self.convo2_description,
        )
        log.debug(self.file_path)
        log.debug(self.user1)
        log.debug(self.user2)
        log.debug(self.convo1)

    def tearDown(self):
        self.user1.delete()
        self.user2.delete()
        self.convo1.delete()
        self.convo2.delete()
        self.user1_proj.delete()
        self.user2_proj.delete()

    def test_user_creation(self):
        """Tests if user is a User object"""
        self.assertTrue(isinstance(self.user1, get_user_model()))
        self.assertTrue(isinstance(self.user2, get_user_model()))
        self.assertEqual(User.objects.filter().count(), 2)

    def test_convo_creation(self):
        """Tests if convo is a Convo object"""
        self.assertTrue(isinstance(self.convo1, Convo))
        self.assertTrue(isinstance(self.convo2, Convo))
        self.assertEqual(Convo.objects.filter().count(), 2)

    def test_default_is_private(self):
        """Tests if default is private"""
        self.assertTrue(self.convo1.is_public)
        self.assertTrue(self.convo2.is_public)

    def test_toggle_public_private(self):
        """Tests if toggle public and private is working"""
        toggle_convos_public_affiliation(convo_ids=[self.convo2.pk], is_public=True)
        convo = Convo.objects.get(id=self.convo2.pk)
        self.assertTrue(convo.is_public)
        toggle_convos_public_affiliation(convo_ids=[self.convo2.pk], is_public=False)
        convo = Convo.objects.get(id=self.convo2.pk)
        self.assertFalse(convo.is_public)

    def test_user1_convos(self):
        """Tests user1 convo count, id, name, description"""
        convo_qs = Convo.objects.filter(project=self.user1_proj)
        convo = convo_qs.first()
        self.assertEqual(convo_qs.count(), 1)
        self.assertEqual(convo.id, 1)
        self.assertEqual(convo.name, self.convo1_name)
        self.assertEqual(convo.description, self.convo1_description)

    def test_user2_convos(self):
        """Tests user2 convo count, id, name, description"""
        convo_qs = Convo.objects.filter(project=self.user2_proj)
        convo = convo_qs.first()
        self.assertEqual(convo_qs.count(), 1)
        self.assertEqual(convo.id, 2)
        self.assertEqual(convo.name, self.convo2_name)
        self.assertEqual(convo.description, self.convo2_description)
