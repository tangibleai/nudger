from wikipedia import wikipedia as wiki

from hub.spacy_language_model import nlp


def get_wiki_page_summary_tokens(text):
    """
    This function takes a search string and returns first wikipedia page summary as sentences.

    Args:
    text (str): Search string

    Returns:
    [str]: Return page summary as list of sentences.
    """
    titles = wiki.search(text)
    page = None
    content = None
    for title in titles:
        try:
            page = wiki.page(title)
            break
        except Exception as e:
            pass
    if page:
        content = page.content.split("\n")[0]
    doc = nlp(content) if content else None
    return list(doc.sents) if doc else []


if __name__ == "__main__":
    search_text = "Ford"
    tokens = get_wiki_page_summary_tokens(search_text)
    print(tokens)
