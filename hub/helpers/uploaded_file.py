"""Module has UploadedFileForm class - the only one intended to be instantiated directly among all represented here"""

import io
import requests
import yaml

from django.core.files import File
from django.contrib import messages
from django.utils.safestring import mark_safe

from hub.convograph_validator import helpers
from hub.convograph_validator.convograph_validator_errors import (
    ConvoGraphValidationError,
)
from hub.forms import ConvoForm, ConvoSpreadsheetForm
from hub.models import ProjectCollaborator
from hub.spreadsheet_to_db import upload_spreadsheet_to_db
from hub.spreadsheet_linter import ConvoSpreadsheetValidationError
from hub.yaml_to_db_records.yaml_to_db_records import convoyaml_to_convomodel

from ..utils import get_file_format_version


class Initializer:
    def __init__(self, request):
        self.request = request

        self.file = self.request.FILES["file"]
        filename = self.file.name
        self.file_ext = filename.split(".")[-1]

        self.project_collaborator = ProjectCollaborator.objects.filter(
            user=self.request.user
        ).first()

        self.flash_msg_caller = messages.info
        self.result_msg = "New conversation has been created and activated!"


class YmlFileForm(Initializer):
    def handle_valid_form(self):
        try:
            self.file_to_saved_db_records()
            return
        except ConvoGraphValidationError as exc:
            self.result_msg = str(exc) + " "
        except Exception:
            self.result_msg = (
                'Wrong syntax! File content should contain multiple entries starting from "-" character. Most likely you forgot to put leading "-" before one of states. Use the format in the '
                + mark_safe(
                    "<b><a href='https://gitlab.com/tangibleai/convohub/-/blob/main/data/countByOne_0001.yml'>link</a></b>"
                )
            )
        self.result_msg += ""
        self.flash_msg_caller = messages.error
        return

    def file_to_saved_db_records(self):
        raw_content = self.file.read()
        file_as_yml = yaml.safe_load(raw_content)
        format_version = get_file_format_version(file_as_yml)
        convograph_validator = helpers.get_validator_by_version(format_version)
        convograph_validator(file_as_yml).run()
        start_state_data = self.get_start_state_data(file_as_yml)
        convo_name = start_state_data.get("convo_name")
        convo_description = start_state_data.get("convo_description")

        is_conversation_public = "checkbox" not in self.request.POST
        convoyaml_to_convomodel(
            graph=self.file,
            project=self.project_collaborator.project,
            convo_name=convo_name,
            convo_description=convo_description,
            is_public=is_conversation_public,
        )
        return

    def get_start_state_data(self, file_as_yml):
        for state_data in file_as_yml:
            if state_data["name"] == "start":
                return state_data


class CSVFileForm(Initializer):
    def handle_valid_form(self):
        try:
            self.file_to_saved_db_records()
        except ConvoSpreadsheetValidationError as exc:
            self.result_msg = (
                str(exc)
                + " "
                + mark_safe(
                    '<b><a href="https://gitlab.com/tangibleai/convohub/-/blob/main/data/countByOne_0001.yml">link</a></b>'
                )
            )
            self.flash_msg_caller = messages.error
        return

    def file_to_saved_db_records(self):
        form = ConvoSpreadsheetForm(self.request.POST, self.request.FILES)
        convo_name = form.cleaned_data["convo_name"]
        convo_description = form.cleaned_data["convo_description"]
        file = form.cleaned_data["file"]
        sheet_id = form.cleaned_data["sheet_id"]
        is_public = form.cleaned_data["is_public"]

        file, file_type = self.convert_file_to_django_file_class(
            file, sheet_id, self.request
        )

        upload_spreadsheet_to_db(
            self.project_collaborator.project,
            file,
            file_type,
            convo_name,
            convo_description,
            is_public,
        )
        return

    def convert_file_to_django_file_class(self, file_input, sheet_id_input, request):
        file_type = "csv"
        if file_input:
            file_content = request.FILES["file"].read()
            file_name = request.FILES["file"].name

            if file_name.endswith(".xlsx"):
                file_type = "xlsx"
        elif sheet_id_input:
            sheet_link = f"https://docs.google.com/spreadsheets/d/{sheet_id_input}/export?format=csv"
            sheet_file = requests.get(sheet_link)
            file_content = sheet_file.content
            file_name = "spreadsheet.csv"

        file = File(io.BytesIO(file_content), name=file_name)
        return file, file_type


class UploadedFileForm:
    """Accepts the request object with uploaded file. Creates db records - Convo and related models from file content"""

    ext_to_handler_class = {
        "yml": YmlFileForm,
        "yaml": YmlFileForm,
        "csv": CSVFileForm,
    }
    ext_to_form_class = {
        "yml": ConvoForm,
        "yaml": ConvoForm,
        "csv": ConvoSpreadsheetForm,
    }

    def __init__(self, request):
        file = request.FILES["file"]
        filename = file.name
        file_ext = filename.split(".")[-1]

        form_class = self.ext_to_form_class.get(file_ext)
        self.form = form_class(request.POST, request.FILES)

        handler_class = self.ext_to_handler_class.get(file_ext)
        self.handler = handler_class(request)

    def handle(self):
        if self.form.is_valid():
            self.handler.handle_valid_form()
        return

    def get_result_message(self):
        return self.handler.result_msg

    def get_flash_message_caller(self):
        return self.handler.flash_msg_caller
