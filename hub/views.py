import json
import requests
import uuid
import yaml
from pathlib import Path

from datetime import datetime
from django.contrib import messages
from django.contrib.auth.models import AnonymousUser, User
from django.http import FileResponse
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.utils.html import format_html
from django.views import View
from rest_framework.response import Response
from rest_framework.views import APIView

from convohub import settings
from convohub.constants import DEFAULT_PROJECT

from hub.forms import ConvoForm

from hub.models import Convo, State, Project
from users.helpers import digitalocean
from .helpers.uploaded_file import UploadedFileForm
from .utils import get_project

ENV = settings.ENV


class APINextStateReply(APIView):
    """Returns the next state based on the input

    Uses the State.get_next_state_from_database in models.py to find the next state information

    Use with /quiz/api

    JSON Input Example
    {
        'state_name': 'select-language',
        'user_text': 'English',
        'context': {'lang': 'en'}
    }

    Query Parameter Example
    /quiz/api/?state_name=selected-language-welcome&user_text=Ready
    """

    def get(self, request):
        data = request.data
        data.update(request.query_params.dict())
        time = datetime.now()
        message_id = uuid.uuid4()

        response = State.get_next_state_from_database(data)

        return Response(
            {
                "state": response["state"],
                "messages": response["messages"],
                "triggers": response["triggers"],
                "context": response["context"],
                "message_id": message_id,
                "message_direction": "outbound",
                "sender_type": "chatbot",
                "user_id": "chat_j2Lk356",
                "bot_id": "Poly Chatbot",
                "channel": "WSNYC Website",
                "send_time": time,
            }
        )


def is_signed_up_user(user):
    """Checks whether a logged in user has actually signed up or not"""
    if not user.email and not user.password:
        return False
    return True


def add_links_to_public_convos(url_host, url_scheme, url_full_path, convos):
    authority = url_scheme + url_host
    full_path_parts = [p for p in url_full_path.split("/") if p not in ("/", "")]

    if full_path_parts[-1].startswith("?"):
        del full_path_parts[-1]
    for c in convos:
        if c.is_public:
            full_path = f'/{"/".join(full_path_parts)}/{c.id}'
            c.public_url = authority + full_path
    return convos


def get_bot_widget_icon(request):
    widget_icon_relative_path = request.GET.get("widget_icon_relative_path")[1:]

    if settings.USE_S3:
        image_url = digitalocean.get_private_image_url(widget_icon_relative_path)
        response = requests.get(image_url)
        image_data = response.content
        content_type = response.headers.get("Content-Type", "application/octet-stream")
        return HttpResponse(image_data, content_type=content_type)
    elif Path(widget_icon_relative_path).is_file():
        opened_file = open(widget_icon_relative_path, "rb")
        response = FileResponse(opened_file)
        return response
    else:
        return HttpResponse(status=404)


def handle_public_convo(request, convo_id):
    public_convo = Convo.objects.get(id=convo_id)
    with public_convo.file.open("r") as file:
        file_content = file.read()
    states_list = yaml.safe_load(file_content)
    formatted_data = json.dumps(states_list, indent=4)

    return render(
        request,
        "hub/public_convo.html",
        {"formatted_data": formatted_data},
    )


class BaseView(View):
    file_css = "polyface/polyface.css"
    file_js = "polyface/polyface.js"

    def post(self, request):
        file_form_handler = UploadedFileForm(request)
        file_form_handler.handle()
        result_msg = file_form_handler.get_result_message()
        flash_message_caller = file_form_handler.get_flash_message_caller()
        flash_message_caller(request, format_html("{}", result_msg))
        return


class BothubView(BaseView):
    template_name = "hub/bothub.html"

    def get(self, request):
        """handles GET request"""
        payload = request.GET
        if payload and "flash_message" in payload:
            messages.info(request, payload["flash_message"])
            return redirect("bothub")

        project = get_project(request.user)
        form = ConvoForm()
        return render(
            request,
            self.template_name,
            {
                "file_css": self.file_css,
                "file_js": self.file_js,
                "project_name_slug": project.slug,
                "form": form,
                "visitor_id": request.user.id
                if not isinstance(request.user, AnonymousUser)
                else None,
            },
        )

    def post(self, request):
        """handles POST request"""
        super().post(request)
        return redirect("bothub")


class DashboardView(BaseView):
    template_name = "hub/dashboard.html"

    def get(self, request):
        """handles GET request"""
        project = get_project(request.user)
        active_convo = self.active_project_convo_or_default(project)
        websocket_url = (
            ENV.get("WEBSOCKET_URL") + f"/ws/quiz/{request.user}/{active_convo.id}"
        )
        widget_picture = self.get_widget_picture(request)
        return render(
            request,
            self.template_name,
            {
                "file_css": self.file_css,
                "file_js": self.file_js,
                "welcome_popup_on": json.dumps(False),
                "empty_message_allowed": json.dumps(False),
                "name": "Qary",
                "bot_picture": widget_picture,
                "fetch_image_endpoint_url": "http://localhost:8000/quiz/bot_widget_icon",
                "ws_url": websocket_url,
                "is_s3": json.dumps(str(settings.USE_S3) in (True, "True", "true")),
                "project_name_slug": project.slug,
                "form": ConvoForm(),
                "visitor_id": request.user.id
                if not isinstance(request.user, AnonymousUser)
                else None,
            },
        )

    def active_project_convo_or_default(self, project):
        return Convo.objects.filter(project=project).order_by(
            "-activated_on"
        ).first() or Convo.objects.get(project=Project.objects.get(**DEFAULT_PROJECT))

    def get_widget_picture(self, request):
        try:
            widget_pic = request.user.profile.bot_widget_image.url
        except AttributeError:
            widget_pic = User.objects.get(username=ENV.get("DJANGO_DEFAULT_USERNAME"))
        return widget_pic

    def post(self, request):
        """handles POST request"""
        super().post(request)
        return redirect("dashboard")


def chat_page(request, project_name_slug):
    file_css = "polyface/polyface.css"
    file_js = "polyface/polyface.js"

    project = Project.objects.filter(slug=project_name_slug).first()

    active_convo = (
        Convo.objects.filter(project=project).order_by("-activated_on").first()
    ) or Convo.objects.filter(project=Project.objects.get(**DEFAULT_PROJECT)).order_by(
        "-activated_on"
    ).first()
    url = ENV.get("WEBSOCKET_URL") + f"/ws/quiz/{request.user}/{active_convo.id}"
    try:
        widget_pic = request.user.profile.bot_widget_image.url
    except AttributeError:
        widget_pic = User.objects.get(username=ENV.get("DJANGO_DEFAULT_USERNAME"))

    return render(
        request,
        "hub/chat_page.html",
        {
            "file_css": file_css,
            "file_js": file_js,
            "welcome_popup_on": json.dumps(False),
            "empty_message_allowed": json.dumps(False),
            "name": "Qary",
            "bot_picture": widget_pic,
            "fetch_image_endpoint_url": "http://localhost:8000/quiz/bot_widget_icon",
            "ws_url": url,
            "is_s3": json.dumps(str(settings.USE_S3) in (True, "True", "true")),
            "project_name_slug": project_name_slug,
        },
    )
