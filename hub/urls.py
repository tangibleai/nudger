from django.urls import path

from hub import views

urlpatterns = [
    # sequence is important!
    path("bothub", views.BothubView.as_view(), name="bothub"),
    path("dashboard", views.DashboardView.as_view(), name="dashboard"),
    path("bot_widget_icon", views.get_bot_widget_icon, name="bot-widget-icon"),
    path(
        "bothub/<str:convo_id>",
        views.handle_public_convo,
        name="public-convo",
    ),
]
