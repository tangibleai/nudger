from hub.models import Action, State, Trigger


def actions_to_db_records(dialog, convo):
    for state_name, state_data in dialog.items():
        if "actions" in state_data:
            create_state_actions(state_name, convo, state_data)
    return


def create_state_actions(state_name, convo, state_data):
    state = State.objects.get(
        state_name=state_name,
        convo=convo,
    )
    for action_data in state_data["actions"]:
        create_action_db_record(action_data, state)
    return


def create_action_db_record(action_data, state):
    for action_name, kwargs in action_data.items():
        Action.objects.create(
            name=action_name,
            state=state,
            kwargs=kwargs,
        )
    return


def triggers_to_db_records(dialog, convo):
    for state_name, state_data in dialog.items():
        state_triggers = state_data["triggers"]
        from_state = State.objects.get(
            state_name=state_name,
            convo=convo,
        )
        state_triggers_to_db_records(state_triggers, convo, from_state)
    return


def state_triggers_to_db_records(state_triggers, convo, from_state):
    for trig_data in state_triggers:
        trig_text = trig_data["user_text"]
        target_state_name = trig_data["target"]
        to_state = State.objects.get(state_name=target_state_name, convo=convo)
        create_context = {
            "from_state": from_state,
            "to_state": to_state,
            "intent_text": trig_text,
            "is_button": False,
        }
        if "button_text" in trig_data:
            create_context["is_button"] = True
            create_context["button_text"] = trig_data["button_text"]
        Trigger.objects.create(**create_context)
    return
