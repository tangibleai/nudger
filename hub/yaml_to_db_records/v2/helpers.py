from hub.models import Message, State, Trigger

LANGS = {
    "English": "en",
    "Spanish (Español)": "es",
    "Chinese - Simplified (简体中文)": "zh-hans",
    "Chinese - Traditional (繁體中文)": "zh-hant",
}


def messages_to_db_records(dialog, convo):
    for state_name, state_data in dialog.items():
        state = State.objects.get(
            state_name=state_name,
            convo=convo,
        )
        for lang in LANGS.values():
            # Makes an entry in the Message table for each English message of a State
            for i, msg in enumerate(state_data.get("actions", {}).get(lang, "")):
                Message.objects.create(
                    state=state,
                    sequence_number=i,
                    bot_text=msg,
                    lang=lang,
                )
    return


def triggers_to_db_records(dialog, convo):
    for s_name in dialog:
        from_state = State.objects.get(
            state_name=s_name,
            convo=convo,
        )
        for lang in LANGS.values():
            # TODO: See if this try can be simplified to test whether there is a langauge represented and then take the stuff if it is or pass if it isn't

            lang_assets = get_lang_assets(dialog, s_name, lang)
            triggers, btns, functions_to_call = (
                lang_assets["triggers"],
                lang_assets["buttons"],
                lang_assets["functions_to_call"],
            )
            for is_btn, items in enumerate([triggers, btns]):
                for intent_text, to_state_name in items:
                    to_state = State.objects.get(state_name=to_state_name, convo=convo)
                    Trigger.objects.create(
                        from_state=from_state,
                        to_state=to_state,
                        intent_text=intent_text,
                        update_kwargs=functions_to_call,
                        lang=lang,
                        is_button=is_btn,
                    )
    return None


def get_lang_assets(dialog, state_name, lang):
    """Returns language functions to call, buttons and triggers"""
    try:
        functions_to_call = dict(
            dialog[state_name].get("triggers", {}).get("functions", "").items()
        )
    except:
        functions_to_call = None

    try:
        buttons = list(dialog[state_name].get("buttons", {}).get(lang, "").items())
    except AttributeError:
        buttons = list(dialog[state_name].get("buttons", {}).get(lang, ""))

    try:
        triggers = list(dialog[state_name].get("triggers", {}).get(lang, "").items())
    except AttributeError:
        triggers = list(dialog[state_name].get("triggers", {}).get(lang, ""))

    return {
        "functions_to_call": functions_to_call,
        "buttons": buttons,
        "triggers": triggers,
    }
