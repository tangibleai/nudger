import io
import re
import urllib
import yaml
from collections import abc
from pathlib import Path

import numpy as np
import pandas as pd

from django.conf import settings
from hub.models import Convo, State
from .version_helpers import VersionHelper


LANGS = {
    "English": "en",
    "Spanish (Español)": "es",
    "Chinese - Simplified (简体中文)": "zh-hans",
    "Chinese - Traditional (繁體中文)": "zh-hant",
}


def coerce_dialog_tree_series(states):
    """Ensure that (yamlfilepath | list) -> dict -> Series"""
    if isinstance(states, pd.Series):
        return states
    if isinstance(states, (str, Path)):
        with Path(states).open() as fin:
            states = yaml.full_load(fin)
    if isinstance(states, io.TextIOWrapper):
        states = yaml.full_load(states)
    if isinstance(states, abc.Mapping):
        return pd.Series(states)
    if isinstance(states, (list, np.ndarray)):
        states = pd.Series(
            states, index=[s.get("name", str(i)) for (i, s) in enumerate(states)]
        )
        states.index.name = "name"
        return states
    raise ValueError(
        f"Unable to coerce {type(states)} into pd.Series:\n  states={str(states)[:130]}..."
    )


def convoyaml_to_convomodel(
    graph, project, convo_name, convo_description, is_public=True
) -> Convo:
    convos = [
        obj
        for obj in Convo.objects.filter(project=project)
        if convo_name in obj.name and re.match(r"\d+$", obj.name.split("_")[-1])
    ]
    max_version = max([int(obj.name.split("_")[-1]) for obj in convos] or [0])
    new_convo_name = (
        f"{convo_name}_{max_version + 1}"
        if max_version or len(Convo.objects.filter(project=project, name=convo_name))
        else convo_name
    )
    new_convo = Convo.objects.create(
        project=project,
        file=graph,
        name=new_convo_name,
        description=convo_description,
        is_public=is_public,
    )

    if settings.USE_S3:
        url = f"{settings.MEDIA_URL}{new_convo.file.name}"
        response = urllib.request.urlopen(url)
        file_path = io.TextIOWrapper(
            response, encoding="utf-8"
        )  # using S3 as an object storage
    else:
        file_path = (
            f"{settings.MEDIA_ROOT}/{new_convo.file.name}"  # using local storage
        )

    graph_to_python = yaml.safe_load(new_convo.file.read())
    format_version = get_conversation_version(graph_to_python)
    new_convo.version = format_version
    new_convo.save()

    DIALOG_TREE = coerce_dialog_tree_series(file_path)
    dialog = {node["name"]: node for node in DIALOG_TREE}
    states_to_db_records(dialog, new_convo)
    VersionHelper(dialog, new_convo, format_version).execute_helpers()
    return new_convo


def get_conversation_version(conversation):
    for state_data in conversation:
        state_name = state_data["name"]
        if state_name == "start":
            return state_data["version"]
    return "default"


def states_to_db_records(dialog, convo):
    nlp_algo = dialog["start"]["nlp"]
    for name in dialog:
        if "nlp" in dialog[name]:
            nlp_algo = dialog[name]["nlp"]
        State.objects.create(state_name=name, convo=convo, nlp_algo=nlp_algo)
    return
