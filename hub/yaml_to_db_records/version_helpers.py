import importlib


class VersionHelper:
    module_names = ["helpers"]
    helpers = [
        "messages_to_db_records",
        "actions_to_db_records",
        "triggers_to_db_records",
    ]

    def __init__(self, conversation, convo, conversation_version):
        self.conversation = conversation
        self.convo = convo
        self.dir_name = self.conversation_version_to_directory_name(
            conversation_version
        )

    def conversation_version_to_directory_name(self, version):
        version_as_str = str(version)
        self.dir_name = "v" + version_as_str.replace(".", "_")
        return self.dir_name

    def execute_helpers(self):
        for module_name in self.module_names:
            self.execute_module_helpers(module_name)
        return

    def execute_module_helpers(self, module_name):
        module = importlib.import_module(
            f".{self.dir_name}.{module_name}", package=__package__
        )
        for helper in self.helpers:
            self.get_and_execute_helper(module, helper)
        return

    def get_and_execute_helper(self, module, helper):
        callable_helper = getattr(module, helper, None)
        if callable_helper is not None:
            callable_helper(self.conversation, self.convo)
        return
