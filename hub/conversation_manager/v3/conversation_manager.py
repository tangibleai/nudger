import json
import logging

from hub.models import Action, Convo, State, Trigger, ProjectCollaborator
from hub.understanders import TriggerMatcher
from hub.actions_system import action_runners
from hub.actions_system.actions import send_triggers
from convohub.constants import START_STATE_NAME
from users.models import Context

from django.contrib.auth import get_user_model


User = get_user_model()
log = logging.getLogger(__name__)


class ConversationRunner:
    def __init__(self, context):
        self.context = context
        convo_id = context["convo_id"]
        self.convo = Convo.objects.get(id=convo_id)

        # TODO: Need to refactor to incorporate multiple users or project
        self.project = self.convo.project
        self.project_collaborator = ProjectCollaborator.objects.filter(
            project=self.project
        ).first()

        self.context_record = self.get_or_create_context_record()
        self.state_name = context.get("state_name", START_STATE_NAME)
        self.state = State.objects.get(convo=self.convo, state_name=self.state_name)
        self.user_text = context.get("user_text", "")
        self.nlp_algo = ""
        self.state_actions = None
        self.triggers = list()
        self.update_context({"conversation_version": self.convo.version})

    def get_or_create_context_record(self):
        context_record, _ = Context.objects.get_or_create(
            user=self.project_collaborator.user
        )
        return context_record

    def fill_context_with_next_state(self):
        self.user_text = self.context.get("user_text", "")
        self.process_next_state()
        self.update_context(
            {
                "state_name": self.state_name,
                "user_text": self.user_text,
            }
        )
        self.update_context_in_db()
        return self.context

    def process_next_state(self):
        if self.state_name == START_STATE_NAME:
            self.process_start_state()
        self.process_next_chained_states()
        return

    def process_start_state(self):
        self.process_state_actions()
        self.user_text = "__next__"
        return

    def process_next_chained_states(self):
        self.move_to_next_state()
        self.process_state_actions()
        self.process_next_states_having_next_trigger()
        self.process_state_triggers()
        send_triggers(self.context)
        return

    def process_state_actions(self):
        self.extract_state_actions()
        if self.state_has_actions():
            self.execute_state_actions()
        return

    def extract_state_actions(self):
        self.state_actions = Action.objects.filter(state=self.state)
        return self.state_actions

    def state_has_actions(self):
        return bool(len(self.state_actions))

    def execute_state_actions(self):
        for action in self.state_actions:
            action_name = action.name
            kwargs = action.kwargs
            self.execute_action(action_name, kwargs)
        return

    def execute_action(self, action_name, kwargs):
        self.update_context(kwargs)
        if not action_runners.is_allowed(action_name):
            return
        context_diff = action_runners.run(action_name, self.context)
        self.update_context(context_diff)
        return

    def move_to_next_state(self):
        self.extract_state_nlp_algo()
        self.extract_transition_context()
        return

    def extract_state_nlp_algo(self):
        self.nlp_algo = self.state.nlp_algo
        if self.nlp_algo is None:
            self.nlp_algo = self.get_start_state_nlp_algo()
        return self.nlp_algo

    def get_start_state_nlp_algo(self):
        start_state = State.objects.get(state_name=START_STATE_NAME)
        nlp_algo = start_state.nlp_algo
        return nlp_algo

    def extract_transition_context(self):
        self.extract_state_triggers()
        self.determine_next_state_name()
        self.state = State.objects.get(convo=self.convo, state_name=self.state_name)
        return

    def extract_state_triggers(self):
        self.triggers = Trigger.objects.filter(from_state=self.state)
        return

    def determine_next_state_name(self):
        fallback_state_name = START_STATE_NAME
        self.state_name = None
        for t in self.triggers:
            target_state_name = t.to_state.state_name
            trigger_text = t.intent_text
            if self.does_user_text_match_trigger_text(trigger_text):
                self.state_name = target_state_name
                return self.state_name
            if trigger_text == "__default__":
                fallback_state_name = target_state_name
        self.state_name = self.state_name or fallback_state_name
        return self.state_name

    def does_user_text_match_trigger_text(self, trigger_text):
        return TriggerMatcher().is_match(
            algo_name=self.nlp_algo,
            trig_text=trigger_text,
            user_text=self.user_text,
        )

    def process_next_states_having_next_trigger(self):
        self.extract_state_triggers()
        while self.state_has_next_trigger():
            self.user_text = "__next__"
            self.move_to_next_state()
            self.process_state_actions()
            self.extract_state_triggers()
        return

    def state_has_next_trigger(self):
        for trigger in self.triggers:
            if trigger.intent_text == "__next__":
                return True
        return False

    def process_state_triggers(self):
        self.trigger_model_instances_to_dictionary()
        self.convert_triggers_to_json()
        self.context = action_runners.update_context(
            self.context, "triggers", self.triggers
        )
        return

    def trigger_model_instances_to_dictionary(self):
        triggers_data = list()
        for t in self.triggers:
            trig_data = {
                "intent_text": t.intent_text,
                "target": t.to_state.state_name,
            }
            if t.button_text:
                trig_data["button_text"] = t.button_text
            triggers_data.append(trig_data)
        self.triggers = triggers_data
        triggers_context = {"triggers": triggers_data}
        return triggers_context

    def convert_triggers_to_json(self):
        triggers_to_json = []
        for t in self.triggers:
            triggers_to_json.append(self.trigger_to_json(t))
        self.triggers = triggers_to_json
        return

    def trigger_to_json(self, trigger: Trigger):
        stringified_trig = {}
        for key, value in trigger.items():
            try:
                stringified_trig[json.dumps(key)] = json.dumps(value)
            except TypeError:
                stringified_trig[json.dumps(key)] = str(value)
        return stringified_trig

    def update_context(self, context_diff):
        for kwarg, value in context_diff.items():
            self.context = action_runners.update_context(self.context, kwarg, value)
        return self.context

    def update_context_in_db(self):
        self.context_record.context = self.context
        self.context_record.save()
        return
