import json
import logging

from hub.models import Convo, State, Trigger
from hub.model_converters import MessageConverter, TriggerConverted
from hub.understanders import TriggerMatcher
from hub.actions_system import action_runners
from convohub.constants import START_STATE_NAME

from django.contrib.auth import get_user_model


User = get_user_model()
log = logging.getLogger(__name__)

from hub.actions_system.actions import send_message, send_triggers


class ConversationRunner:
    def __init__(self, context):
        self.context = context
        convo_id = context["convo_id"]
        self.convo = Convo.objects.get(id=convo_id)
        self.context = action_runners.update_context(
            self.context, "conversation_version", self.convo.version
        )

        self.state_name = context.get("state_name", START_STATE_NAME)
        self.user_text = context.get("user_text", "")
        self.key_to_function_result_mapping = {}
        self.bot_messages = context.get("bot_messages", [])
        self.lang = context.get("lang", "en")
        self.triggers = context.get("triggers", [])
        self.message_sequence_num = 0

    def fill_context_with_next_state(self):
        self.reinitialize_variables()
        self.extract_next_state()
        self.update_context()
        self.send_state_assets()
        return self.context

    def reinitialize_variables(self):
        self.user_text = self.context.get("user_text", "")
        self.bot_messages = self.context.get("bot_messages", [])

    def send_state_assets(self):
        self.send_messages()
        self.send_triggers()
        return

    def send_messages(self):
        bot_messages_count = len(self.bot_messages)
        for i, msg in enumerate(self.bot_messages):
            self.context["bot_text"] = msg["bot_text"]
            self.context["message_sequence_number"] = self.message_sequence_num
            self.context["is_message_last"] = i == bot_messages_count - 1
            self.message_sequence_num += 1
            send_message(self.context)
        self.context["bot_text"] = ""
        self.message_sequence_num = 0
        return

    def send_triggers(self):
        self.context["triggers"] = self.triggers
        send_triggers(self.context)
        self.context["triggers"] = []
        return

    def extract_next_state(self):
        if self.state_name == START_STATE_NAME:
            self.process_start_state()  # only for just started conversation

        self.move_to_next_state()
        self.extract_next_state_assets()
        self.process_next_states_having_next_trigger()
        return

    def process_start_state(self):
        self.extract_start_state_messages()
        self.user_text = "__next__"
        return

    def extract_start_state_messages(self):
        if self.state_has_next_trigger():
            start_state = State.objects.get(
                state_name=START_STATE_NAME, convo=self.convo
            )
            self.bot_messages = MessageConverter.messages_to_list_of_dicts(
                start_state, self.lang
            )
        return self.bot_messages

    def state_has_next_trigger(self):
        from_state = State.objects.get(state_name=self.state_name, convo=self.convo)
        triggers = Trigger.objects.filter(from_state=from_state, is_button=False)
        return any(True if t.intent_text == "__next__" else False for t in triggers)

    def move_to_next_state(self):
        from_state = State.objects.get(state_name=self.state_name, convo=self.convo)
        nlp_algo = from_state.nlp_algo
        triggers = TriggerConverted.triggers_to_list_of_dicts(from_state, self.lang)
        default_state_name = START_STATE_NAME
        next_state_name = None
        for t in triggers:
            if TriggerMatcher().is_match(
                algo_name=nlp_algo, trig_text=t["intent_text"], user_text=self.user_text
            ):
                next_state_name = t["to_state"]
                break
            if t["intent_text"] == "__default__":
                default_state_name = t["to_state"]
        if next_state_name is None:
            next_state_name = default_state_name
        self.state_name = next_state_name
        return

    def extract_next_state_assets(self) -> dict:
        from_state = State.objects.get(state_name=self.state_name, convo=self.convo)
        next_state_messages = MessageConverter.messages_to_list_of_dicts(
            from_state, self.lang
        )
        next_state_triggers = TriggerConverted.triggers_to_list_of_dicts(
            from_state, self.lang
        )
        self.bot_messages += next_state_messages
        self.triggers = next_state_triggers
        self.convert_triggers_to_json()
        return

    def convert_triggers_to_json(self):
        triggers = []
        for t in self.triggers:
            stringified_trig = {}
            for key, value in t.items():
                try:
                    stringified_trig[json.dumps(key)] = json.dumps(value)
                except TypeError:
                    stringified_trig[json.dumps(key)] = str(value)
            triggers.append(stringified_trig)
        self.triggers = triggers

    def process_next_states_having_next_trigger(self):
        while self.state_has_next_trigger():
            self.user_text = "__next__"
            self.move_to_next_state()
            self.extract_next_state_assets()
        return

    def update_context(self):
        self.context.update(
            {
                "state_name": self.state_name,
                "user_text": self.user_text,
            }
        )
        self.context.update(self.key_to_function_result_mapping)
        return self.context
