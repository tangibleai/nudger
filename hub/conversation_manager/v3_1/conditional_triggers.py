def is_int(num: str):
    try:
        int(num)
        return True
    except ValueError:
        return False


def is_float(num: str):
    try:
        float(num)
        return True
    except ValueError:
        return False


class Condition:
    condition_name_to_types_data = {
        ("gt", "gte", "lt", "lte", "eq"): {
            "actual_var_type_verified": lambda var_type: var_type not in (type(None),),
            "actual_var_type_should_be_equal_desired": True,
        },
        ("contains",): {
            "actual_var_type_verified": lambda var_type: var_type not in (type(None),),
            "actual_var_type_should_be_equal_desired": False,
        },
        ("startswith", "endswith"): {
            "actual_var_type_verified": lambda var_type: var_type not in (type(None),),
            "actual_var_type_should_be_equal_desired": False,
        },
    }

    def __init__(self, actual, desired, condition_name: str):
        self.actual = actual
        self.desired = desired
        self.condition_name = condition_name
        self.verify_type_match()

    def run(self):
        callable_condition = getattr(self, self.condition_name)
        return callable_condition()

    def verify_type_match(self):
        for cond_names, types_data in self.condition_name_to_types_data.items():
            if self.condition_name in cond_names:
                self.execute_type_verifications(types_data)
                return
        raise ConditionMatcherError()

    def execute_type_verifications(self, types_data):
        actual_var_type_verifier = types_data["actual_var_type_verified"]
        type_verified = actual_var_type_verifier(type(self.actual))
        if not type_verified:
            raise ConditionMatcherError()
        if types_data["actual_var_type_should_be_equal_desired"] and type(
            self.actual
        ) != type(self.desired):
            raise ConditionMatcherError()
        return

    def gt(self):
        return self.actual > self.desired

    def gte(self):
        return self.actual >= self.desired

    def lt(self):
        return self.actual < self.desired

    def lte(self):
        return self.actual <= self.desired

    def eq(self):
        return self.actual == self.desired

    def contains(self):
        return self.desired in self.actual

    def startswith(self):
        return self.actual.startswith(self.desired)

    def endswith(self):
        return self.actual.endswith(self.desired)


class Aggregation:
    aggregation_name_to_types_data = {
        ("int",): {
            "var_type_verified": lambda var_type: var_type in (str, list, dict),
        },
        ("lower", "strip", "upper", "lstrip", "rstrip"): {
            "var_type_verified": lambda var_type: var_type in (str,),
        },
        ("float",): {
            "var_type_verified": lambda var_type: var_type in (dict,),
        },
        ("len",): {
            "var_type_verified": lambda var_type: var_type not in (type(None),),
        },
    }

    def __init__(self, aggregator_name, value):
        self.aggregator_name = aggregator_name
        self.handler_name = self.aggregator_name
        if is_int(self.aggregator_name):
            self.handler_name = "int"
        elif is_float(self.aggregator_name):
            self.handler_name = "float"
        self.value = value
        self.verify_type_match()

    def verify_type_match(self):
        for aggr_names, types_data in self.aggregation_name_to_types_data.items():
            if self.handler_name in aggr_names:
                actual_var_type_verifier = types_data["var_type_verified"]
                type_verified = actual_var_type_verifier(type(self.value))
                if not type_verified:
                    raise ConditionMatcherError()
                return
        raise ConditionMatcherError()

    def run(self):
        callable_aggregator = getattr(self, self.handler_name)
        return callable_aggregator()

    def len(self):
        return len(self.value)

    def lower(self):
        return self.value.lower()

    def upper(self):
        return self.value.upper()

    def int(self):
        return self.value[int(self.aggregator_name)]

    def float(self):
        return self.value[float(self.aggregator_name)]


class ConditionMatcher:
    def __init__(self, context: dict, expression: str, value):
        self.context = context
        self.expression = expression
        self.value = value
        self.expression_parts = expression.split("__")
        self.expression_beginning = self.expression_parts[0]
        self.define_affected_variable()
        self.condition_provided_or_error()

    def define_affected_variable(self):
        if self.expression_beginning in self.context:
            self.affected_variable = self.context[self.expression_beginning]
        elif (
            is_int(self.expression_beginning)
            or is_float(self.expression_beginning)
            or any(
                [
                    self.expression_beginning in items
                    for items in {
                        **Condition.condition_name_to_types_data,
                        **Aggregation.aggregation_name_to_types_data,
                    }
                ]
            )
        ):
            self.affected_variable = self.context
        else:
            raise ConditionMatcherError(
                f'Unrecognized variable "{self.expression_beginning}" detected'
            )
        return

    def condition_provided_or_error(self):
        if (
            len(self.expression_parts) == 1
            and self.expression_beginning == self.affected_variable
        ):
            raise ConditionMatcherError(
                f"No condition provided for '{self.expression}' condition expression"
            )
        return

    def run(self):
        if (
            self.expression_beginning not in self.context
            and self.expression_beginning != "context"
        ):
            self.expression_parts.insert(0, None)
        self.run_aggregators()
        self.run_condition()
        return self.affected_variable

    def run_aggregators(self):
        aggregators = self.expression_parts[1:-1]
        for aggr_name in aggregators:
            self.recognize_aggregation_name(aggr_name)
            aggregation_runner = Aggregation(
                aggregator_name=aggr_name, value=self.affected_variable
            )
            self.affected_variable = aggregation_runner.run()
        return

    def recognize_aggregation_name(self, aggregation_name):
        is_aggregation = (
            any(
                [
                    aggregation_name in items
                    for items in Aggregation.aggregation_name_to_types_data
                ]
            )
            or is_int(aggregation_name)
            or is_float(aggregation_name)
        )
        if not is_aggregation:
            raise ConditionMatcherError(
                f"{aggregation_name} aggregation isn't recognized"
            )
        return

    def run_condition(self):
        condition_name = self.expression_parts[-1]
        self.recognize_condition_name(condition_name)
        condition_runner = Condition(
            actual=self.affected_variable,
            desired=self.value,
            condition_name=condition_name,
        )
        self.affected_variable = condition_runner.run()

    def recognize_condition_name(self, condition_name):
        is_condition = any(
            [
                condition_name in items
                for items in Condition.condition_name_to_types_data
            ]
        )
        if not is_condition:
            raise ConditionMatcherError(f"{condition_name} condition isn't recognized")
        return


class ConditionMatcherError(Exception):
    pass
