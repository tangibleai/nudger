import json
import logging

from django.contrib.auth import get_user_model

from hub.models import Action, Convo, State, Trigger
from hub.understanders import TriggerMatcher
from hub.actions_system import action_runners
from hub.actions_system.actions import (
    send_triggers,
    resolve_parametrized_string,
    increment_number_of_turns,
)
from convohub.constants import START_STATE_NAME
from users.models import Context
from .conditional_triggers import ConditionMatcher


User = get_user_model()
log = logging.getLogger(__name__)


class ConversationRunner:
    def __init__(self, context):
        self.context = context
        convo_id = context["convo_id"]
        self.convo = Convo.objects.get(id=convo_id)
        username = context["username"]
        user = User.objects.get(username=username)
        self.context_record, _ = Context.objects.get_or_create(user=user)
        self.state_name = context.get("state_name", START_STATE_NAME)
        if self.state_name == START_STATE_NAME:
            self.context["num_turns"] = 0
        self.state = State.objects.get(convo=self.convo, state_name=self.state_name)
        self.user_text = context.get("user_text", "")
        self.nlp_algo = ""
        self.state_actions = None
        self.triggers = list()
        self.update_context({"conversation_version": self.convo.version})

    def fill_context_with_next_state(self):
        self.user_text = self.context.get("user_text", "")
        if self.user_text not in ("", "__next__"):
            self.update_context(increment_number_of_turns(self.context))
        self.add_text_to_messages(text_key="user_text", value=self.user_text)
        self.process_next_state()
        self.update_context(
            {
                "state_name": self.state_name,
                "user_text": self.user_text,
            }
        )

        self.update_context_in_db()
        return self.context

    def add_text_to_messages(self, text_key="bot_text", value=""):
        messages = self.context.get("messages", [])
        msg_data = {
            text_key: value,
            "state_name": self.state_name,
        }
        messages.append(msg_data)
        self.context["messages"] = messages
        return self.context

    def process_next_state(self):
        if self.state_name == START_STATE_NAME:
            self.process_state_actions()
            self.user_text = "__next__"
        self.process_next_chained_states()
        return

    def process_next_chained_states(self):
        self.move_to_next_state()
        self.process_state_actions()
        self.process_next_states_having_next_trigger()
        self.process_state_triggers()
        send_triggers(self.context)
        return

    def process_state_actions(self):
        self.state_actions = Action.objects.filter(state=self.state)
        if len(self.state_actions):
            self.execute_state_actions()
        return

    def execute_state_actions(self):
        for action in self.state_actions:
            action_name = action.name
            kwargs = action.kwargs
            self.execute_action(action_name, kwargs)
        return

    def execute_action(self, action_name, kwargs):
        self.update_context(kwargs)
        if not action_runners.is_allowed(action_name):
            return
        context_diff = action_runners.run(action_name, self.context)
        self.update_context(context_diff)
        return

    def move_to_next_state(self):
        self.extract_state_nlp_algo()
        self.extract_transition_context()
        return

    def extract_state_nlp_algo(self):
        self.nlp_algo = self.state.nlp_algo
        if self.nlp_algo is None:
            start_state = State.objects.get(state_name=START_STATE_NAME)
            self.nlp_algo = start_state.nlp_algo
        return self.nlp_algo

    def extract_transition_context(self):
        self.triggers = Trigger.objects.filter(from_state=self.state)
        self.extract_next_state_name()
        self.state = State.objects.get(convo=self.convo, state_name=self.state_name)
        return

    def extract_next_state_name(self):
        next_state_name_determiner = NextStateName(
            self.context, self.user_text, self.triggers, self.nlp_algo
        )
        self.state_name = next_state_name_determiner.determine()
        return self.state_name

    def process_next_states_having_next_trigger(self):
        self.triggers = Trigger.objects.filter(from_state=self.state)
        while self.state_has_next_trigger():
            self.user_text = "__next__"
            self.move_to_next_state()
            self.process_state_actions()
            self.triggers = Trigger.objects.filter(from_state=self.state)
        return

    def state_has_next_trigger(self):
        for trigger in self.triggers:
            if trigger.intent_text == "__next__":
                return True
        return False

    def process_state_triggers(self):
        self.trigger_model_instances_to_dictionary()
        self.convert_triggers_to_json()
        self.context = action_runners.update_context(
            self.context, "triggers", self.triggers
        )
        return

    def trigger_model_instances_to_dictionary(self):
        triggers_data = list()
        for t in self.triggers:
            trig_data = {
                "intent_text": t.intent_text,
                "target": t.to_state.state_name,
            }
            if t.button_text:
                trig_data["button_text"] = t.button_text
            triggers_data.append(trig_data)
        self.triggers = triggers_data
        triggers_context = {"triggers": triggers_data}
        return triggers_context

    def convert_triggers_to_json(self):
        triggers_to_json = []
        for t in self.triggers:
            triggers_to_json.append(self.trigger_to_json(t))
        self.triggers = triggers_to_json
        return

    def trigger_to_json(self, trigger: Trigger):
        stringified_trig = {}
        for key, value in trigger.items():
            try:
                stringified_trig[json.dumps(key)] = json.dumps(value)
            except TypeError:
                stringified_trig[json.dumps(key)] = str(value)
        return stringified_trig

    def update_context(self, context_diff):
        for kwarg, value in context_diff.items():
            self.context = action_runners.update_context(self.context, kwarg, value)
        return self.context

    def update_context_in_db(self):
        self.context_record.context = self.context
        self.context_record.save()
        return


class NextStateName:
    def __init__(self, context, user_text, triggers, nlp_algo):
        self.context = context
        self.user_text = user_text
        self.triggers = triggers
        self.nlp_algo = nlp_algo

        # variables being populated during workflow
        self.conditional_triggers = []
        self.non_conditional_trigger = None

    def determine(self):
        if self.user_text == "__next__":
            self.divide_triggers_to_conditional_and_non_conditional()

            if not self.conditional_triggers:
                self.state_name = self.non_conditional_trigger.to_state.state_name
            else:
                self.determine_among_conditional_triggers()
        else:
            fallback_state_name = START_STATE_NAME
            self.state_name = None
            for t in self.triggers:
                target_state_name = t.to_state.state_name
                trigger_text = t.intent_text
                if TriggerMatcher().is_match(
                    algo_name=self.nlp_algo,
                    trig_text=trigger_text,
                    user_text=self.user_text,
                ):
                    self.state_name = target_state_name
                    return self.state_name
                if trigger_text == "__default__":
                    fallback_state_name = target_state_name
            self.state_name = self.state_name or fallback_state_name
        return self.state_name

    def divide_triggers_to_conditional_and_non_conditional(self):
        for t in self.triggers:
            if t.triggercondition_set.all():
                self.conditional_triggers.append(t)
            else:
                self.non_conditional_trigger = t
        return

    def determine_among_conditional_triggers(self):
        self.state_name = None
        for t in self.conditional_triggers:
            if self.all_conditions_are_met(t):
                self.state_name = t.to_state.state_name
        if self.state_name is None:
            self.state_name = self.non_conditional_trigger.to_state.state_name
        return self.state_name

    def all_conditions_are_met(self, trigger: Trigger):
        trigger_conditions = trigger.triggercondition_set.all()
        for t_c in trigger_conditions:
            expression = t_c.expression
            value = t_c.value
            condition_matcher = ConditionMatcher(self.context, expression, value)
            if not condition_matcher.run():
                return False
        return True

    def does_user_text_match_trigger_text(self, trigger_text):
        return TriggerMatcher().is_match(
            algo_name=self.nlp_algo,
            trig_text=trigger_text,
            user_text=self.user_text,
        )


class ApiConversation(ConversationRunner):
    def process_next_chained_states(self):
        self.move_to_next_state()
        self.process_state_actions()
        self.process_next_states_having_next_trigger()
        self.process_state_triggers()
        return

    def execute_action(self, action_name, kwargs):
        self.update_context(kwargs)
        if not action_runners.is_allowed(action_name):
            return
        context_diff = {}
        if not action_name.startswith("send"):
            context_diff = action_runners.run(action_name, self.context)
            self.update_context(context_diff)
        elif action_name == "send_message":
            resolve_parametrized_string(self.context)
            self.add_text_to_messages(
                text_key="bot_text", value=self.context["bot_text"]
            )
            return
        return
