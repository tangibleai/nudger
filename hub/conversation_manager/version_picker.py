from hub.conversation_manager.v2.conversation_manager import (
    ConversationRunner as V2ConversationRunner,
)
from hub.conversation_manager.v3.conversation_manager import (
    ConversationRunner as V3ConversationRunner,
)
from hub.conversation_manager.v3_1.conversation_manager import (
    ConversationRunner as V3_1ConversationRunner,
)


VERSION_TO_CONVERSATION_RUNNER = {
    "default": V3ConversationRunner,
    3: V3ConversationRunner,
    2: V2ConversationRunner,
    3.1: V3_1ConversationRunner,
}
