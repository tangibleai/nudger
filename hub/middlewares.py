import random
import string

from django.contrib.auth import login, logout
from django.contrib.auth.models import AnonymousUser, User
from django.contrib.sessions.models import Session
from django.db import IntegrityError
from django.db.utils import DatabaseError

from .models import UserSession
from users.models import ExternalAccount


def generate_random_alphanumeric(length):
    characters = string.ascii_letters + string.digits
    random_alphanumeric = "".join(random.choice(characters) for _ in range(length))
    return random_alphanumeric


def create_random_user():
    while True:
        try:
            new_user = User.objects.create(username=generate_random_alphanumeric(10))
            break
        except IntegrityError:
            pass
    return new_user


def populate_session_middleware(get_response):
    def middleware(request):
        request_user = request.user
        user = (
            create_random_user()
            if isinstance(request_user, AnonymousUser)
            else request_user
        )
        request.user = user

        # login(
        #     request,
        #     user,
        #     backend="django.contrib.auth.backends.ModelBackend",
        # )
        # request.user = user
        # request.session[
        #     "user_id"
        # ] = (
        #     user.id
        # )  # will create or update django.contrib.sessions.models.Session object
        # session = Session.objects.get(session_key=request.session.session_key)
        # UserSession.objects.create(user=user, session=session)

        response = get_response(request)
        return response

    return middleware


def relogin_after_connecting_social_account_middleware(get_response):
    def middleware(request):
        original_user_id = request.COOKIES.get("original_user_id", None)
        current_user_id = request.user.id

        if (
            not original_user_id
            or not current_user_id
            or current_user_id == original_user_id
        ):
            response = get_response(request)
            return response

        current_user = User.objects.get(id=current_user_id)
        current_username = current_user.username
        current_email = current_user.email
        original_user = User.objects.get(id=original_user_id)
        provider_name = request.COOKIES.get("provider_name", None)
        external_account = ExternalAccount.objects.filter(
            username=current_username, email=current_email, provider=provider_name
        )
        if not external_account:
            response = get_response(request)
            return response

        external_account.update(user=original_user)
        request.user = original_user
        current_user.delete()

        login(
            request,
            original_user,
            backend="django.contrib.auth.backends.ModelBackend",
        )

        response = get_response(request)
        response.delete_cookie("original_user_id")
        return response

    return middleware


def sign_in_with_social_account_middleware(get_response):
    def middleware(request):
        prior_user_id = request.COOKIES.get("prior_user_id", None)
        if prior_user_id:
            user = User.objects.get(id=prior_user_id)
            request.user = user
            response = get_response(request)
            return response
        provider_name = request.COOKIES.get("provider_name", None)
        user = request.user
        username = user.username
        email = user.email
        external_account = ExternalAccount.objects.filter(
            username=username, email=email, provider=provider_name
        ).first()
        if not provider_name or not external_account:
            response = get_response(request)
            return response

        if not external_account.user:
            user.delete()
            print(email, User.objects.filter(email=email))
            user = User.objects.get_or_create(username=email, email=email)[0]
            external_account.user = user
            external_account.save()
        user = external_account.user

        request.user = user

        response = get_response(request)
        response.set_cookie("prior_user_id", user.id, max_age=300)
        response.delete_cookie("provider_name")
        return response

    return middleware
