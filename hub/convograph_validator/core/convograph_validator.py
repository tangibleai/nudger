from ..convograph_validator_errors import ConvoGraphValidationError


class ConvoGraphValidator:
    def __init__(self, yaml_to_python):
        """
        :params:
            yaml_to_python: python representation of yaml file content (can be obtained with `yaml.safe_load()` func)
        """
        self._convograph = yaml_to_python
        self._convograph = self.stringify_convograph_contents(self._convograph)
        self._additional_validations = []
        self.include_additional_validations()

    def include_additional_validations(self):
        raise NotImplementedError(
            '"include_additional_validations" method of core "ConvoGraphValidator" class isn\'t implemented.'
        )

    def stringify_convograph_contents(self, content):
        if isinstance(content, dict):
            content = self.content_from_dict_to_str(content)
        elif isinstance(content, list):
            content = self.content_from_list_to_str(content)
        else:
            content = str(content)
        return content

    def content_from_dict_to_str(self, content):
        new_data = {}
        for key, value in content.items():
            new_data[str(key)] = self.stringify_convograph_contents(value)
        content = new_data
        return content

    def content_from_list_to_str(self, content):
        for i, item in enumerate(content.copy()):
            content[i] = self.stringify_convograph_contents(item)
        return content

    def convograph_structure_valid_or_error(self):
        """
        convograph_validator = ConvoGraphValidator([
            {
                "name": "start",
                "convo_name": "example_convo_name",
                "convo_description": "example_convo_description",
                "nlp": "keyword",
                "level": 0,
                "actions": {
                    "en": [
                        "Hi, I'm a chatbot at Fondation Botnar.",
                    ]
                },
                "triggers": {"en": {"__next__": "stop"}},
            },
            {
                "name": "stop",
                "actions": {"en": ["The end"]},
                "triggers": {"en": {"some_trigger": "start", "__default__": "start"}},
            },
        ])

        >>> convograph_validator.convograph_structure_valid_or_error()
        True
        """
        self.convograph_is_list_or_error()
        self.every_state_is_dict_or_error()
        return

    def convograph_is_list_or_error(self):
        if not isinstance(self._convograph, list):
            raise ConvoGraphValidationError(
                'Wrong syntax! File content should contain multiple entries starting from "-" character. Most likely you forgot to put leading "-" before one of states.'
            )

    def every_state_is_dict_or_error(self):
        for state in self._convograph:
            if not isinstance(state, dict):
                state_name_or_definition = state["name"] if "name" in state else state
                raise ConvoGraphValidationError(
                    f'"{state_name_or_definition}" state doesn\'t start from leading "-" character.'
                )

    def every_atomic_value_in_convograph_fits_size_or_error(self):
        for s in self._convograph:
            if s["name"] in ("start", "stop"):
                continue
            self.every_atomic_value_in_state_fits_size_or_error(s)
        return

    def every_atomic_value_in_state_fits_size_or_error(self, state):
        state_name = state["name"]
        for attr_name, value in state.items():
            self.value_fits_size_or_error(value, attr_name, state_name)
        return

    def value_fits_size_or_error(self, attr_val, attr_name, state_name):
        if isinstance(attr_val, dict):
            self.every_value_in_dict_fits_size_or_error(attr_val, state_name)
        elif isinstance(attr_val, list):
            self.every_item_in_list_fits_size_or_error(attr_val, attr_name, state_name)
        elif isinstance(attr_val, str):
            self.str_value_fits_size_or_error(attr_val, attr_name, state_name)
        return

    def every_value_in_dict_fits_size_or_error(self, attr_val, state_name):
        for sub_attr_name, sub_attr_val in attr_val.items():
            self.value_fits_size_or_error(sub_attr_val, sub_attr_name, state_name)
        return

    def every_item_in_list_fits_size_or_error(self, attr_val, attr_name, state_name):
        for item in attr_val:
            self.value_fits_size_or_error(item, attr_name, state_name)
        return

    def str_value_fits_size_or_error(self, val, attr_name, state_name):
        if len(val) > 1024:
            raise ConvoGraphValidationError(
                f'Some value of "{attr_name}" attribute of "{state_name}" state exceeded max length of 1024 characters.'
            )

    def run(self):
        validations = [
            self.convograph_structure_valid_or_error,
            self.every_atomic_value_in_convograph_fits_size_or_error,
        ]
        for v in validations:
            v()
        for v in self._additional_validations:
            v.run()
        return
