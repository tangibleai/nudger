class ActionValidator:
    def __init__(self, convograph):
        self._convograph = convograph
        self._state_name = None
        self._action_data = None

    def actions_valid_or_error(self):
        raise NotImplementedError(
            '"actions_valid_or_error" method of core "ActionValidator" class isn\'t implemented'
        )

    def run(self):
        validations = [
            self.actions_valid_or_error,
        ]
        for v in validations:
            v()
        return
