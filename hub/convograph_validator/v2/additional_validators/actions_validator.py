from ...convograph_validator_errors import ConvoGraphValidationError


class ActionValidator:
    def __init__(self, convograph):
        self._convograph = convograph

    def actions_valid_or_error(self):
        for state_data in self._convograph:
            if state_data["name"] in ("start", "stop"):
                continue
            if not isinstance(state_data["actions"], dict):
                raise ConvoGraphValidationError(
                    f'"{state_data["name"]}" state has wrong value specified for "actions". It must be of `dict` type.'
                )
            for lang, lang_actions in state_data["actions"].items():
                if not isinstance(lang_actions, list):
                    raise ConvoGraphValidationError(
                        f'"{state_data["name"]}" state has wrong value specified for "{lang}" actions. It must be of `list` type.'
                    )
                if not len(lang_actions):
                    raise ConvoGraphValidationError(
                        f'"{lang}" actions are empty for "{state_data["name"]}" state.'
                    )
        return True

    def run(self):
        validations = [
            self.actions_valid_or_error,
        ]
        for v in validations:
            v()
        return
