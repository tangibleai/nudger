from ...convograph_validator_errors import ConvoGraphValidationError
from ...core.additional_validators.triggers_validator import (
    TriggerValidator as CoreTriggerValidator,
)


class TriggerValidator(CoreTriggerValidator):
    def __init__(self, convograph):
        super().__init__(convograph)
        self.state_name = None
        self.trigger_data = None
        self.required_trigger_data_keys = ("user_text", "target")
        self.state_has_conditional_trigger = False
        self.state_data = None

    def triggers_valid_or_error(self):
        for state_data in self._convograph:
            self.state_data = state_data
            if state_data["name"] in ("start", "stop"):
                continue
            self.state_triggers_is_list_or_error()
            self.state_triggers_valid_or_error()
            self.triggers_have_essential_or_error()
        return

    def state_triggers_is_list_or_error(self):
        state_triggers = self.state_data["triggers"]
        if not isinstance(state_triggers, list):
            raise ConvoGraphValidationError(
                f'"{self.state_data["name"]}" state has wrong value specified for "triggers". It must be of `list` type.'
            )
        return

    def state_triggers_valid_or_error(self):
        for trigger_data in self.state_data["triggers"]:
            self.trigger_data = trigger_data
            self.state_name = self.state_data["name"]
            self.trigger_data_is_valid_or_error()
        return

    def trigger_data_is_valid_or_error(self):
        self.trigger_data_is_dict_or_error()
        self.trigger_data_has_required_keys()
        self.trigger_points_on_existing_state_or_error()
        return

    def trigger_data_is_dict_or_error(self):
        if not isinstance(self.trigger_data, dict):
            raise ConvoGraphValidationError(
                f'Some trigger data inside "{self.state_name}" state isn\'t a `dict` type.'
            )
        return

    def trigger_data_has_required_keys(self):
        for key in self.required_trigger_data_keys:
            if key not in self.trigger_data:
                raise ConvoGraphValidationError(
                    f'"{self.trigger_data}" trigger data doesn\'t have required "{key}" key.'
                )
        return

    def trigger_points_on_existing_state_or_error(self):
        target_state_name = self.trigger_data["target"]
        if target_state_name not in self._states_names:
            raise ConvoGraphValidationError(
                f'"{self.trigger_data}" trigger of "{self.state_name}" state points on unexisting state.'
            )
        return

    def triggers_have_essential_or_error(self):
        for state_data in self._convograph:
            self.state_data = state_data
            if state_data["name"] in ("start", "stop"):
                continue
            self.next_and_default_not_used_together_or_error()
        return

    def next_and_default_not_used_together_or_error(self):
        triggers = self.get_triggers_texts()
        forbidden_triggers_combination = set(("__default__", "__next__"))
        if forbidden_triggers_combination <= triggers:
            return ConvoGraphValidationError(
                f'"__default__" and "__next__" trigger can\'t be used together in "{self.state_data["name"]}" state.'
            )
        elif "__default__" not in triggers and "__next__" not in triggers:
            return ConvoGraphValidationError(
                f'Neither "__default__ " nor "__next__" trigger was found inside triggers of "{self.state_data["name"]}" state.'
            )

    def get_triggers_texts(self):
        triggers = set()
        for trig_data in self.state_data["triggers"]:
            trig_text = trig_data["user_text"]
            triggers.add(trig_text)
        return triggers

    def states_comply_with_next_trigger_or_error(self):
        for state_data in self._convograph:
            if state_data["name"] in ("start", "stop"):
                continue
            self.state_data = state_data
            triggers_names = self.get_state_triggers_names()
            self.vefify_no_triggers_used_with_next(triggers_names)
            self.check_state_has_conditional_trigger()
            self.verify_state_complies_with_conditional_trigger(triggers_names)
        return True

    def get_state_triggers_names(self):
        triggers_names = list()
        for trig_data in self.state_data["triggers"]:
            trig_text = trig_data["user_text"]
            triggers_names.append(trig_text)
        return triggers_names

    def vefify_no_triggers_used_with_next(self, triggers_names):
        if "__next__" in triggers_names and len(set(triggers_names)) > 1:
            raise ConvoGraphValidationError(
                f'No triggers can be used inside triggers of "{self.state_data["name"]}" state along with "__next__" trigger.'
            )
        return

    def check_state_has_conditional_trigger(self):
        for trig_data in self.state_data["triggers"]:
            if "condition" in trig_data:
                self.state_has_conditional_trigger = True
        return

    def verify_state_complies_with_conditional_trigger(self, triggers_names):
        if "__next__" in triggers_names:
            self.verify_non_conditional_trigger_present()
        return

    def verify_non_conditional_trigger_present(self):
        if self.state_has_conditional_trigger:
            for trig_data in self.state_data["triggers"]:
                if "condition" not in trig_data:
                    return
            raise ConvoGraphValidationError(
                f'"{self.state_data["name"]}" state has conditional __next__ trigger, but doesn\'t have non-conditional one which is required.'
            )
        return

    def validate_conditional_triggers(self):
        for state_data in self._convograph:
            self.state_name = state_data["name"]
            for trig_data in state_data["triggers"]:
                if "condition" in trig_data:
                    self.validate_conditional_trigger(trig_data)
        return

    def validate_conditional_trigger(self, trig_data):
        trigger_conditions = trig_data["condition"]
        for condition in trigger_conditions.keys():
            if not isinstance(condition, str):
                raise ConvoGraphValidationError(f"")
        return

    def run(self):
        validations = [
            self.triggers_valid_or_error,
            self.triggers_have_essential_or_error,
            self.states_comply_with_next_trigger_or_error,
            self.validate_conditional_triggers,
        ]
        for v in validations:
            v()
        return
