from ..core.convograph_validator import ConvoGraphValidator as CoreConvoGraphValidator
from .additional_validators.actions_validator import ActionValidator
from .additional_validators.states_validator import (
    StateValidator,
    StartStateValidator,
    StopStateValidator,
)
from .additional_validators.triggers_validator import TriggerValidator


class ConvoGraphValidator(CoreConvoGraphValidator):
    def include_additional_validations(self):
        self._additional_validations = [
            ActionValidator(self._convograph),
            StateValidator(self._convograph),
            StartStateValidator(self._convograph),
            StopStateValidator(self._convograph),
            TriggerValidator(self._convograph),
        ]
        return
