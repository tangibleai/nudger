from ...convograph_validator_errors import ConvoGraphValidationError
from ...core.additional_validators.states_validator import (
    StateValidator as CoreStateValidator,
    StartStateValidator as CoreStartStateValidator,
    StopStateValidator as CoreStopStateValidator,
)


class StateValidator(CoreStateValidator):
    pass


class StartStateValidator(CoreStartStateValidator):
    _allowed_attrs = (
        "name",
        "convo_name",
        "nlp",
        "triggers",
        "convo_description",
        "lang",
        "version",
    )

    def __init__(self, convograph):
        super().__init__(convograph, self._allowed_attrs)

    def start_state_has_only_next_trigger_or_error(self):
        triggers_count = len(self._state_data["triggers"])
        trigger_data = self.get_first_trigger_data()
        trigger_text = trigger_data.get("user_text", "")
        expected_text = "__next__"
        if triggers_count != 1 or trigger_text != expected_text:
            raise ConvoGraphValidationError(
                f'"start" state must have only "__next__" trigger specified.'
            )
        return


class StopStateValidator(CoreStopStateValidator):
    def stop_state_has_only_default_trigger_or_error(self):
        triggers_count = len(self._state_data["triggers"])
        first_trigger_data = self.get_first_trigger_data()
        expected_data = {
            "user_text": "__default__",
            "target": "start",
        }
        if triggers_count != 1 or first_trigger_data != expected_data:
            raise ConvoGraphValidationError(
                f'"stop" state must have only "__default__" trigger specified pointing on "start" state..'
            )
        return
