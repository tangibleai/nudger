import re


def is_state_name_valid(state_name):
    return bool(re.match("[a-zA-Z0-9_/.]+", state_name))


from .v2.convograph_validator import ConvoGraphValidator as V2ConvoGraphValidator
from .v3.convograph_validator import ConvoGraphValidator as V3ConvoGraphValidator
from .v3_1.convograph_validator import ConvoGraphValidator as V3_1ConvoGraphValidator


def get_validator_by_version(version):
    version_as_str = str(version)
    formatted_version = version_as_str.replace(".", "_")
    return globals()[f"V{formatted_version}ConvoGraphValidator"]
