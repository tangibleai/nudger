import numpy as np
import pandas as pd
import re

from hub.models import Convo
from hub.spreadsheet_linter import ConvoSpreadsheetValidator
from hub.yaml_to_db_records.version_helpers import VersionHelper
from hub.yaml_to_db_records.yaml_to_db_records import states_to_db_records


def create_new_convo_name(project, convos, convo_name):
    """Creates a Convo name based on the version of the Convo"""
    max_version = max([int(obj.name.split("_")[-1]) for obj in convos] or [0])
    new_convo_name = (
        f"{convo_name}_{max_version + 1}"
        if max_version or len(Convo.objects.filter(project=project, name=convo_name))
        else convo_name
    )
    return new_convo_name


def create_new_convo(project, file, convo_name, convo_description, is_public):
    """Creates a new Convo object from the chatbot metadata"""
    convos = [
        obj
        for obj in Convo.objects.filter(project=project)
        if convo_name in obj.name and re.match(r"\d+$", obj.name.split("_")[-1])
    ]

    new_convo_name = create_new_convo_name(project, convos, convo_name)

    new_convo = Convo.objects.create(
        project=project,
        file=file,
        name=new_convo_name,
        description=convo_description,
        is_public=is_public,
    )
    return new_convo


def read_spreadsheet_to_df(file_source, source_type):
    """Reads a CSV, Excel, or Google Sheet into a dataframe"""
    df = None
    if source_type == "csv":
        df = pd.read_csv(file_source)
    elif source_type == "xlsx":
        df = pd.read_excel(
            file_source,
            engine="openpyxl",
            header=0,
        )
    return df


def normalize_df(script_df):
    """Replaces all empty values with 0 and ensures numbers are in integer format"""
    script_df = script_df.fillna(0).replace("", 0)
    script_df["Go to Row Number"] = script_df["Go to Row Number"].apply(np.int64)
    return script_df


def create_start_state(convo_name, convo_description):
    """Sets the default start state for the conversation graph

    >>> create_start_state('Test convo', 'A wonderful conversation')
    {'name': 'start', 'convo_name': 'Test convo', 'convo_description': 'A wonderful conversation', 'nlp': 'keyword', 'lang': 'en', 'version': 3, 'triggers': [{'user_text': '__next__', 'target': 'row3'}]}
    """
    start_state = {
        "name": "start",
        "convo_name": convo_name,
        "convo_description": convo_description,
        "nlp": "keyword",
        "lang": "en",
        "version": 3,
        "triggers": [{"user_text": "__next__", "target": "row3"}],
    }
    return start_state


def set_current_and_next_state_names(index, row):
    """Creates state names based on the row being processed

    >>> df = pd.DataFrame({'Go to Row Number': [4]})
    >>> row = df.iloc[0]
    >>> set_current_and_next_state_names(1, row)
    ('row3', 'row4')
    """
    current_state = f"row{index+2}"
    next_state = f"row{row['Go to Row Number']}"
    if next_state == "row0":
        next_state = "stop"
    return current_state, next_state


def build_single_button_trigger_option(script_df, next_row_index):
    """Builds a trigger object for a button row

    >>> df = pd.DataFrame({'Type': ['Text', 'Condition', 'Condition', 'Text'], 'User says': ['Hi', '', '', 'Nice to meet you'], 'Go to Row Number': [1, 2, 3, 4]})
    >>> build_single_button_trigger_option(df, 1)
    {'user_text': 'Hi', 'button_text': 'Hi', 'target': 'row4'}
    """
    trigger_text = script_df.loc[next_row_index, "User says"]
    next_state = f'row{script_df.loc[next_row_index, "Go to Row Number"]}'
    trigger_option = {
        "user_text": trigger_text,
        "button_text": trigger_text,
        "target": next_state,
    }
    return trigger_option


def normalize_condition_str(str):
    """Runs basic text processing to ensure condition and operand are in correct format"""
    condition_list = str.split("=")
    normalized_condition_list = [x.strip() for x in condition_list]

    condition, operand = normalized_condition_list
    operand = (
        True
        if operand.lower() == "true"
        else False
        if operand.lower() == "false"
        else operand
    )
    return condition, operand


# Based on demo_conditional_triggers.py  Needs to be updated to support condition object
def convert_condition_str_to_object(str):
    """Converts a text representation of a trigger condition to an object

    >>> convert_condition_str_to_object("name__contains= Tad")
    {'context__contains': {'name': 'Tad'}}
    >>> convert_condition_str_to_object("username__isnull=True")
    {'context__username__isnull': True}
    >>> convert_condition_str_to_object("name__startswith= a")
    {'context__username__startswith': 'a'}
    """
    cond = {}

    condition, operand = normalize_condition_str(str)

    # FIXME: Need to add support for 'functions' not just 'context'
    condition = "context__" + condition

    cond = {condition: operand}

    if "__contains" in condition:
        condition_variable = re.findall(r"__(\w+)__", condition)
        try:
            condition_variable = condition_variable[0]
        except:
            condition_variable = ""  # FIXME: Add validation to avoid this

        condition = re.sub(r"\w+__", "context__", condition)
        cond = {condition: {condition_variable: operand}}

    return cond


def build_single_condition_trigger_option(script_df, next_row_index):
    """Builds a trigger object for a condition row

    >>> df = pd.DataFrame({'Type': ['Text', 'Condition', 'Condition', 'Text'], 'User says': ['Hi', '', '', 'Nice to meet you'], 'Go to Row Number': [1, 2, 3, 4], 'Condition': ['', 'contains name', 'contains age', '']})
    >>> build_single_condition_trigger_option(df, 1)
    {'user_text': '__next__', 'target': 'row2', 'condition': {'contains': 'name'}}
    """
    condition_text = script_df.loc[next_row_index, "Condition"]
    # condition_object = convert_condition_str_to_object(condition_text)
    cond_arr = condition_text.split()
    condition_option = {
        "user_text": "__next__",
        "target": f'row{script_df.loc[next_row_index, "Go to Row Number"]}',
        "condition": {cond_arr[0]: cond_arr[1]},
    }
    return condition_option


def build_single_user_input_option(script_df, next_row_index):
    """Builds a trigger based on a user input row"""
    user_says = script_df.loc[next_row_index, "User says"]
    user_response = user_says if user_says else ""
    next_state = f'row{script_df.loc[next_row_index, "Go to Row Number"]}'
    trigger_option = {
        "user_text": user_response,
        "target": next_state,
    }
    return trigger_option


def select_trigger_option_type(script_df, next_row_index, next_row_type):
    """Selects the type of trigger object that should be created

    >>> df = pd.DataFrame({'Type': ['Text', 'Condition', 'Condition', 'Text'], 'User says': ['Hi', '', '', 'Nice to meet you'], 'Go to Row Number': [1, 2, 3, 4], 'Condition': ['', 'contains name', 'contains age', '']})
    >>> select_trigger_option_type(df, 1, 'Condition')
    {'user_text': '__next__', 'target': 'row2', 'condition': {'contains': 'name'}}
    """
    trigger = []
    if next_row_type == "Condition":
        trigger = build_single_condition_trigger_option(script_df, next_row_index)
    elif next_row_type == "Button":
        trigger = build_single_button_trigger_option(script_df, next_row_index)
    elif next_row_type == "User Input":
        trigger = build_single_user_input_option(script_df, next_row_index)
    return trigger


def build_triggers_list(script_df, index, row_type):
    """Iterates through valid trigger rows until reaching a non-trigger row

    >>> df = pd.DataFrame({'Type': ['Text', 'Condition', 'Condition', 'Text'], 'User says': ['Hi', '', '', 'Nice to meet you'], 'Go to Row Number': [1, 2, 3, 4], 'Condition': ['', 'contains name', 'contains age', '']})
    >>> build_triggers_list(df, 0, 'Condition')
    [{'user_text': '__next__', 'target': 'row2', 'condition': {'contains': 'name'}}, {'user_text': '__next__', 'target': 'row3', 'condition': {'contains': 'age'}}]
    """
    row_check = True
    next_row_index = index + 1
    trigger_states = []

    while row_check:
        next_row_type = script_df.loc[next_row_index, "Type"]
        if next_row_type == row_type:
            trigger_option = select_trigger_option_type(
                script_df, next_row_index, next_row_type
            )
            trigger_states.append(trigger_option)
            next_row_index += 1
        else:
            row_check = False
    return trigger_states


def build_triggers_for_state(script_df, index, row, next_state_name):
    """Builds the predefined user inputs available for each state

    >>> df = pd.DataFrame({'Type': ['Button Prompt', 'Button', 'Message'], 'User says': ['', 'Hi', ''], 'Go to Row Number': [0, 4, 5]})
    >>> row = df.iloc[2]
    >>> build_triggers_for_state(df, 0, row, 'row5')
    [{'user_text': '__next__', 'target': 'row5'}]
    """
    default_trigger = {"user_text": "__next__", "target": next_state_name}
    triggers = [default_trigger]
    current_row_type = row["Type"]

    next_row = index + 1
    next_row_type = script_df.loc[next_row, "Type"]
    if current_row_type == "Button Prompt":
        triggers = build_triggers_list(script_df, index, row["Type"])
    elif next_row_type == "Condition":
        triggers = build_triggers_list(script_df, index, next_row_type)
        triggers.append(default_trigger)
    elif next_row_type == "User Input":
        default_trigger = {"user_text": "__default__", "target": next_state_name}
        triggers = build_triggers_list(script_df, index, next_row_type)
        triggers.append(default_trigger)
    return triggers


def build_system_action(script_df, next_row_index):
    """Builds an action to save the user message as a variable in the context dictionary

    Will need to be updated later to support more system actions
    """
    action = {}
    action_text = script_df.loc[next_row_index, "System Action"]
    action_arr = action_text.split()
    if action_arr[0].lower() == "save":
        action = {"extract_user_text": {"key_to_update": action_arr[1]}}
    return action


def search_next_rows_for_system_actions(script_df, index, row_type):
    """Determines if the next row has a save action

    Needs to be generalized for other types of rows, system actions, and/or current row saving
    """
    row_check = True
    next_row_index = index + 1
    actions = []

    while row_check:
        next_row_type = script_df.loc[next_row_index, "Type"]
        if next_row_type == row_type:
            action = build_system_action(script_df, next_row_index)
            actions.append(action)
            next_row_index += 1
        else:
            row_check = False
    return actions


def build_action_for_state(script_df, index, row):
    """Creates an action for the chatbot to perform based on the row being processed

    >>> df = pd.DataFrame({'Type': ['Text'], 'Bot says': ['Hi!']})
    >>> row = df.iloc[0]
    >>> build_action_for_state(row)
    [{'send_message': {'bot_text': 'Hi!'}}]
    """
    action = []
    if row["Type"] == "Text":
        action = [{"send_message": {"bot_text": row["Bot says"]}}]
    elif row["Type"] == "Image":
        action = [{"send_image": {"image_name": row["Links"]}}]
    elif row["Type"] in ["User Input Prompt"]:
        action = [{"send_message": {"bot_text": row["Bot says"]}}]
        system_actions = search_next_rows_for_system_actions(
            script_df, index, "User Input"
        )
        action = system_actions + action
    return action


def build_state_object(script_df, index, row):
    """Creates a full representation of a chatbot state, including any message, user input options, and metadata

    >>> df = pd.DataFrame({'Type': ['Button Prompt', 'Button', 'Message'], 'User says': ['', 'Hi', ''], 'Go to Row Number': [0, 3, 4]})
    >>> row = df.iloc[0]
    >>> build_state_object(df, 0, row)
    {'current_state': 'row2', 'state_entry': {'name': 'row2', 'actions': [], 'triggers': [{'user_text': 'Hi', 'button_text': 'Hi', 'target': 'row3'}]}}
    """
    current_state_name, next_state_name = set_current_and_next_state_names(index, row)
    actions = build_action_for_state(script_df, index, row)
    triggers = build_triggers_for_state(script_df, index, row, next_state_name)
    entry = {
        "name": current_state_name,
        "actions": actions,
        "triggers": triggers,
    }
    return {"current_state": current_state_name, "state_entry": entry}


def extract_save_state(state):
    """Extracts a save entry from the state dictionary"""
    save_action = {}
    for action in state["state_entry"]["actions"]:
        if "extract_user_text" in action.keys():
            save_action = action
            state["state_entry"]["actions"].remove(action)
    return save_action, state


def extract_target_rows(state):
    """Extract the target rows for a save action"""
    target_rows = []
    for row in state["state_entry"]["triggers"]:
        target_rows.append(row["target"])
    return target_rows


def build_actions_queue_dict(state):
    """Updates the state and pulls out actions to add to the queue"""
    new_actions = {}
    save_action, state = extract_save_state(state)
    target_rows = extract_target_rows(state)
    for row in target_rows:
        new_actions[row] = save_action
    return new_actions, state


def extract_save_action(actions_queue, current_row):
    try:
        action = actions_queue[current_row]
    except:
        action = {}
    return action


def process_script_df_rows(script_df, convo_name, convo_description):
    full_dialog = {}
    full_dialog["start"] = create_start_state(convo_name, convo_description)

    actions_queue = {}

    for index, row in script_df.iterrows():
        if row["Type"] in ["Text", "Button Prompt", "Image", "User Input Prompt"]:
            state = build_state_object(script_df, index, row)

            # NOTE: Save states occur in the state after a user has responded
            new_actions, updated_state = build_actions_queue_dict(state)
            if new_actions:
                state = updated_state
                # NOTE: This will overwrite duplicate keys.
                # FIXME: Need to add validation to avoid losing save data.
                actions_queue.update(new_actions)
            action_to_add = extract_save_action(actions_queue, state["current_state"])
            if action_to_add:
                state["state_entry"]["actions"].append(action_to_add)

            full_dialog[state["current_state"]] = state["state_entry"]

    full_dialog["stop"] = {
        "name": "stop",
        "triggers": [{"user_text": "__default__", "target": "start"}],
    }
    return full_dialog


def convert_spreadsheet_to_dictionary(file_source, file_type):
    """Converts each row of the spreadsheet to state object in a dialog dictionary"""
    df = read_spreadsheet_to_df(file_source, file_type)
    df = normalize_df(df)

    ConvoSpreadsheetValidator(df).run()

    return df


def upload_spreadsheet_to_db(
    project, file_source, file_type, convo_name, convo_description, is_public
):
    df = convert_spreadsheet_to_dictionary(file_source, file_type)
    dialog = process_script_df_rows(df, convo_name, convo_description)

    print("DIALOG=========================")
    print(dialog)
    print("===============================")

    new_convo = create_new_convo(
        project, file_source, convo_name, convo_description, is_public
    )
    format_version = 3.1
    new_convo.version = format_version
    new_convo.save()

    states_to_db_records(dialog, new_convo)
    VersionHelper(dialog, new_convo, format_version).execute_helpers()
