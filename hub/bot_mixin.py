from django.contrib.auth.mixins import UserPassesTestMixin
from django.shortcuts import HttpResponse


class StaffRequiredMixin(UserPassesTestMixin):
    def test_func(self):
        return self.request.user.is_staff

    def handle_no_permission(self):
        return HttpResponse("You are no allowed to see this page!")
